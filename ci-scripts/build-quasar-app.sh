#!/bin/sh
set -e

app=$1

script_dir="$(cd "$(dirname "$0")" && pwd)"
root_dir=$(dirname $script_dir)

# build local modules

echo "Building local modules"

for module_dir in $root_dir/software/js-modules/*; do
  echo "Building $(basename $module_dir)"
  cd $module_dir
  npm install
  npm run build
done

# install quasar

echo "Installing Quasar"
yarn global add @quasar/cli typescript

# build app

echo "Building $app"
cd $root_dir/software/$app
yarn
quasar build

# move outputs to artifacts dir

echo "Collecting artifacts"
cd $root_dir
mkdir -p artifacts
mv $root_dir/software/$app/dist/spa artifacts/$app

echo "Done"