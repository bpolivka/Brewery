#!/bin/sh
set -e

app=$1

script_dir="$(cd "$(dirname "$0")" && pwd)"
root_dir=$(dirname $script_dir)

cd $root_dir/software/$app
dotnet restore
dotnet publish -c Release -o $root_dir/artifacts/$app --no-self-contained --no-restore
