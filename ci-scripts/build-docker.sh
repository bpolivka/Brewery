#!/bin/sh

script_dir="$(cd "$(dirname "$0")" && pwd)"
root_dir=$(dirname $script_dir)

src_dir="$root_dir/software"
docker_dir="$src_dir/docker"
context_dir="$root_dir/docker-context"

component=$1

docker buildx build \
  --platform $CI_BUILDX_ARCHS --progress plain --pull --push \
  -t "$CI_REGISTRY_IMAGE/$component" \
  -f "$docker_dir/$component/Dockerfile" \
  "$context_dir"
