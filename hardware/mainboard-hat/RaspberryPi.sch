EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J?
U 1 1 5EA676C8
P 5650 3200
AR Path="/5EA676C8" Ref="J?"  Part="1" 
AR Path="/5EA63D4E/5EA676C8" Ref="J1"  Part="1" 
F 0 "J1" H 5700 4317 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 5700 4226 50  0001 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 5650 3200 50  0001 C CNN
F 3 "~" H 5650 3200 50  0001 C CNN
	1    5650 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2600 5400 2600
Wire Wire Line
	5450 2800 5400 2800
Wire Wire Line
	5450 2900 5400 2900
Wire Wire Line
	5450 3200 5400 3200
Wire Wire Line
	5450 3300 5400 3300
Wire Wire Line
	5450 3400 5400 3400
Wire Wire Line
	5450 3700 5400 3700
Wire Wire Line
	5450 3900 5400 3900
Wire Wire Line
	5450 4000 5400 4000
Wire Wire Line
	5450 4100 5400 4100
Wire Wire Line
	6000 4100 5950 4100
Wire Wire Line
	6000 3800 5950 3800
Wire Wire Line
	6000 3600 5950 3600
Wire Wire Line
	6000 3500 5950 3500
Wire Wire Line
	6000 3400 5950 3400
Wire Wire Line
	6000 3300 5950 3300
Wire Wire Line
	6000 3100 5950 3100
Wire Wire Line
	6000 3000 5950 3000
Wire Wire Line
	6000 2800 5950 2800
Wire Wire Line
	6000 2700 5950 2700
Wire Wire Line
	6000 2600 5950 2600
$Comp
L power:+3.3V #PWR?
U 1 1 5EA676E5
P 4300 2050
AR Path="/5EA676E5" Ref="#PWR?"  Part="1" 
AR Path="/5EA63D4E/5EA676E5" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 4300 1900 50  0001 C CNN
F 1 "+3.3V" H 4315 2223 50  0000 C CNN
F 2 "" H 4300 2050 50  0001 C CNN
F 3 "" H 4300 2050 50  0001 C CNN
	1    4300 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5EA676EB
P 6700 2050
AR Path="/5EA676EB" Ref="#PWR?"  Part="1" 
AR Path="/5EA63D4E/5EA676EB" Ref="#PWR0116"  Part="1" 
F 0 "#PWR0116" H 6700 1900 50  0001 C CNN
F 1 "+5V" H 6715 2223 50  0000 C CNN
F 2 "" H 6700 2050 50  0001 C CNN
F 3 "" H 6700 2050 50  0001 C CNN
	1    6700 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2300 4300 2050
Wire Wire Line
	4300 2300 5000 2300
Wire Wire Line
	6700 2300 6700 2050
Wire Wire Line
	5950 2300 6700 2300
Wire Wire Line
	6700 2400 6700 2300
Wire Wire Line
	5950 2400 6700 2400
Connection ~ 6700 2300
Wire Wire Line
	4300 3100 5450 3100
Connection ~ 4300 2300
$Comp
L power:GND #PWR?
U 1 1 5EA676FB
P 5700 4750
AR Path="/5EA676FB" Ref="#PWR?"  Part="1" 
AR Path="/5EA63D4E/5EA676FB" Ref="#PWR0117"  Part="1" 
F 0 "#PWR0117" H 5700 4500 50  0001 C CNN
F 1 "GND" H 5705 4577 50  0000 C CNN
F 2 "" H 5700 4750 50  0001 C CNN
F 3 "" H 5700 4750 50  0001 C CNN
	1    5700 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2700 4400 3500
Wire Wire Line
	4400 4600 5700 4600
Wire Wire Line
	5700 4600 5700 4750
Wire Wire Line
	4400 2700 5450 2700
Wire Wire Line
	4400 3500 5450 3500
Connection ~ 4400 3500
Wire Wire Line
	4400 3500 4400 4200
Wire Wire Line
	4400 4200 5450 4200
Connection ~ 4400 4200
Wire Wire Line
	4400 4200 4400 4600
Wire Wire Line
	7050 2500 7050 2900
Connection ~ 7050 2900
Wire Wire Line
	7050 2900 7050 3200
Connection ~ 7050 3200
Wire Wire Line
	7050 3200 7050 3700
Connection ~ 7050 3700
Wire Wire Line
	7050 3700 7050 3900
Connection ~ 7050 3900
Wire Wire Line
	7050 3900 7050 4600
Text Label 4500 2500 0    50   ~ 0
GPIO3
Text Label 4500 3800 0    50   ~ 0
GPIO6
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5EA67742
P 5000 2300
AR Path="/5EA67742" Ref="#FLG?"  Part="1" 
AR Path="/5EA63D4E/5EA67742" Ref="#FLG0104"  Part="1" 
F 0 "#FLG0104" H 5000 2375 50  0001 C CNN
F 1 "PWR_FLAG" H 5000 2473 50  0000 C CNN
F 2 "" H 5000 2300 50  0001 C CNN
F 3 "~" H 5000 2300 50  0001 C CNN
	1    5000 2300
	1    0    0    -1  
$EndComp
Connection ~ 5000 2300
Wire Wire Line
	5000 2300 5450 2300
Wire Wire Line
	6000 4200 5950 4200
Wire Wire Line
	6000 4000 5950 4000
Wire Wire Line
	5400 2400 5450 2400
Wire Wire Line
	4300 3100 4300 2300
Wire Wire Line
	4000 3800 4000 3400
$Comp
L Device:R R?
U 1 1 5EA67755
P 4000 3250
AR Path="/5EA67755" Ref="R?"  Part="1" 
AR Path="/5EA63D4E/5EA67755" Ref="R6"  Part="1" 
F 0 "R6" H 4070 3296 50  0000 L CNN
F 1 "68R" H 4070 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 3930 3250 50  0001 C CNN
F 3 "~" H 4000 3250 50  0001 C CNN
	1    4000 3250
	1    0    0    -1  
$EndComp
Text Notes 3400 3350 0    47   ~ 0
Power LED
Wire Wire Line
	4000 3100 3700 3100
Wire Wire Line
	3750 3200 3750 2600
Connection ~ 3750 3200
Wire Wire Line
	3700 3200 3750 3200
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5EA6774A
P 3500 3100
AR Path="/5EA6774A" Ref="J?"  Part="1" 
AR Path="/5EA63D4E/5EA6774A" Ref="J9"  Part="1" 
F 0 "J9" H 3418 3317 50  0000 C CNN
F 1 "Conn_01x02" H 3418 3226 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 3500 3100 50  0001 C CNN
F 3 "~" H 3500 3100 50  0001 C CNN
	1    3500 3100
	-1   0    0    -1  
$EndComp
Text Notes 3400 2750 0    47   ~ 0
Reset Switch
Wire Wire Line
	3750 4600 3750 3200
$Comp
L Connector_Generic:Conn_01x02 J?
U 1 1 5EA67737
P 3550 2500
AR Path="/5EA67737" Ref="J?"  Part="1" 
AR Path="/5EA63D4E/5EA67737" Ref="J8"  Part="1" 
F 0 "J8" H 3468 2717 50  0000 C CNN
F 1 "Conn_01x02" H 3468 2626 50  0000 C CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 3550 2500 50  0001 C CNN
F 3 "~" H 3550 2500 50  0001 C CNN
	1    3550 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3750 2500 5450 2500
Wire Wire Line
	4000 3800 5450 3800
Wire Wire Line
	3750 4600 4400 4600
Connection ~ 4400 4600
Wire Wire Line
	5950 2500 7050 2500
Wire Wire Line
	5950 2900 7050 2900
Wire Wire Line
	5950 3200 7050 3200
Wire Wire Line
	5950 3700 7050 3700
Wire Wire Line
	5950 3900 7050 3900
Text HLabel 6000 3800 2    47   BiDi ~ 0
GPIO12
Wire Wire Line
	5700 4600 7050 4600
Connection ~ 5700 4600
Text HLabel 5400 2400 0    50   BiDi ~ 0
GPIO2_SDA1
Text HLabel 5400 2600 0    50   BiDi ~ 0
GPIO4_GPCLK0
Text HLabel 5400 2900 0    50   BiDi ~ 0
GPIO27
Text HLabel 5400 2800 0    50   BiDi ~ 0
GPIO17
Wire Wire Line
	5450 3000 5400 3000
Text HLabel 5400 3000 0    50   BiDi ~ 0
GPIO22
Text HLabel 5400 3200 0    50   BiDi ~ 0
GPIO10_MOSI
Text HLabel 5400 3300 0    50   BiDi ~ 0
GPIO9_MISO
Text HLabel 5400 3400 0    50   Input ~ 0
GPIO11_CLK
Wire Wire Line
	5450 3600 5400 3600
Text HLabel 5400 3600 0    50   BiDi ~ 0
GPIO0_SDA0
Text HLabel 5400 3700 0    50   BiDi ~ 0
GPIO5
Text HLabel 5400 3900 0    50   BiDi ~ 0
GPIO13_PWM1
Text HLabel 5400 4000 0    50   BiDi ~ 0
GPIO19
Text HLabel 5400 4100 0    50   BiDi ~ 0
GPIO26
Text HLabel 6000 2600 2    50   BiDi ~ 0
GPIO14_TXD0
Text HLabel 6000 2700 2    50   BiDi ~ 0
GPIO15_RXD0
Text HLabel 6000 2800 2    50   BiDi ~ 0
GPIO18_PWM0
Text HLabel 6000 3000 2    50   BiDi ~ 0
GPIO23
Text HLabel 6000 3100 2    50   BiDi ~ 0
GPIO24
Text HLabel 6000 3300 2    50   BiDi ~ 0
GPIO25
Text HLabel 6000 3400 2    50   BiDi ~ 0
GPIO8_CE0
Text HLabel 6000 3500 2    50   BiDi ~ 0
GPIO7_CE1
Text HLabel 6000 3600 2    50   BiDi ~ 0
GPIO1_SCL0
Text HLabel 6000 4000 2    50   BiDi ~ 0
GPIO16
Text HLabel 6000 4100 2    50   BiDi ~ 0
GPIO20
Text HLabel 6000 4200 2    50   BiDi ~ 0
GPIO21
$EndSCHEMATC
