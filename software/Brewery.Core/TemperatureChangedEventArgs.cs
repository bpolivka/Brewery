﻿using System;

namespace Brewery
{
    public class TemperatureChangedEventArgs : EventArgs
    {
        public TemperatureProbe Probe { get; set; }
    }
}
