﻿using System;
using System.Collections.Generic;

namespace Brewery.Messages
{
    public class BreweryState
    {
        public Dictionary<string, RelayState> Relays { get; } = new Dictionary<string, RelayState>();
        public Dictionary<string, TemperatureProbeState> TemperatureProbes { get; } = new Dictionary<string, TemperatureProbeState>();
        public Dictionary<string, HeatingElementState> HeatingElements { get; } = new Dictionary<string, HeatingElementState>();
    }
}
