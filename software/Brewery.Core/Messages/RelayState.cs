﻿using System;
namespace Brewery.Messages
{
    public class RelayState
    {
        public bool Enabled { get; set; }
    }
}
