﻿using System;
namespace Brewery.Messages
{
    public class TemperatureChangedMessage
    {
        public double Temperature { get; set; } 
    }
}
