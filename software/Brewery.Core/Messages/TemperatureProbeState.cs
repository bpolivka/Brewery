﻿namespace Brewery.Messages
{
    public class TemperatureProbeState
    {
        public double Temperature { get; set; }
    }
}
