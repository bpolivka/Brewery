﻿using System;
using System.Collections.Generic;

namespace Brewery.Messages
{
    public class HeatingElementHistoryMessage
    {
        public List<HeatingElementHistoryRecord> Records { get; } = new List<HeatingElementHistoryRecord>();
    }

    public class HeatingElementHistoryRecord
    {
        public DateTime Time { get; set; }
        public int PowerLevel { get; set; }
    }
}
