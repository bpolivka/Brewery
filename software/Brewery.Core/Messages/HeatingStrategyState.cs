﻿using System;
using System.Text.Json;

namespace Brewery.Messages
{
    public class HeatingStrategyState
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public JsonElement Parameters { get; set; }
    }
}
