﻿
using System;
using System.Collections.Generic;

namespace Brewery.Messages
{
    public class HeatingElementState
    {
        public int PowerLevel { get; set; }
        public List<HeatingStrategyState> AvailableStrategies { get; } = new List<HeatingStrategyState>();
        public Guid CurrentStrategyId { get; set; }
    }
}
