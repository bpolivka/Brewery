﻿using System;
using System.Collections.Generic;

namespace Brewery.Messages
{
    public class TemperatureProbeHistoryMessage
    {
        public List<TemperatureHistoryRecord> Records { get; } = new List<TemperatureHistoryRecord>();
    }

    public class TemperatureHistoryRecord
    {
        public DateTime Time { get; set; }
        public double Temperature { get; set; }
    }
}
