using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Brewery
{
  public class TemperatureProbe
  {
    public string Name { get; }
    public ITemperatureProbeImpl Impl { get; }
    public TemperatureProbeCalibrationData CalibrationData { get; set; }

    public TemperatureProbe(string name, ITemperatureProbeImpl impl)
    {
      Name = name;
      Impl = impl;
      CalibrationData = TemperatureProbeCalibrationData.Create(new []{ 1.0, 0.0 });
      Impl.TemperatureChanged += RaiseTemperatureChanged;
    }

    public double Temperature
    {
      get
      {
        var temp = Impl.Temperature;
        var coefficients = CalibrationData.Coefficients;

        // Initialize result 
        var result = coefficients[0];

        // Evaluate value of polynomial using Horner's method 
        for (int i = 1; i < coefficients.Length; i++)
        {
          result = result * temp + coefficients[i];
        }

        return result;
      }
    }

    public event Action TemperatureChanged;

    void RaiseTemperatureChanged()
    {
      TemperatureChanged?.Invoke();
    }

    public void UpdateConfiguration(JObject conf)
    {
      var calibrationData = conf.ToObject<TemperatureProbeCalibrationData>();

      CalibrationData = calibrationData;
    }
  }

  public class TemperatureProbeCalibrationData
  {
    
    public static TemperatureProbeCalibrationData Create(double[] coefficients)
    {
      return new TemperatureProbeCalibrationData
      {
        Coefficients = coefficients
      };
    }

    public double[] Coefficients { get; set; }
  }
}