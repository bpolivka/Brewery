﻿namespace Brewery
{
    public class StrategyUpdatedEventArgs
    {
        public StrategyUpdatedEventArgs(string elementName, HeatingStrategy oldStrategy, HeatingStrategy newStrategy)
        {
            ElementName = elementName;
            OldStrategy = oldStrategy;
            NewStrategy = newStrategy;
        }

        public string ElementName { get; }
        public HeatingStrategy OldStrategy { get; }
        public HeatingStrategy NewStrategy { get; }
    }
}