using System;

namespace Brewery
{
    public interface ITimeProvider
    {
        DateTime GetCurrentTime();
    }
}