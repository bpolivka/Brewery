﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery
{
    public interface ITemperatureProbeController
    {
        Task StartAsync(CancellationToken cancellationToken);
        Task StopAsync(CancellationToken cancellationToken);
        
        ITemperatureProbeImpl CreateProbeImpl();
    }
}
