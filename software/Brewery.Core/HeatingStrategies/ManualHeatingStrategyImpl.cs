﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery.HeatingStrategies
{

    public class ManualHeatingStrategyImpl : IHeatingStrategyImpl
    {
        int _powerLevel = 0;

        public string Name => "Manual";
        public string Type => "manual";

        public PowerLevelCalculationResult CalcPowerLevel(int limit, bool dryRun)
        {
            return new PowerLevelCalculationResult
            {
                PowerLevel = Math.Min(_powerLevel, limit)
            };
        }

        public JObject GetParameters()
        {
            return new JObject(
                new JProperty("powerLevel", _powerLevel)
                );
        }

        public void SetParameters(JObject parameters)
        {
            foreach (var prop in parameters.Properties())
            {
                switch (prop.Name)
                {
                    case "powerLevel":
                        _powerLevel = Math.Min(prop.Value.Value<int>(), 100);
                        break;
                }
            }
        }
    }
}
