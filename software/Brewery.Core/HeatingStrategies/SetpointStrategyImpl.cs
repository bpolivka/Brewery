﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery.HeatingStrategies
{
    public abstract class SetpointStrategyImpl : IHeatingStrategyImpl
    {
        public string Type => "setpoint";

        public abstract string Name { get; }

        protected abstract double Setpoint { get; set; }

        public abstract PowerLevelCalculationResult CalcPowerLevel(int limit, bool dryRun);

        public JObject GetParameters()
        {
            return new JObject(
                new JProperty("setpoint", Setpoint)
                );
        }

        public void SetParameters(JObject parameters)
        {
            foreach (var prop in parameters.Properties())
            {
                switch (prop.Name)
                {
                    case "setpoint":
                        Setpoint = prop.Value.Value<double>();
                        break;
                }
            }
        }
    }
}
