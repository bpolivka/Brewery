﻿using System;
namespace Brewery.HeatingStrategies
{
  public class PidController
  {
    private readonly ITimeProvider _timeProvider;

    public DateTime LastTime { get; private set; }
    public double? LastInput { get; private set; }

    public double OutputSum { get; private set; }
    public double POnMeasurementTerm { get; private set; }
    public double POnErrorTerm { get; private set; }
    public double ITerm { get; private set; }
    public double DTerm { get; private set; }

    public double MinValue { get; set; } = 0.0;
    public double MaxValue { get; set; } = 100.0;

    public double Setpoint { get; set; }
    public double KpOnMeasurement { get; set; }
    public double KpOnError { get; set; }
    public double Ki { get; set; }
    public double Kd { get; set; }
    public double OutputSumLimit { get; set; } = 100.0;

    public PidController(ITimeProvider timeProvider)
    {
      _timeProvider = timeProvider;
      Reset();
    }

    public double Calculate(double input, bool dryRun)
    {
      var now = _timeProvider.GetCurrentTime();
      var dt = (now - LastTime).TotalSeconds;

      var outputSum = OutputSum;

      // compute error terms
      var error = Setpoint - input;
      var dInput = input - (LastInput ?? input);
      var iTerm = (Ki * dt) * error;
      outputSum += iTerm;

      // add proportional on measurement
      var pOnMeasurementTerm = KpOnMeasurement * dInput;
      outputSum -= pOnMeasurementTerm;

      outputSum = Clamp(outputSum);
      if (outputSum > OutputSumLimit)
        outputSum = OutputSumLimit;

      // add proportional on error
      var pOnErrorTerm = KpOnError * error;
      var output = pOnErrorTerm;

      // compute rest of PID output
      var dTerm = (Kd / dt) * dInput;
      output += outputSum - dTerm;

      output = Clamp(output);

      if (!dryRun)
      {
        // update state
        ITerm = iTerm;
        OutputSum = outputSum;
        POnMeasurementTerm = pOnMeasurementTerm;
        POnErrorTerm = pOnErrorTerm;
        DTerm = dTerm;
        LastInput = input;
        LastTime = now;
      }

      return output;
    }

    public void Reset()
    {
      LastInput = null;
      LastTime = _timeProvider.GetCurrentTime();
      OutputSum = 0.0;
      POnMeasurementTerm = 0.0;
      POnErrorTerm = 0.0;
      ITerm = 0.0;
      DTerm = 0.0;
    }

    double Clamp(double value)
    {
      if (value > MaxValue) return MaxValue;
      if (value < MinValue) return MinValue;
      return value;
    }
  }
}
