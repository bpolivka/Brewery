﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery.HeatingStrategies
{
    public class DisabledHeatingStrategyImpl : IHeatingStrategyImpl
    {
        public static DisabledHeatingStrategyImpl Instance { get; } = new DisabledHeatingStrategyImpl();

        private DisabledHeatingStrategyImpl() { }

        public string Name => "Disabled";
        public string Type => "disabled";

        public PowerLevelCalculationResult CalcPowerLevel(int limit, bool dryRun)
        {
            return new PowerLevelCalculationResult
            {
                PowerLevel = 0
            };
        }

        public JObject GetParameters()
        {
            return new JObject();
        }

        public void SetParameters(JObject parameters)
        {
        }
    }
}
