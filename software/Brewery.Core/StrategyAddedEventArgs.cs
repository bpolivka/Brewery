﻿using System;

namespace Brewery
{
    public class StrategyAddedEventArgs : EventArgs
    {
        public StrategyAddedEventArgs(string elementName, HeatingStrategy strategy)
        {
            ElementName = elementName;
            Strategy = strategy;
        }

        public string ElementName { get; }
        public HeatingStrategy Strategy { get; }
    }
}