﻿namespace Brewery
{
    public class StrategyCompileFailedEventArgs
    {
        public StrategyCompileFailedEventArgs(string name, string message)
        {
            Name = name;
            Message = message;
        }

        public string Name { get; }
        public string Message { get; }
    }
}