﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace Brewery
{
  public class HeatingElement
  {
    readonly HeatingStrategy _disabledStrategy;
    List<HeatingStrategy> _strategies = new List<HeatingStrategy>();
    private readonly IMediator _mediator;
    readonly Relay _dependsOnRelay;
    int _powerLevel = 0;

    public HeatingElement(IMediator mediator, string name, Relay relay, Relay dependsOnRelay)
    {
      Name = name;
      Relay = relay;
      _mediator = mediator;
      _dependsOnRelay = dependsOnRelay;
      _dependsOnRelay.EnabledChanged += OnDependsOnRelayEnabledChanged;

      _disabledStrategy = new HeatingStrategy(HeatingStrategies.DisabledHeatingStrategyImpl.Instance);

      AddAvailableStrategy(_disabledStrategy);
      AddAvailableStrategy(new HeatingStrategy(new HeatingStrategies.ManualHeatingStrategyImpl()));

      CurrentStrategy = _disabledStrategy;
    }

    public int PowerLevel
    {
      get => _powerLevel;
      set
      {
        if (value != _powerLevel)
        {
          _powerLevel = value;
          RaisePowerLevelChanged();
        }
      }
    }

    public event Action PowerLevelChanged;
    public event Action StrategyChanged;

    public void AddAvailableStrategy(HeatingStrategy strategy)
    {
      lock (_strategies) { _strategies.Add(strategy); }
      strategy.ParametersChanged += OnStrategyParametersChanged;
    }

    public void RemoveAvailableStrategy(HeatingStrategy strategy)
    {
      lock (_strategies) { _strategies.Remove(strategy); }
      strategy.ParametersChanged -= OnStrategyParametersChanged;
      if (CurrentStrategy == strategy)
      {
        SetCurrentStrategy(_disabledStrategy);
      }
    }

    private void OnStrategyParametersChanged(HeatingStrategy strategy)
    {
      var parameters = strategy.GetParameters();
      _mediator.Publish(new Handlers.HeatingStrategyParametersChanged.Notification(
          this,
          strategy,
          parameters));
    }

    void RaisePowerLevelChanged() => PowerLevelChanged?.Invoke();

    public int GetDesiredPowerLevel()
    {
      var result = CurrentStrategy.CalcPowerLevel(100, true);
      return result.PowerLevel;
    }

    public void UpdatePowerLevel(int maxLevel)
    {
      var result = CurrentStrategy.CalcPowerLevel(maxLevel, false);

      _mediator.Publish(new Handlers.HeatingElementPowerLevelCalculated.Notification(
          this,
          CurrentStrategy,
          result));

      PowerLevel = Math.Min(result.PowerLevel, maxLevel);
    }

    public event Action EnabledChanged;

    void RaiseEnabledChanged() => EnabledChanged?.Invoke();

    public bool Enabled { get; private set; } = false;

    public string Name { get; }
    public Relay Relay { get; }

    public HeatingStrategy CurrentStrategy { get; private set; }

    public IEnumerable<HeatingStrategy> AvailableStrategies
    {
      get
      {
        lock (_strategies)
        {
          return _strategies.ToList();
        }
      }
    }

    public HeatingStrategy GetAvailableStrategy(Guid id)
    {
      lock (_strategies)
      {
        return _strategies.FirstOrDefault(s => s.Id == id);
      }
    }

    public void SetCurrentStrategy(HeatingStrategy strategy)
    {
      CurrentStrategy = strategy;
      MaybeSetEnabled();
      StrategyChanged?.Invoke();
    }

    public async Task FireForAsync(TimeSpan fireTime)
    {
      if (!Enabled) return;

      var enabledCts = new CancellationTokenSource();

      void OnEnabledChanged()
      {
        enabledCts.Cancel();
      }

      _dependsOnRelay.EnabledChanged += OnEnabledChanged;

      Relay.Enabled = true;

      try
      {
        await Task.Delay(fireTime, enabledCts.Token);
      }
      catch (OperationCanceledException) { }

      Relay.Enabled = false;

      _dependsOnRelay.EnabledChanged -= OnEnabledChanged;
    }

    void MaybeSetEnabled()
    {
      var enabled =
          _dependsOnRelay.Enabled &&
          CurrentStrategy != _disabledStrategy;

      if (Enabled != enabled)
      {
        Enabled = enabled;
        RaiseEnabledChanged();

        if (!Enabled)
        {
          PowerLevel = 0;
        }
      }
    }

    void OnDependsOnRelayEnabledChanged()
    {
      MaybeSetEnabled();
    }

    public Messages.HeatingElementState GetState()
    {
      var state = new Messages.HeatingElementState
      {
        PowerLevel = PowerLevel
      };

      foreach (var strategy in AvailableStrategies)
      {
        state.AvailableStrategies.Add(strategy.GetState());
      }

      state.CurrentStrategyId = CurrentStrategy.Id;

      return state;
    }
  }
}
