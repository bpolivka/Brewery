﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;
using MediatR;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Linq;
using Nito.AsyncEx;

namespace Brewery
{
  public class HeatingElementManager : IHostedService
  {
    Dictionary<string, HeatingElement> _byName = new Dictionary<string, HeatingElement>();
    CancellationTokenSource _cts = new CancellationTokenSource();
    TimeSpan _timeSlice = TimeSpan.FromSeconds(3);
    readonly AsyncMonitor _enabledElementMonitor = new AsyncMonitor();
    private readonly IMediator _mediator;
    private readonly HeatingStrategyManager _strategyManager;
    Task _schedulerLoopTask;

    public HeatingElementManager(
        RelayManager relayManager,
        IMediator mediator,
        HeatingStrategyManager strategyManager)
    {
      _mediator = mediator;
      _strategyManager = strategyManager;

      Hlt = CreateHeatingElement("hlt", relayManager.HltSsr, relayManager.Hlt);
      Boil = CreateHeatingElement("boil", relayManager.BoilSsr, relayManager.Boil);

      _strategyManager.StrategyAdded += OnStrategyAdded;
      _strategyManager.StrategyRemoved += OnStrategyRemoved;
      _strategyManager.StrategyUpdated += OnStrategyUpdated;
      _strategyManager.StrategyCompileFailed += OnStrategyCompileFailed;
    }

    private void OnStrategyCompileFailed(object sender, StrategyCompileFailedEventArgs e)
    {
      _mediator.Publish(new Handlers.HeatingStrategyCompileFailed.Notification(e.Name, e.Message));
    }

    private void OnStrategyUpdated(object sender, StrategyUpdatedEventArgs e)
    {
      var element = GetByName(e.ElementName);
      if (element == null) return;

      var isCurrent = element.CurrentStrategy == e.OldStrategy;

      element.RemoveAvailableStrategy(e.OldStrategy);
      element.AddAvailableStrategy(e.NewStrategy);

      _mediator.Publish(new Handlers.HeatingStrategyUpdated.Notification(element, e.OldStrategy, e.NewStrategy));

      if (isCurrent)
      {
        element.SetCurrentStrategy(e.NewStrategy);
      }
    }

    private void OnStrategyRemoved(object sender, StrategyRemovedEventArgs e)
    {
      var element = GetByName(e.ElementName);
      element?.RemoveAvailableStrategy(e.Strategy);
      _mediator.Publish(new Handlers.HeatingStrategyRemoved.Notification(element, e.Strategy));
    }

    private void OnStrategyAdded(object sender, StrategyAddedEventArgs e)
    {
      var element = GetByName(e.ElementName);
      element?.AddAvailableStrategy(e.Strategy);
      _mediator.Publish(new Handlers.HeatingStrategyAdded.Notification(element, e.Strategy));
    }

    public HeatingElement Hlt { get; }
    public HeatingElement Boil { get; }

    HeatingElement CreateHeatingElement(string name, Relay relay, Relay dependsOnRelay)
    {
      var element = new HeatingElement(_mediator, name, relay, dependsOnRelay);
      _byName[name] = element;
      element.EnabledChanged += OnElementEnabledChanged;
      element.PowerLevelChanged += () => OnPowerLevelChanged(element);
      element.StrategyChanged += () => OnStrategyChanged(element);
      return element;
    }

    private void OnStrategyChanged(HeatingElement element)
    {
      _mediator.Publish(new Handlers.HeatingElementStrategyChanged.Notification(element, element.CurrentStrategy));

    }

    public HeatingElement GetByName(string name)
    {
      _byName.TryGetValue(name, out var element);
      return element;
    }

    private void OnPowerLevelChanged(HeatingElement element)
    {
      _mediator.Publish(new Handlers.HeatingElementPowerLevelChanged.Notification(element));
    }

    private void OnElementEnabledChanged()
    {
      _enabledElementMonitor.Pulse();
    }

    async Task<List<HeatingElement>> GetEnabledElementsAsync()
    {
      using (await _enabledElementMonitor.EnterAsync(_cts.Token))
      {
        while (true)
        {
          var activeElements = _byName.Values.Where(x => x.Enabled).ToList();

          if (activeElements.Count > 0)
            return activeElements;

          await _enabledElementMonitor.WaitAsync(_cts.Token);
        }
      }
    }

    async Task RunSchedulerLoopAsync()
    {
      while (!_cts.Token.IsCancellationRequested)
      {
        try
        {
          await RunSchedulerAsync();
        }
        catch (OperationCanceledException)
        {
          break;
        }
      }
    }

    async Task RunSchedulerAsync()
    {
      Console.WriteLine("RunSchedulerAsync");
      var enabledElements = await GetEnabledElementsAsync();
      var numEnabled = enabledElements.Count;
      Console.WriteLine($"numEnabled: {numEnabled}");
      if (numEnabled == 0)
      {
        return;
      }

      var maxPower = 100 / numEnabled;
      var perElementTimeSlice = _timeSlice / (double)numEnabled;

      var desiredOutputLevels = new List<(HeatingElement element, int desiredLevel)>();

      foreach (var element in enabledElements)
      {
        desiredOutputLevels.Add((element, element.GetDesiredPowerLevel()));
      }

      var remainingElements = desiredOutputLevels.Count;
      var remainingPowerBudget = 100;

      foreach (var (element, desiredLevel) in desiredOutputLevels.OrderBy(x => x.desiredLevel))
      {
        _cts.Token.ThrowIfCancellationRequested();

        Console.WriteLine($"{element.Name} desires {desiredLevel}");

        var maxOutputLevel = remainingPowerBudget / remainingElements;

        Console.WriteLine($"remaining power budget: {remainingPowerBudget}");
        Console.WriteLine($"remaining elements: {remainingElements}");
        Console.WriteLine($"max output level: {maxOutputLevel}");

        element.UpdatePowerLevel(maxOutputLevel);
        var powerLevel = element.PowerLevel;

        --remainingElements;
        remainingPowerBudget -= powerLevel;

        Console.WriteLine($"{element.Name} desired {desiredLevel}, got {powerLevel}");

        var fireTime = _timeSlice * ((double)powerLevel / 100.0);
        await element.FireForAsync(fireTime);
      }

      if (remainingPowerBudget > 0)
      {
        Console.WriteLine($"left over power budget: {remainingPowerBudget}");

        var pauseTime = _timeSlice * ((double)remainingPowerBudget / 100.0);
        await Task.Delay(pauseTime, _cts.Token);
      }

      Console.WriteLine("------------------------------------------------");
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
      await _strategyManager.StartAsync(cancellationToken);
      _schedulerLoopTask = RunSchedulerLoopAsync();
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
      _cts.Cancel();
      await _strategyManager.StopAsync(cancellationToken);
    }

    class BreweryGetStateHandler : Handlers.BreweryGetState.NotificationHandler
    {
      readonly HeatingElementManager _manager;

      public BreweryGetStateHandler(HeatingElementManager manager)
      {
        _manager = manager;
      }

      public Task Handle(Handlers.BreweryGetState.Notification notification, CancellationToken cancellationToken)
      {
        foreach (var entry in _manager._byName)
        {
          notification.State.HeatingElements[entry.Key] = entry.Value.GetState();
        }

        return Task.CompletedTask;
      }
    }

    class HeatingStrategySetParametersHandler : Handlers.HeatingStrategySetParameters.INotificationHandler
    {
      readonly HeatingElementManager _manager;

      public HeatingStrategySetParametersHandler(HeatingElementManager manager)
      {
        _manager = manager;
      }


      public Task Handle(HeatingStrategySetParameters.Notification notification, CancellationToken cancellationToken)
      {
        Handle(notification.ElementName, notification.StrategyId, notification.Parameters);
        return Task.CompletedTask;
      }

      void Handle(string elementName, Guid strategyId, JObject parameters)
      {
        var element = _manager.GetByName(elementName);
        if (element == null) return;
        var strategy = element.GetAvailableStrategy(strategyId);
        if (strategy == null) return;
        strategy.SetParameters(parameters);
      }
    }

    class HeatingElementSetStrategyHandler : Handlers.HeatingElementSetStrategy.INotificationHandler
    {
      readonly HeatingElementManager _manager;

      public HeatingElementSetStrategyHandler(HeatingElementManager manager)
      {
        _manager = manager;
      }

      public void Handle(string elementName, Guid strategyId)
      {
        var element = _manager.GetByName(elementName);
        if (element == null) return;
        var strategy = element.GetAvailableStrategy(strategyId);
        if (strategy == null) return;
        element.SetCurrentStrategy(strategy);
      }

      public Task Handle(HeatingElementSetStrategy.Notification notification, CancellationToken cancellationToken)
      {
        Handle(notification.ElementName, notification.StrategyId);
        return Task.CompletedTask;
      }
    }
  }
}
