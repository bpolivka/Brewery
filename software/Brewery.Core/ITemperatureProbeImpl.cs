﻿using System;
namespace Brewery
{ 
    public interface ITemperatureProbeImpl
    {
        double Temperature { get; }
        
        event Action TemperatureChanged;
    }
}
