﻿using Autofac;
using MediatR;
using Microsoft.Extensions.Hosting;

namespace Brewery
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<SystemTimeProvider>()
                .As<ITimeProvider>()
                .SingleInstance();
            
            builder
                .RegisterType<HeatingStrategyManager>()
                .AsSelf();

            builder
                .RegisterType<RelayManager>()
                .AsSelf()
                .As<IHostedService>()
                .SingleInstance();

            builder
                .RegisterType<TemperatureProbeManager>()
                .AsSelf()
                .As<IHostedService>()
                .SingleInstance();

            builder
                .RegisterType<HeatingElementManager>()
                .AsSelf()
                .As<IHostedService>()
                .SingleInstance();

            // Uncomment to enable polymorphic dispatching of requests, but note that
            // this will conflict with generic pipeline behaviors
            // builder.RegisterSource(new ContravariantRegistrationSource());

            // Mediator itself
            builder
                .RegisterType<Mediator>()
                .As<IMediator>()
                .InstancePerLifetimeScope();

            // request & notification handlers
            builder.Register<ServiceFactory>(context =>
            {
                var c = context.Resolve<IComponentContext>();
                return t => c.Resolve(t);
            });

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(INotificationHandler<>))
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .InstancePerLifetimeScope();
        }
    }
}
