﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using Microsoft.Extensions.Options;

namespace Brewery
{
    public class HeatingStrategyManager : IDisposable
    {
        readonly DirectoryInfo _strategiesDir;
        readonly FileSystemWatcher _fsWatcher = new FileSystemWatcher();
        readonly ILifetimeScope _parentScope;
        readonly List<StrategyEntry> _entries = new List<StrategyEntry>();

        class StrategyEntry : IDisposable
        {
            public ILifetimeScope Scope { get; set; }
            public HeatingStrategy Strategy { get; set; }
            public string ElementName { get; set; }
            public string Filename { get; set; }

            public void Dispose()
            {
                if (Scope != null)
                {
                    Scope.Dispose();
                    Scope = null;
                }
            }
        }

        public HeatingStrategyManager(IOptions<BreweryOptions> options, ILifetimeScope parentScope)
        {
            var breweryOptions = options.Value;
            _parentScope = parentScope;
            _strategiesDir = new DirectoryInfo($"{breweryOptions.ConfigurationDirectory}/strategies");
            if (!_strategiesDir.Exists)
                _strategiesDir.Create();

            _fsWatcher.Path = _strategiesDir.FullName;

            _fsWatcher.NotifyFilter =
                NotifyFilters.LastWrite |
                NotifyFilters.FileName |
                NotifyFilters.CreationTime;

            _fsWatcher.Filter = "*.csx";
            _fsWatcher.IncludeSubdirectories = true;

            _fsWatcher.Created += OnCreated;
            _fsWatcher.Deleted += OnDeleted;
            _fsWatcher.Renamed += OnRenamed;
            _fsWatcher.Changed += OnChanged;
        }

        public EventHandler<StrategyAddedEventArgs> StrategyAdded;
        public EventHandler<StrategyRemovedEventArgs> StrategyRemoved;
        public EventHandler<StrategyUpdatedEventArgs> StrategyUpdated;
        public EventHandler<StrategyCompileFailedEventArgs> StrategyCompileFailed;

        public void Dispose()
        {
            _fsWatcher.Dispose();
        }

        public IEnumerable<HeatingStrategy> GetStrategiesForElement(string elementName)
        {
            lock (_entries)
            {
                return _entries
                    .Where(x => x.ElementName == elementName)
                    .Select(x => x.Strategy)
                    .ToList();
            }
        }

        async Task<StrategyEntry> LoadAsync(FileInfo file)
        {
            var entry = new StrategyEntry
            {
                Filename = file.FullName,
                ElementName = file.Directory.Name
            };

            try
            {
                var code = await File.ReadAllTextAsync(file.FullName);

                var options = ScriptOptions.Default
                    .WithEmitDebugInformation(true)
                    .AddReferences(Assembly.GetExecutingAssembly());

                var cls = await CSharpScript.EvaluateAsync<Type>(code, options);
                entry.Scope = _parentScope.BeginLifetimeScope(builder => builder.RegisterType(cls).As<IHeatingStrategyImpl>());
                var impl = entry.Scope.Resolve<IHeatingStrategyImpl>();
                entry.Strategy = new HeatingStrategy(impl);
                return entry;
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error: {e}");
                entry.Dispose();
                var name = $"{file.Directory.Name}/{file.Name}";
                StrategyCompileFailed?.Invoke(this, new StrategyCompileFailedEventArgs(name, e.ToString()));
                throw;
            }
        }

        async Task LoadAllAsync()
        {
            foreach (var subdir in _strategiesDir.GetDirectories())
            {
                foreach (var file in subdir.GetFiles("*.csx"))
                {
                    try
                    {
                        var entry = await LoadAsync(file);
                        lock (_entries)
                        {
                            _entries.Add(entry);
                        }
                        StrategyAdded?.Invoke(this, new StrategyAddedEventArgs(entry.ElementName, entry.Strategy));
                    }
                    catch { }
                }
            }
        }

        StrategyEntry GetEntry(FileInfo file)
        {
            lock (_entries)
            {
                return _entries.FirstOrDefault(e => e.Filename == file.FullName);
            }
        }

        void RemoveStrategy(FileInfo file)
        {
            var entry = GetEntry(file);
            if (entry == null) return;
            lock (_entries)
            {
                _entries.Remove(entry);
            }
            entry.Dispose();
            StrategyRemoved?.Invoke(this, new StrategyRemovedEventArgs(entry.ElementName, entry.Strategy));
        }

        void AddStrategy(FileInfo file)
        {
            if (GetEntry(file) != null) return;

            try
            {
                var entry = LoadAsync(file).GetAwaiter().GetResult();
                lock (_entries)
                {
                    _entries.Add(entry);
                }
                StrategyAdded?.Invoke(this, new StrategyAddedEventArgs(entry.ElementName, entry.Strategy));
            }
            catch { }
        }

        void UpdateStrategy(FileInfo file)
        {
            var existing = GetEntry(file);
            if (existing == null)
            {
                AddStrategy(file);
                return;

            }
            try
            {
                var entry = LoadAsync(file).GetAwaiter().GetResult();

                entry.Strategy.SetParameters(existing.Strategy.GetParameters());

                lock (_entries)
                {
                    _entries.Remove(existing);
                    existing.Dispose();
                    _entries.Add(entry);
                }

                StrategyUpdated?.Invoke(this, new StrategyUpdatedEventArgs(entry.ElementName, existing.Strategy, entry.Strategy));
            }
            catch { }
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine($"changed: {e.ChangeType} {e.Name}");
            var file = new FileInfo(e.FullPath);
            UpdateStrategy(file);
        }

        private void OnRenamed(object sender, RenamedEventArgs e)
        {
            Console.WriteLine($"renamed: {e.ChangeType} {e.OldName} -> {e.Name}");
            RemoveStrategy(new FileInfo(e.OldFullPath));
            AddStrategy(new FileInfo(e.FullPath));
        }

        private void OnDeleted(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine($"deleted: {e.ChangeType} {e.Name}");
            RemoveStrategy(new FileInfo(e.FullPath));
        }

        private void OnCreated(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine($"created: {e.ChangeType} {e.Name}");
            AddStrategy(new FileInfo(e.FullPath));
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await LoadAllAsync();
            _fsWatcher.EnableRaisingEvents = true;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _fsWatcher.EnableRaisingEvents = false;
            return Task.CompletedTask;
        }
    }
}
