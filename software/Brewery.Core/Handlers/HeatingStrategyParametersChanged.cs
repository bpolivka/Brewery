﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery.Handlers
{
    public static class HeatingStrategyParametersChanged
    {
        public class Notification : MediatR.INotification
        {
            public Notification(HeatingElement element, HeatingStrategy strategy, JObject parameters)
            {
                Element = element;
                Strategy = strategy;
                Parameters = parameters;
            }

            public HeatingElement Element { get; }
            public HeatingStrategy Strategy { get; }
            public JObject Parameters { get; }
        }

        public interface INotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
