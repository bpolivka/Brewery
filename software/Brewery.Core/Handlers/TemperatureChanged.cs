﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery.Handlers
{
    public static class TemperatureChanged
    {
        public class Notification : MediatR.INotification
        {
            public Notification(TemperatureProbe probe)
            {
                Probe = probe;
            }

            public TemperatureProbe Probe { get; private set; }
        }

        public interface NotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
