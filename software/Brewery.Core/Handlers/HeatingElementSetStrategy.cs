﻿using System;
namespace Brewery.Handlers
{
    public static class HeatingElementSetStrategy
    {
        public class Notification : MediatR.INotification
        {
            public Notification(string elementName, Guid strategyId)
            {
                ElementName = elementName;
                StrategyId = strategyId;
            }

            public string ElementName { get; }
            public Guid StrategyId { get; }
        }

        public interface INotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
