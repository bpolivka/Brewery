﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery.Handlers
{
    public static class HeatingStrategySetParameters
    {
        public class Notification : MediatR.INotification
        {
            public Notification(string elementName, Guid strategyId, JObject parameters)
            {
                ElementName = elementName;
                StrategyId = strategyId;
                Parameters = parameters;
            }

            public string ElementName { get; }
            public Guid StrategyId { get; }
            public JObject Parameters { get; }
        }

        public interface INotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
