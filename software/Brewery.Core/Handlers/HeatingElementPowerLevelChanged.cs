﻿using System;
namespace Brewery.Handlers
{
    public static class HeatingElementPowerLevelChanged
    {
        public class Notification : MediatR.INotification
        {
            public Notification(HeatingElement element)
            {
                Element = element;
            }

            public HeatingElement Element { get; private set; }
        }

        public interface NotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
