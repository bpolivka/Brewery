﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery.Handlers
{
    public static class HeatingStrategyUpdated
    {
        public class Notification : MediatR.INotification
        {
            public Notification(HeatingElement element, HeatingStrategy oldStrategy, HeatingStrategy newStrategy)
            {
                Element = element;
                OldStrategy = oldStrategy;
                NewStrategy = newStrategy;
            }

            public HeatingElement Element { get; }
            public HeatingStrategy OldStrategy { get; }
            public HeatingStrategy NewStrategy { get; }
        }

        public interface INotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
