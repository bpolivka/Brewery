﻿using System;
namespace Brewery.Handlers
{
    public static class HeatingElementPowerLevelCalculated
    {
        public class Notification : MediatR.INotification
        {
            public Notification(HeatingElement element, HeatingStrategy strategy, PowerLevelCalculationResult result)
            {
                Element = element;
                Strategy = strategy;
                Result = result;
            }

            public HeatingElement Element { get; }
            public HeatingStrategy Strategy { get; }
            public PowerLevelCalculationResult Result { get; }
        }

        public interface NotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
