﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery.Handlers
{
    public static class HeatingStrategyRemoved
    {
        public class Notification : MediatR.INotification
        {
            public Notification(HeatingElement element, HeatingStrategy strategy)
            {
                Element = element;
                Strategy = strategy;
            }

            public HeatingElement Element { get; }
            public HeatingStrategy Strategy { get; }
        }

        public interface INotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
