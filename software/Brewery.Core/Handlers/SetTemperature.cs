﻿using System;
namespace Brewery.Handlers
{
    public static class SetTemperature
    {
        public class Notification : MediatR.INotification
        {
            public Notification(string probeName, double temperature)
            {
                ProbeName = probeName;
                Temperature = temperature;
            }

            public string ProbeName { get; }
            public double Temperature { get; }
        }

        public interface INotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
