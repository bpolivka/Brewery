﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery.Handlers
{
    public static class RelayStateChanged
    {
        public class Notification : MediatR.INotification
        {
            public Notification(Relay relay)
            {
                Relay = relay;
            }

            public Relay Relay { get; private set; }
        }

        public interface NotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
