﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery.Handlers
{
    public static class HeatingElementGetHistory
    {
        public class Request : MediatR.IRequest<Messages.HeatingElementHistoryMessage>
        {
            public Request(string element, DateTime fromTime, DateTime toTime)
            {
                Element = element;
                FromTime = fromTime;
                ToTime = toTime;
            }

            public string Element { get; }
            public DateTime FromTime { get; }
            public DateTime ToTime { get; }
        }

        public interface RequestHandler : MediatR.IRequestHandler<Request, Messages.HeatingElementHistoryMessage>
        {
        }
    }
}
