﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery.Handlers
{
    public static class HeatingStrategyCompileFailed
    {
        public class Notification : MediatR.INotification
        {
            public Notification(string name, string message)
            {
                Name = name;
                Message = message;
            }

            public string Name { get; }
            public string Message { get; }
        }

        public interface INotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
