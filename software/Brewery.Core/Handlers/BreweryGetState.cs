﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery.Handlers
{
    public static class BreweryGetState
    {
        public class Notification : MediatR.INotification
        {
            public Notification(Messages.BreweryState state)
            {
                State = state;
            }

            public Messages.BreweryState State { get; private set; }
        }

        public interface NotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
