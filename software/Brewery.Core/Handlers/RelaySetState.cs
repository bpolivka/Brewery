﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery.Handlers
{
    public static class RelaySetState
    {
        public class Notification : MediatR.INotification
        {
            public Notification(string relay, bool enabled)
            {
                Relay = relay;
                Enabled = enabled;
            }

            public string Relay { get; private set; }
            public bool Enabled { get; private set; }
        }

        public interface NotificationHandler : MediatR.INotificationHandler<Notification>
        {
        }
    }
}
