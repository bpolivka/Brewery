﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery.Handlers
{
    public static class TemperatureProbeGetHistory
    {
        public class Request : MediatR.IRequest<Messages.TemperatureProbeHistoryMessage>
        {
            public Request(string probe, DateTime fromTime, DateTime toTime)
            {
                Probe = probe;
                FromTime = fromTime;
                ToTime = toTime;
            }

            public string Probe { get; }
            public DateTime FromTime { get; }
            public DateTime ToTime { get; }
        }

        public interface RequestHandler : MediatR.IRequestHandler<Request, Messages.TemperatureProbeHistoryMessage>
        {
        }
    }
}
