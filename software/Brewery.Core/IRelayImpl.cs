﻿namespace Brewery
{
    public interface IRelayImpl
    {
        void SetState(bool enabled);
    }
}