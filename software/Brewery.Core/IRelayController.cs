﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery
{
    public interface IRelayController
    {
        IRelayImpl CreateImpl(string name);

        Task StartAsync(CancellationToken cancellationToken);
        Task StopAsync(CancellationToken cancellationToken);
    }
}
