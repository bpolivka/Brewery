﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Brewery
{
    public class Relay
    {
        readonly IRelayImpl _impl;
        bool _enabled = false;
        List<Relay> _dependsOn = new List<Relay>();
        List<Relay> _conflictsWith = new List<Relay>();

        public Relay(string name, IRelayImpl impl)
        {
            Name = name;
            _impl = impl;
        }

        public string Name { get; }

        public event Action EnabledChanged;

        public bool Enabled
        {
            get => _enabled;
            set
            {
                if (value == _enabled)
                    return;

                if (!value || CanEnable)
                {
                    _enabled = value;
                    _impl.SetState(value);
                    EnabledChanged?.Invoke();
                }
            }
        }

        bool CanEnable => _dependsOn.All(r => r.Enabled) && !_conflictsWith.Any(r => r.Enabled);

        public Relay DependsOn(Relay other)
        {
            _dependsOn.Add(other);

            other.EnabledChanged += () =>
            {
                // if we're enabled and the relay we depend on has gone disabled, we go disabled
                if (_enabled && !other.Enabled)
                {
                    Enabled = false;
                }
            };

            return this;
        }

        public Relay ConflictsWith(Relay other)
        {
            _conflictsWith.Add(other);

            return this;
        }
    }
}
