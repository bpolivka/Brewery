﻿using System;
using Newtonsoft.Json.Linq;
using System.Text.Json;

namespace Brewery
{
    public class HeatingStrategy
    {
        readonly IHeatingStrategyImpl _impl;

        public HeatingStrategy(IHeatingStrategyImpl impl)
        {
            _impl = impl;
        }

        public string Name => _impl.Name;
        public string Type => _impl.Type;

        public Guid Id { get; } = Guid.NewGuid();

        public PowerLevelCalculationResult CalcPowerLevel(int limit, bool dryRun) =>
          _impl.CalcPowerLevel(limit, dryRun);

        public JObject GetParameters() => _impl.GetParameters();

        public void SetParameters(JObject parameters)
        {
            _impl.SetParameters(parameters);
            ParametersChanged?.Invoke(this);
        }

        public Messages.HeatingStrategyState GetState()
        {
            return new Messages.HeatingStrategyState
            {
                Id = Id,
                Name = _impl.Name,
                Type = _impl.Type,
                Parameters = JsonDocument.Parse(_impl.GetParameters().ToString()).RootElement
            };
        }

        public event Action<HeatingStrategy> ParametersChanged;
    }
}
