﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Brewery
{
    public class TemperatureProbeManager : IHostedService, IDisposable
    {
        ITemperatureProbeController _controller;
        IMediator _mediator;
        Dictionary<string, TemperatureProbe> _probesByName = new Dictionary<string, TemperatureProbe>();
        readonly FileSystemWatcher _fsWatcher = new FileSystemWatcher();
        private readonly FileInfo _confFile;

        private const string ConfigFileName = "temperature_probes.json";
        
        public TemperatureProbeManager(IOptions<BreweryOptions> breweryOptions, ITemperatureProbeController controller, IMediator mediator)
        {
            _mediator = mediator;

            _controller = controller;

            Hlt = CreateProbe("hlt");
            Boil = CreateProbe("boil");
            MltIn = CreateProbe("mltIn");
            MltOut = CreateProbe("mltOut");

            _confFile = new FileInfo($"{breweryOptions.Value.ConfigurationDirectory}/{ConfigFileName}");
            
            _fsWatcher.Path = _confFile.DirectoryName;
            _fsWatcher.Filter = _confFile.Name;

            _fsWatcher.NotifyFilter =
                NotifyFilters.LastWrite |
                NotifyFilters.CreationTime;

            _fsWatcher.Created += OnProbesFileCreated;
            _fsWatcher.Changed += OnProbesFileChanged;
            
            UpdateProbesConfiguration();
        }

        void UpdateProbesConfiguration()
        {
            if (_confFile.Exists)
            {
                var configObj = JObject.Parse(File.ReadAllText(_confFile.FullName));
                UpdateProbesConfiguration(configObj);
            }
        }

        void UpdateProbesConfiguration(JObject conf)
        {
            foreach (var prop in conf.Properties())
            {
                var probe = GetProbeNamed(prop.Name);
                if (probe == null) continue;
                probe.UpdateConfiguration(prop.Value.Value<JObject>());
            }
        }
        
        private void OnProbesFileChanged(object sender, FileSystemEventArgs e)
        {
            UpdateProbesConfiguration();
        }

        private void OnProbesFileCreated(object sender, FileSystemEventArgs e)
        {
            UpdateProbesConfiguration();
        }

        public TemperatureProbe GetProbeNamed(string name)
        {
            _probesByName.TryGetValue(name, out var probe);
            return probe;
        }

        private void OnTemperatureChanged(TemperatureChangedEventArgs e)
        {
            var notification = new Handlers.TemperatureChanged.Notification(e.Probe);
            _mediator.Publish(notification);
        }

        TemperatureProbe CreateProbe(string name)
        {
            var probeImpl = _controller.CreateProbeImpl();
            var probe = new TemperatureProbe(name, probeImpl);
            probe.TemperatureChanged += () => OnTemperatureChanged(new TemperatureChangedEventArgs { Probe = probe });
            _probesByName[name] = probe;
            return probe;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _controller.StartAsync(cancellationToken);
            _fsWatcher.EnableRaisingEvents = true;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            _fsWatcher.EnableRaisingEvents = false;
            await _controller.StopAsync(cancellationToken);
        }

        public TemperatureProbe Hlt { get; }
        public TemperatureProbe MltIn { get; }
        public TemperatureProbe MltOut { get; }
        public TemperatureProbe Boil { get; }
        
        class BreweryGetStateHandler : Handlers.BreweryGetState.NotificationHandler
        {
            readonly TemperatureProbeManager _manager;

            public BreweryGetStateHandler(TemperatureProbeManager manager)
            {
                _manager = manager;
            }

            public Task Handle(Handlers.BreweryGetState.Notification notification, CancellationToken cancellationToken)
            {
                foreach (var entry in _manager._probesByName)
                {
                    notification.State.TemperatureProbes[entry.Key] = new Messages.TemperatureProbeState
                    {
                        Temperature = entry.Value.Temperature
                    };
                }

                return Task.CompletedTask;
            }
        }

        public void Dispose()
        {
            _fsWatcher.Dispose();
        }
    }
}
