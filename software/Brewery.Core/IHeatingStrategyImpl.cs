﻿using System;
using Newtonsoft.Json.Linq;

namespace Brewery
{
    public interface IHeatingStrategyImpl
    {
        string Name { get; }
        string Type { get; }
        JObject GetParameters();
        void SetParameters(JObject parameters);
        PowerLevelCalculationResult CalcPowerLevel(int limit, bool dryRun);
    }
}
