using System.Collections.Generic;

namespace Brewery
{
    public class PowerLevelCalculationResult
    {
        public int PowerLevel { get; set; }
        public Dictionary<string, object> Detail { get; } = new Dictionary<string, object>();
    }
}