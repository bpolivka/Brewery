﻿namespace Brewery
{
    public class StrategyRemovedEventArgs
    {
        public StrategyRemovedEventArgs(string elementName, HeatingStrategy strategy)
        {
            ElementName = elementName;
            Strategy = strategy;
        }

        public string ElementName { get; }
        public HeatingStrategy Strategy { get; }
    }
}