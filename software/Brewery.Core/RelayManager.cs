﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;
using MediatR;
using Microsoft.Extensions.Hosting;

namespace Brewery
{
    public class RelayManager : IHostedService
    {
        IRelayController _controller;
        IMediator _mediator;
        Dictionary<string, Relay> _relaysByName = new Dictionary<string, Relay>();

        public RelayManager(IRelayController controller, IMediator mediator)
        {
            _mediator = mediator;

            _controller = controller;

            Hlt = CreateRelay("hlt");
            Boil = CreateRelay("boil");
            Pump1 = CreateRelay("pump1");
            Pump2 = CreateRelay("pump2");
            Stir = CreateRelay("stir");
            Fan = CreateRelay("fan");
            Alarm = CreateRelay("alarm");
            Tbd = CreateRelay("tbd");
            HltSsr = CreateRelay("hltSsr");
            BoilSsr = CreateRelay("boilSsr");

            HltSsr.DependsOn(Hlt).ConflictsWith(BoilSsr);
            BoilSsr.DependsOn(Boil).ConflictsWith(HltSsr);
        }

        public Relay GetRelayNamed(string name)
        {
            _relaysByName.TryGetValue(name, out var relay);
            return relay;
        }

        private void OnRelayStateChanged(Relay relay)
        {
            var notification = new Handlers.RelayStateChanged.Notification(relay);
            _mediator.Publish(notification);
        }

        Relay CreateRelay(string name)
        {
            var impl = _controller.CreateImpl(name);
            var relay = new Relay(name, impl);
            _relaysByName[name] = relay;
            relay.EnabledChanged += () => OnRelayStateChanged(relay);
            return relay;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return _controller.StartAsync(cancellationToken);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return _controller.StopAsync(cancellationToken);
        }

        public Relay Hlt { get; private set; }
        public Relay Boil { get; private set; }
        public Relay Pump1 { get; private set; }
        public Relay Pump2 { get; private set; }
        public Relay Stir { get; private set; }
        public Relay Fan { get; private set; }
        public Relay Alarm { get; private set; }
        public Relay Tbd { get; private set; }
        public Relay HltSsr { get; private set; }
        public Relay BoilSsr { get; private set; }


        class BreweryGetStateHandler : Handlers.BreweryGetState.NotificationHandler
        {
            readonly RelayManager _manager;

            public BreweryGetStateHandler(RelayManager manager)
            {
                _manager = manager;
            }

            public Task Handle(Handlers.BreweryGetState.Notification notification, CancellationToken cancellationToken)
            {
                foreach (var entry in _manager._relaysByName)
                {
                    notification.State.Relays[entry.Key] = new Messages.RelayState
                    {
                        Enabled = entry.Value.Enabled
                    };
                }

                return Task.CompletedTask;
            }
        }

        class RelaySetStateHandler : Handlers.RelaySetState.NotificationHandler
        {
            readonly RelayManager _manager;

            public RelaySetStateHandler(RelayManager manager)
            {
                _manager = manager;
            }


            public Task Handle(RelaySetState.Notification notification, CancellationToken cancellationToken)
            {
                var relay = _manager.GetRelayNamed(notification.Relay);
                relay.Enabled = notification.Enabled;
                return Task.CompletedTask;
            }
        }
    }
}
