﻿using System;
using Vibrant.InfluxDB.Client;

namespace Brewery.Controller.DataCollection
{
    [InfluxMeasurement("power_levels")]
    class PowerLevelMeasurement
    {
        [InfluxTimestamp]
        public DateTime Time { get; set; }
        [InfluxTag("element")]
        public string Element { get; set; }
        [InfluxField("value")]
        public int Value { get; set; }
    }
}
