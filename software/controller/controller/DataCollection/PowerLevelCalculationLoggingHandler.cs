﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;
using Microsoft.VisualStudio.Web.CodeGenerators.Mvc.Controller;
using Vibrant.InfluxDB.Client;

namespace Brewery.Controller.DataCollection
{
    public class PowerLevelCalculationLoggingHandler : Handlers.HeatingElementPowerLevelCalculated.NotificationHandler
    {
        private readonly IInfluxClient _influxClient;

        public PowerLevelCalculationLoggingHandler(IInfluxClient influxDBClient)
        {
            _influxClient = influxDBClient;
        }

        public Task Handle(HeatingElementPowerLevelCalculated.Notification notification, CancellationToken cancellationToken)
        {
            var measurement = new Vibrant.InfluxDB.Client.Rows.DynamicInfluxRow();
            measurement.Tags["element"] = notification.Element.Name;
            measurement.Tags["strategy"] = notification.Strategy.Name;
            measurement.Fields["power_level"] = notification.Result.PowerLevel;
            foreach (var kv in notification.Result.Detail)
            {
                measurement.Fields[kv.Key] = kv.Value;
            }

            return _influxClient.WriteAsync("brewery", "strategy_calcs", new[] { measurement });
        }
    }
}
