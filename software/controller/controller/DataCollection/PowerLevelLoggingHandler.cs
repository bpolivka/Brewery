﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;
using Vibrant.InfluxDB.Client;

namespace Brewery.Controller.DataCollection
{
    public class PowerLevelLoggingHandler : Handlers.HeatingElementPowerLevelChanged.NotificationHandler
    {
        private readonly IInfluxClient _influxClient;

        public PowerLevelLoggingHandler(IInfluxClient influxDBClient)
        {
            _influxClient = influxDBClient;
        }

        public Task Handle(HeatingElementPowerLevelChanged.Notification notification, CancellationToken cancellationToken)
        {
            var measurement = new PowerLevelMeasurement
            {
                Element = notification.Element.Name,
                Value = notification.Element.PowerLevel,
                Time = DateTime.UtcNow
            };

            return _influxClient.WriteAsync("brewery", new[] { measurement });
        }
    }
}
