﻿using System;
using Vibrant.InfluxDB.Client;

namespace Brewery.Controller.DataCollection
{

    [InfluxMeasurement("temperatures")]
    class TemperatureMeasurement
    {
        [InfluxTimestamp]
        public DateTime Time { get; set; }
        [InfluxTag("probe")]
        public string Probe { get; set; }
        [InfluxField("value")]
        public double Value { get; set; }
    }
}
