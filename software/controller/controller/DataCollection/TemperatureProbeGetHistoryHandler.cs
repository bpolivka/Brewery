﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;
using Brewery.Messages;
using Vibrant.InfluxDB.Client;

namespace Brewery.Controller.DataCollection
{
    public class TemperatureProbeGetHistoryHandler : Handlers.TemperatureProbeGetHistory.RequestHandler
    {
        private readonly IInfluxClient _influxClient;

        public TemperatureProbeGetHistoryHandler(IInfluxClient influxDBClient)
        {
            _influxClient = influxDBClient;
        }

        public async Task<TemperatureProbeHistoryMessage> Handle(TemperatureProbeGetHistory.Request request, CancellationToken cancellationToken)
        {
            var resultSet = await _influxClient.ReadAsync<TemperatureMeasurement>(
                "brewery",
                @"
                select
                    mean(value) as value
                from
                    temperatures
                where
                    probe=$probe and
                    time>=$fromTime and time <=$toTime
                group by
                    time(250ms) fill(previous)",
                new { probe = request.Probe, fromTime = request.FromTime, toTime = request.ToTime })
                .ConfigureAwait(false);

            var msg = new TemperatureProbeHistoryMessage();

            if (resultSet.Results.Count > 0 && resultSet.Results[0].Series.Count > 0)
            {
                var series = resultSet.Results[0].Series[0];

                var records = series.Rows
                    .SkipWhile(r => r.Value == 0.0)
                    .Select(r => new TemperatureHistoryRecord { Time = r.Time, Temperature = r.Value });

                msg.Records.AddRange(records);
            }

            return msg;
        }
    }
}
