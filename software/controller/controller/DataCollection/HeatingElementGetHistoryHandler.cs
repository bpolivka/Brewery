﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;
using Brewery.Messages;
using Vibrant.InfluxDB.Client;

namespace Brewery.Controller.DataCollection
{
    public class HeatingElementGetHistoryHandler : Handlers.HeatingElementGetHistory.RequestHandler
    {
        private readonly IInfluxClient _influxClient;

        public HeatingElementGetHistoryHandler(IInfluxClient influxDBClient)
        {
            _influxClient = influxDBClient;
        }

       
        public async Task<HeatingElementHistoryMessage> Handle(HeatingElementGetHistory.Request request, CancellationToken cancellationToken)
        {
            var resultSet = await _influxClient.ReadAsync<PowerLevelMeasurement>(
                "brewery",
                @"
                select
                    mean(value) as value
                from
                    power_levels
                where
                    element=$element and
                    time>=$fromTime and time <=$toTime
                group by
                    time(250ms) fill(previous)",
                new { element = request.Element, fromTime = request.FromTime, toTime = request.ToTime })
                .ConfigureAwait(false);

            var msg = new HeatingElementHistoryMessage();

            if (resultSet.Results.Count > 0 && resultSet.Results[0].Series.Count > 0)
            {
                var series = resultSet.Results[0].Series[0];

                var records = series.Rows
                    .SkipWhile(r => r.Value == 0.0)
                    .Select(r => new HeatingElementHistoryRecord { Time = r.Time, PowerLevel = r.Value });

                msg.Records.AddRange(records);
            }

            return msg;
        }
    }
}
