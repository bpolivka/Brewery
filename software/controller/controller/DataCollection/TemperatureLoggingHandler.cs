﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;
using Vibrant.InfluxDB.Client;

namespace Brewery.Controller.DataCollection
{
    public class TemperatureLoggingHandler : Handlers.TemperatureChanged.NotificationHandler
    {
        private readonly IInfluxClient _influxClient;

        public TemperatureLoggingHandler(IInfluxClient influxDBClient)
        {
            _influxClient = influxDBClient;
        }

        public Task Handle(TemperatureChanged.Notification notification, CancellationToken cancellationToken)
        {
            var measurement = new TemperatureMeasurement
            {
                Probe = notification.Probe.Name,
                Value = notification.Probe.Temperature,
                Time = DateTime.UtcNow
            };

            return _influxClient.WriteAsync("brewery", new[] { measurement });
        }
    }
}
