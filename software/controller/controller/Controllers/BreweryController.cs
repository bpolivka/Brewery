﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Brewery.Controller.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BreweryController : ControllerBase
    {
        IMediator _mediator;

        public BreweryController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [Route("state")]
        public async Task<Messages.BreweryState> GetBreweryState()
        {
            var msg = new Messages.BreweryState();
            await _mediator.Publish(new Handlers.BreweryGetState.Notification(msg));
            return msg;
        }

        [HttpPost("relay/{name}/state")]
        public async Task SetRelayState(string name, Messages.RelayState state)
        {
            await _mediator.Publish(new Handlers.RelaySetState.Notification(name, state.Enabled));
        }
    }
}
