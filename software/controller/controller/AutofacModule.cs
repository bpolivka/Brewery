﻿using System;
using System.Device.Gpio;
using System.Device.Spi;
using System.Runtime.InteropServices;
using System.Threading;
using Autofac;
using Brewery.Controller.Simulator;
using MediatR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Vibrant.InfluxDB.Client;

namespace Brewery.Controller
{
    public class AutofacModule : Module
    {
        public bool UseSimulator { get; set; }
        
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new Brewery.AutofacModule());

            builder
                .Register(ctx =>
                {
                    var options = ctx.Resolve<IOptions<InfluxDbOptions>>().Value;
                    return new InfluxClient(options.Uri);
                })
                .OnActivated(args => InitializeInfluxDB(args.Instance))
                .AutoActivate()
                .As<IInfluxClient>()
                .SingleInstance();


            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(INotificationHandler<>))
                .SingleInstance();

            builder.RegisterAssemblyTypes(ThisAssembly)
                .AsClosedTypesOf(typeof(IRequestHandler<,>))
                .SingleInstance();
            
            if (UseSimulator)
            {
                builder.RegisterModule(new Simulator.AutofacModule());
            }
            else
            {
                builder.RegisterModule(new Gpio.AutofacModule());
            }
        }

        void InitializeInfluxDB(IInfluxClient client)
        {
            while (true)
            {
                try
                {
                    client.PingAsync().Wait();
                    break;
                }
                catch (Exception e)
                {
                    Thread.Sleep(1000);
                }
            }
            client.CreateDatabaseAsync("brewery").Wait();
        }
    }
}
