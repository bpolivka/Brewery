﻿using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json.Linq;

namespace Brewery.Controller.Hubs
{
    public class BreweryHub : Hub
    {
        private readonly IMediator _mediator;

        public BreweryHub(IMediator mediator)
        {
            this._mediator = mediator;
        }

        public async Task<Messages.BreweryState> GetBreweryState()
        {
            var msg = new Messages.BreweryState();
            await _mediator.Publish(new Handlers.BreweryGetState.Notification(msg));
            return msg;
        }

        public Task<Messages.TemperatureProbeHistoryMessage> GetTemperatureProbeHistory(
            string probe, DateTime fromTime, DateTime toTime)
        {
            return _mediator.Send(new Handlers.TemperatureProbeGetHistory.Request(probe, fromTime, toTime));
        }

        public Task<Messages.HeatingElementHistoryMessage> GetHeatingElementHistory(
            string element, DateTime fromTime, DateTime toTime)
        {
            return _mediator.Send(new Handlers.HeatingElementGetHistory.Request(element, fromTime, toTime));
        }

        public async Task SetRelayState(string relayName, bool enabled)
        {
            await _mediator.Publish(new Handlers.RelaySetState.Notification(relayName, enabled));
        }

        public async Task SetTemperature(string probeName, double temperature)
        {
            await _mediator.Publish(new Handlers.SetTemperature.Notification(probeName, temperature));
        }
        
        public Task SetHeatingStrategyParameters(string elementName, Guid strategyId, JsonElement parameters)
        {
            var parametersJobj = JObject.Parse(parameters.ToString());
            return _mediator.Publish(new Handlers.HeatingStrategySetParameters.Notification(elementName, strategyId, parametersJobj));
        }

        public Task SetHeatingElementStrategy(string elementName, Guid strategyId)
        {
            return _mediator.Publish(new Handlers.HeatingElementSetStrategy.Notification(elementName, strategyId));
        }

        class HubNotifiationHandler :
            Handlers.RelayStateChanged.NotificationHandler,
            Handlers.TemperatureChanged.NotificationHandler,
            Handlers.HeatingElementPowerLevelChanged.NotificationHandler,
            Handlers.HeatingStrategyParametersChanged.INotificationHandler,
            Handlers.HeatingElementStrategyChanged.INotificationHandler,
            Handlers.HeatingStrategyAdded.INotificationHandler,
            Handlers.HeatingStrategyRemoved.INotificationHandler,
            Handlers.HeatingStrategyUpdated.INotificationHandler,
            Handlers.HeatingStrategyCompileFailed.INotificationHandler
        {
            private readonly IHubContext<BreweryHub> _context;

            public HubNotifiationHandler(IHubContext<BreweryHub> context)
            {
                _context = context;
            }

            public Task Handle(RelayStateChanged.Notification notification, CancellationToken cancellationToken)
            {
                return _context.Clients.All.SendAsync("RelayStateChanged", notification.Relay.Name, notification.Relay.Enabled);
            }

            public Task Handle(TemperatureChanged.Notification notification, CancellationToken cancellationToken)
            {
                return _context.Clients.All.SendAsync("TemperatureChanged", notification.Probe.Name, notification.Probe.Temperature);
            }

            public Task Handle(HeatingElementPowerLevelChanged.Notification notification, CancellationToken cancellationToken)
            {
                return _context.Clients.All.SendAsync("HeatingElementPowerLevelChanged", notification.Element.Name, notification.Element.PowerLevel);
            }

            public Task Handle(HeatingStrategyParametersChanged.Notification notification, CancellationToken cancellationToken)
            {
                var json = JsonDocument.Parse(notification.Parameters.ToString());
                return _context.Clients.All.SendAsync(
                    "HeatingStrategyParametersChanged", notification.Element.Name, notification.Strategy.Id, json.RootElement);
            }

            public Task Handle(HeatingElementStrategyChanged.Notification notification, CancellationToken cancellationToken)
            {
                return _context.Clients.All.SendAsync("HeatingElementStrategyChanged", notification.Element.Name, notification.Strategy.Id);
            }

            public Task Handle(HeatingStrategyAdded.Notification notification, CancellationToken cancellationToken)
            {
                return _context.Clients.All.SendAsync(
                    "HeatingStrategyAdded", notification.Element.Name, notification.Strategy.GetState());
            }

            public Task Handle(HeatingStrategyRemoved.Notification notification, CancellationToken cancellationToken)
            {
                return _context.Clients.All.SendAsync(
                    "HeatingStrategyRemoved", notification.Element.Name, notification.Strategy.Id);
            }

            public Task Handle(HeatingStrategyUpdated.Notification notification, CancellationToken cancellationToken)
            {
                return _context.Clients.All.SendAsync(
                    "HeatingStrategyUpdated", notification.Element.Name, notification.OldStrategy.Id, notification.NewStrategy.GetState());
            }

            public Task Handle(HeatingStrategyCompileFailed.Notification notification, CancellationToken cancellationToken)
            {
                return _context.Clients.All.SendAsync(
                    "HeatingStrategyCompileFailed", notification.Name, notification.Message);
            }
        }
    }
}
