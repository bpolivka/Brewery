﻿using Autofac;
using Microsoft.Extensions.Hosting;

namespace Brewery.Controller.Simulator
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new Simulation.AutofacModule());
            
            builder
                .RegisterType<Simulator.SimulatedBrewery>()
                .As<IHostedService>()
                .SingleInstance();
        }
    }
}
