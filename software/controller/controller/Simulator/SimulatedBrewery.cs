using System;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Simulation;
using Microsoft.Extensions.Hosting;
using UnitsNet;

namespace Brewery.Controller.Simulator
{
    public class SimulatedBrewery : IHostedService
    {
        private readonly HeatingElementManager _heatingElementManager;
        private readonly TemperatureProbe _hltProbe;
        private readonly TemperatureProbe _mltInProbe;
        private readonly TemperatureProbe _mltOutProbe;
        private readonly TemperatureProbe _boilProbe;
        private readonly SimulatedTemperatureProbeController _temperatureProbeController;
        private readonly Timer _timer;
        
        private Kettle _hlt;
        private Kettle _boil;
        private readonly Temperature _ambientTemperature;
        private DateTime _lastUpdate;
        private bool _hltHeating = false;
        private bool _boilHeating = false;
        private Power _elementPower = Power.FromWatts(5000.0);
        private double _heatingEfficiency = 0.98;
        private double _heatLossFactor = 0.82;

        public SimulatedBrewery(
            HeatingElementManager heatingElementManager, 
            TemperatureProbeManager temperatureProbeManager,
            SimulatedTemperatureProbeController temperatureProbeController)
        {
            _heatingElementManager = heatingElementManager;
            _temperatureProbeController = temperatureProbeController;

            _hltProbe = temperatureProbeManager.Hlt;
            _mltInProbe = temperatureProbeManager.MltIn;
            _mltOutProbe = temperatureProbeManager.MltOut;
            _boilProbe = temperatureProbeManager.Boil;
            
            _ambientTemperature = Temperature.FromDegreesFahrenheit(67.0);
            
            _timer = new Timer(OnTimerExpired, null, TimeSpan.FromMilliseconds(-1), TimeSpan.FromMilliseconds(-1));
            
            _hlt = new Kettle(
                diameter: Length.FromInches(16.125),
                volume: Volume.FromUsGallons(13.0),
                initalTemperature: Temperature.FromDegreesFahrenheit(70.0),
                density: Density.FromKilogramsPerLiter(1.0));

            _boil = new Kettle(
                diameter: Length.FromInches(16.125),
                volume: Volume.FromUsGallons(13.0),
                initalTemperature: Temperature.FromDegreesFahrenheit(70.0),
                density: Density.FromKilogramsPerLiter(1.0));
        }

        private void OnTimerExpired(object state)
        {
            UpdateTemperatures();
            _timer.Change(TimeSpan.FromMilliseconds(500), TimeSpan.FromMilliseconds(-1));
        }

        private void UpdateTemperatures()
        {
            var now = DateTime.Now;
            var dt = now - _lastUpdate;

            if (_hltHeating)
            {
                _hlt.Heat(_elementPower, dt, _heatingEfficiency);
            }

            _hlt.Cool(dt, _ambientTemperature, _heatLossFactor);

            var hltTemp = _hlt.Temperature.DegreesFahrenheit;
            _temperatureProbeController.SetTemperature(_hltProbe, hltTemp);
            _temperatureProbeController.SetTemperature(_mltInProbe, hltTemp - 0.1);
            _temperatureProbeController.SetTemperature(_mltOutProbe, hltTemp = hltTemp - 0.2);
            
            if (_boilHeating)
            {
                _boil.Heat(_elementPower, dt, _heatingEfficiency);
            }

            _boil.Cool(dt, _ambientTemperature, _heatLossFactor);

            _temperatureProbeController.SetTemperature(_boilProbe, _boil.Temperature.DegreesFahrenheit);
            
            _lastUpdate = now;
            _hltHeating = _heatingElementManager.Hlt.Relay.Enabled;
            _boilHeating = _heatingElementManager.Boil.Relay.Enabled;
        }
        
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _lastUpdate = DateTime.Now;
            _timer.Change(TimeSpan.Zero, TimeSpan.FromMilliseconds(-1));

            _heatingElementManager.Hlt.Relay.EnabledChanged += UpdateTemperatures;
            _heatingElementManager.Boil.Relay.EnabledChanged += UpdateTemperatures;
            return Task.CompletedTask;
        }

        public async  Task StopAsync(CancellationToken cancellationToken)
        {
            _heatingElementManager.Hlt.Relay.EnabledChanged -= UpdateTemperatures;
            _heatingElementManager.Boil.Relay.EnabledChanged -= UpdateTemperatures;
            await _timer.DisposeAsync();
        }
    }
}