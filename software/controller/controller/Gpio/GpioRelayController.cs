﻿using System;
using System.Collections.Generic;
using System.Device.Gpio;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery.Controller.Gpio
{
    class GpioRelayController : IRelayController
    {
        Dictionary<string, IRelayImpl> Impls = new Dictionary<string, IRelayImpl>();

        public GpioRelayController(GpioController gpioController)
        {
            Impls["hlt"] = new RelayImpl(gpioController, 24, false);
            Impls["boil"] = new RelayImpl(gpioController, 25, false);
            Impls["pump1"] = new RelayImpl(gpioController, 13, false);
            Impls["pump2"] = new RelayImpl(gpioController, 16, false);
            Impls["stir"] = new RelayImpl(gpioController, 19, false);
            Impls["fan"] = new RelayImpl(gpioController, 20, false);
            Impls["tbd"] = new RelayImpl(gpioController, 21, false);
            Impls["alarm"] = new RelayImpl(gpioController, 26, false);
            Impls["hltSsr"] = new RelayImpl(gpioController, 4, true);
            Impls["boilSsr"] = new RelayImpl(gpioController, 14, true);
        }

        public IRelayImpl CreateImpl(string name)
        {
            return Impls[name];
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        class RelayImpl : IRelayImpl
        {
            GpioController _gpioController;
            int _pinNum;
            bool _activeHigh;

            public RelayImpl(GpioController controller, int pinNum, bool activeHigh)
            {
                _gpioController = controller;
                _pinNum = pinNum;
                _activeHigh = activeHigh;

                _gpioController.OpenPin(pinNum, PinMode.Output);
                _gpioController.Write(pinNum, OffValue);
            }

            PinValue OnValue => _activeHigh ? PinValue.High : PinValue.Low;
            PinValue OffValue => _activeHigh ? PinValue.Low : PinValue.High;

            public void SetState(bool enabled)
            {
                var pinValue = enabled ? OnValue : OffValue;
                _gpioController.Write(_pinNum, pinValue);
            }
        }
    }
}
