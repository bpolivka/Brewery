﻿using System.Device.Gpio;
using System.Device.Gpio.Drivers;
using System.Device.Spi;
using Autofac;
using Microsoft.Extensions.Hosting;

namespace Brewery.Controller.Gpio
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .Register(ctx =>
                {
                    var driver = new RaspberryPi3Driver();
                    return new GpioController(PinNumberingScheme.Logical, driver);
                })
                .SingleInstance();

            builder
                .Register(ctx => SpiDevice.Create(new SpiConnectionSettings(0, 0)
                {
                }))
                .SingleInstance();

            builder
                .RegisterType<Gpio.GpioRelayController>()
                .As<IRelayController>()
                .SingleInstance();

            builder
                .RegisterType<Gpio.GpioTemperatureProbeController>()
                .As<ITemperatureProbeController>()
                .SingleInstance();

            builder
                .RegisterType<Gpio.GpioButtonController>()
                .As<IHostedService>()
                .SingleInstance();
        }
    }
}
