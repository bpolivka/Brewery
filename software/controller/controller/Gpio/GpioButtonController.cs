﻿using System;
using System.Device.Gpio;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace Brewery.Controller.Gpio
{
    class GpioButtonController : IHostedService
    {
        struct ButtonDescriptor
        {
            public string Name;
            public int Pin;
        }

        static ButtonDescriptor[] ButtonDescriptors =
        {
            new ButtonDescriptor { Name = "hlt", Pin = 15 },
            new ButtonDescriptor { Name = "boil", Pin = 18 },
            new ButtonDescriptor { Name = "pump1", Pin = 17 },
            new ButtonDescriptor { Name = "pump2", Pin = 27 },
            new ButtonDescriptor { Name = "stir", Pin = 23 },
            new ButtonDescriptor { Name = "fan", Pin = 22 }
        };

        private readonly GpioController _gpioController;
        private readonly RelayManager _relayManager;

        public GpioButtonController(
            GpioController gpioController,
            RelayManager relayManager)
        {
            _gpioController = gpioController;
            _relayManager = relayManager;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            foreach (var descriptor in ButtonDescriptors)
            {
                var relay = _relayManager.GetRelayNamed(descriptor.Name);

                _gpioController.OpenPin(descriptor.Pin, PinMode.InputPullUp);

                _gpioController.RegisterCallbackForPinValueChangedEvent(
                    descriptor.Pin, PinEventTypes.Falling,
                    (sender, args) => relay.Enabled = !relay.Enabled);

            }

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
