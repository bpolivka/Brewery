﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Device.Spi;
using System.Device.Gpio;
using System.Threading.Tasks;
using Brewery.Handlers;
using System.Collections.Generic;
using Nito.AsyncEx;
using System.Reactive.Subjects;
using System.Reactive.Linq;
using System.Linq;

namespace Brewery.Controller.Gpio
{
    public class GpioTemperatureProbeController : ITemperatureProbeController
    {
        const double VoltsPerTick = 2.5 / 4096.0;

        Lazy<TemperatureProbeManager> _manager;
        GpioController _gpioController;
        SpiDevice _spiDevice;
        Thread _readThread;
        CancellationTokenSource _cts = new CancellationTokenSource();
        readonly AsyncAutoResetEvent _eocEvent = new AsyncAutoResetEvent();


        List<TemperatureProbe> _probes = new List<TemperatureProbe>();

        public GpioTemperatureProbeController(
            Lazy<TemperatureProbeManager> manager,
            GpioController gpioController, 
            SpiDevice spiDevice)
        {
            _manager = manager;
            _gpioController = gpioController;
            _spiDevice = spiDevice;

            _readThread = new Thread(RunLoop);
            _readThread.Priority = ThreadPriority.Lowest;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _probes.Add(_manager.Value.GetProbeNamed("hlt") as TemperatureProbe);
            _probes.Add(_manager.Value.GetProbeNamed("mltIn") as TemperatureProbe);
            _probes.Add(_manager.Value.GetProbeNamed("mltOut") as TemperatureProbe);
            _probes.Add(_manager.Value.GetProbeNamed("boil") as TemperatureProbe);

            _gpioController.OpenPin(12, PinMode.InputPullUp);

            _gpioController.RegisterCallbackForPinValueChangedEvent(
                12, PinEventTypes.Falling,
                onEocPinValueChanged);

            _readThread.Start();
            return Task.CompletedTask;
        }

        private void onEocPinValueChanged(object sender, PinValueChangedEventArgs args)
        {
            if (args.ChangeType == PinEventTypes.Falling)
            {
                _eocEvent.Set();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _cts.Cancel();
            _readThread.Join();
            return Task.CompletedTask;
        }
        
        public ITemperatureProbeImpl CreateProbeImpl()
        {
            return new TemperatureProbeImpl();
        }

        void InitializeAdc()
        {
            // Reset
            _spiDevice.WriteByte(0x10);
            // Averaging (avg of 32 conversions: AVGON=1, NAVG11, NAVG0=1, NSCAN1=0, NSCAN0=0)
            _spiDevice.WriteByte(0x3c);
        }

        void RunLoop()
        {
            try
            {
                while (!_cts.Token.IsCancellationRequested)
                {
                    UpdateTemperatures();
                    Thread.Sleep(10);
                }
            }
            catch (OperationCanceledException ex)
            {
            }
        }

        void UpdateTemperatures()
        {
            // Reset FIFO

            _spiDevice.WriteByte(0x18);
            
            // Conversion

            _spiDevice.WriteByte(0xa0);

            // wait for signal that conversion is complete

            try
            {
                _eocEvent.Wait(new CancellationTokenSource(1).Token);
            }
            catch (OperationCanceledException)
            {
            }

            // read the results out

            ReadOnlySpan<byte> writeBuffer = stackalloc byte[8];
            Span<byte> readBuffer = stackalloc byte[8];

            _spiDevice.TransferFullDuplex(writeBuffer, readBuffer);

            int i = 0;
            foreach (var probe in _probes)
            {
                var value = (uint)readBuffer[i];
                value = value << 8;
                value = value | (uint)readBuffer[i+1];

                var volts = value * VoltsPerTick;
                var temp = volts * 100.0;
                // TODO: calibration

                ((TemperatureProbeImpl)probe.Impl).AddSample(temp);
                i += 2;
            }
        }


        class TemperatureProbeImpl : ITemperatureProbeImpl
        {
            readonly Subject<double> _samplesSubject = new Subject<double>();
            double _temperature;

            public TemperatureProbeImpl()
            {
                _samplesSubject
                    .Buffer(TimeSpan.FromMilliseconds(1000))
                    .Subscribe(PublishTemperature);
            }

            private void PublishTemperature(IList<double> samples)
            {
                if (samples.Count < 2) return;

                var avg = samples
                    .OrderBy(x => x)
                    .Skip(samples.Count / 4)
                    .Take(samples.Count / 2)
                    .Average();

                avg = Math.Round(avg, 1);

                Temperature = avg;
            }

            public double Temperature
            {
                get => _temperature;
                set
                {
                    _temperature = value;
                    TemperatureChanged?.Invoke();
                }
            }

            public event Action TemperatureChanged;

            public void AddSample(double temperature)
            {
                _samplesSubject.OnNext(temperature);
            }
        }
    }
}
