import { boot } from 'quasar/wrappers';
import { Brewery, SignalRBreweryHub } from '@bolonium/brewery-model';

interface IVueWithBrewery extends Vue {
  brewery: Brewery;
}

export default boot(async ({ Vue, app }) => {
  const hub = new SignalRBreweryHub(location.origin + '/breweryhub');
  const brewery = new Brewery(hub);

  Object.defineProperty(Vue.prototype, '$brewery', {
    get: function get() {
      const root = (<Vue>this).$root;
      return (<IVueWithBrewery>root).brewery;
    }
  });

  app.data = { brewery };

  await brewery.start();
});
