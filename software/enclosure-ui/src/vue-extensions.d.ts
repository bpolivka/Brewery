import Vue from 'vue';
import { Brewery } from '@bolonium/brewery-model';

declare module 'vue/types/vue' {
  interface Vue {
    $brewery: Brewery;
  }
}
