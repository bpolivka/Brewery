import { store } from 'quasar/wrappers';
import Vuex from 'vuex';

import relays from './modules/relays';
import { IRelaysState } from './modules/relays/state';
import connection from './modules/connection';
import { IConnectionState } from './modules/connection/state';
import tempProbes from './modules/tempProbes';
import { ITemperatureProbesState } from './modules/tempProbes/state';

import actions from './actions';
import mutations from './mutations';

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export interface IState {
  connection: IConnectionState;
  relays: IRelaysState;
  tempProbes: ITemperatureProbesState;
}

export default store(function({ Vue }) {
  Vue.use(Vuex);

  const Store = new Vuex.Store<IState>({
    modules: {
      relays,
      connection,
      tempProbes
    },

    actions,
    mutations,

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEV
  });

  return Store;
});
