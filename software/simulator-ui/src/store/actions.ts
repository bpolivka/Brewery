import { ActionTree } from 'vuex';
import { IState } from '.';
import { connection, IBreweryState } from 'src/services/signalr';

const actions: ActionTree<IState, IState> = {
  async updateBreweryState(context) {
    const breweryState = await connection.invoke<IBreweryState>(
      'GetBreweryState'
    );

    context.commit('setBreweryState', breweryState);
  }
};

export default actions;
