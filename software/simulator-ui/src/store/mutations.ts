import { MutationTree } from 'vuex';
import { IState } from '.';
import {
  IBreweryState,
  IRelayState,
  ITemperatureProbeState
} from 'src/services/signalr';

const mutation: MutationTree<IState> = {
  setBreweryState(state: IState, breweryState: IBreweryState) {
    for (const [key, value] of Object.entries(breweryState.relays)) {
      state.relays[key].enabled = (<IRelayState>value).enabled;
    }

    for (const [key, value] of Object.entries(breweryState.temperatureProbes)) {
      state.tempProbes[key].temperature = (<ITemperatureProbeState>(
        value
      )).temperature;
    }
  }
};

export default mutation;
