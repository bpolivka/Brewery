import { ActionTree } from 'vuex';
import { IState } from '../../index';
import { IRelaysState } from './state';
import { ISetStateArgs } from '.';
import { connection } from 'src/services/signalr';

const actions: ActionTree<IRelaysState, IState> = {
  async setState(context, args: ISetStateArgs) {
    await connection.send('setRelayState', args.relayName, args.enabled);
  },

  onStateChanged(context, args: ISetStateArgs) {
    context.commit('setState', args);
  }
};

export default actions;
