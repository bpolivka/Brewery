import { MutationTree } from 'vuex';
import { IRelaysState } from './state';
import { ISetStateArgs } from '.';

const mutation: MutationTree<IRelaysState> = {
  setState(state: IRelaysState, args: ISetStateArgs) {
    const relay = state[args.relayName];
    relay.enabled = args.enabled;
  }
};

export default mutation;
