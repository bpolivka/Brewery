export interface IRelayState {
  enabled: boolean;
}

export interface IRelaysState {
  hlt: IRelayState;
  boil: IRelayState;
  pump1: IRelayState;
  pump2: IRelayState;
  stir: IRelayState;
  fan: IRelayState;
  alarm: IRelayState;
  tbd: IRelayState;
  hltSsr: IRelayState;
  boilSsr: IRelayState;

  [key: string]: IRelayState;
}

const state: IRelaysState = {
  hlt: { enabled: false },
  boil: { enabled: false },
  pump1: { enabled: false },
  pump2: { enabled: false },
  stir: { enabled: false },
  fan: { enabled: false },
  alarm: { enabled: false },
  tbd: { enabled: false },
  hltSsr: { enabled: false },
  boilSsr: { enabled: false }
};

export default state;
