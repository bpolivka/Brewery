import { Module } from 'vuex';
import { IState } from '../../index';
import state, { IRelaysState } from './state';
import actions from './actions';
// import getters from './getters';
import mutations from './mutations';

export interface ISetStateArgs {
  relayName: string;
  enabled: boolean;
}

const relaysModule: Module<IRelaysState, IState> = {
  namespaced: true,
  actions,
  // getters,
  mutations,
  state
};

export default relaysModule;
