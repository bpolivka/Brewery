import { Module } from 'vuex';
import { IState } from '../../index';
import state, { ITemperatureProbesState } from './state';
import actions from './actions';
// import getters from './getters';
import mutations from './mutations';

export interface ISetTemperatureArgs {
  probeName: string;
  temperature: number;
}

const relaysModule: Module<ITemperatureProbesState, IState> = {
  namespaced: true,
  actions,
  // getters,
  mutations,
  state
};

export default relaysModule;
