export interface ITemperatureProbeState {
  name: string;
  temperature: number;
}

export interface ITemperatureProbesState {
  hlt: ITemperatureProbeState;
  mltIn: ITemperatureProbeState;
  mltOut: ITemperatureProbeState;
  boil: ITemperatureProbeState;

  [key: string]: ITemperatureProbeState;
}

const state: ITemperatureProbesState = {
  hlt: { name: 'hlt', temperature: 0.0 },
  mltIn: { name: 'mltIn', temperature: 0.0 },
  mltOut: { name: 'mltOut', temperature: 0.0 },
  boil: { name: 'boil', temperature: 0.0 }
};

export default state;
