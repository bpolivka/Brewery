import { ActionTree } from 'vuex';
import { IState } from '../../index';
import { ITemperatureProbesState } from './state';
import { ISetTemperatureArgs } from '.';
import { connection } from 'src/services/signalr';

const actions: ActionTree<ITemperatureProbesState, IState> = {
  async setTemperature(context, args: ISetTemperatureArgs) {
    console.log('setTemp: %o -> %o', args.probeName, args.temperature);
    await connection.send('SetTemperature', args.probeName, args.temperature);
  },

  onTemperatureChanged(context, args: ISetTemperatureArgs) {
    context.commit('setTemperature', args);
  }
};

export default actions;
