import { MutationTree } from 'vuex';
import { ITemperatureProbesState } from './state';
import { ISetTemperatureArgs } from '.';

const mutation: MutationTree<ITemperatureProbesState> = {
  setTemperature(state: ITemperatureProbesState, args: ISetTemperatureArgs) {
    const probe = state[args.probeName];
    probe.temperature = args.temperature;
  }
};

export default mutation;
