import { MutationTree } from 'vuex';
import { IConnectionState } from './state';

const mutation: MutationTree<IConnectionState> = {
  setConnected(state: IConnectionState, connected: boolean) {
    state.connected = connected;
  }
};

export default mutation;
