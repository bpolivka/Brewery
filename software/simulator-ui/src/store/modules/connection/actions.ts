import { ActionTree } from 'vuex';
import { IState } from '../../index';
import { IConnectionState } from './state';

const actions: ActionTree<IConnectionState, IState> = {
  async setConnected(context, connected: boolean) {
    context.commit('setConnected', connected);
    await context.dispatch('updateBreweryState', null, { root: true });
  }
};

export default actions;
