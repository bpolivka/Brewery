import { Module } from 'vuex';
import { IState } from '../../index';
import state, { IConnectionState } from './state';
import actions from './actions';
import mutations from './mutations';

const connectionModule: Module<IConnectionState, IState> = {
  namespaced: true,
  actions,
  mutations,
  state
};

export default connectionModule;
