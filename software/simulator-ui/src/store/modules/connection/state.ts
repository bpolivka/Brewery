export interface IConnectionState {
  connected: boolean;
}

const state: IConnectionState = {
  connected: false
};

export default state;
