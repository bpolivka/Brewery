import * as signalR from '@microsoft/signalr';
export interface IRelayState {
  enabled: boolean;
}

export interface ITemperatureProbeState {
  name: string;
  temperature: number;
}

export interface IBreweryState {
  relays: Map<string, IRelayState>;
  temperatureProbes: Map<string, ITemperatureProbeState>;
}

export const connection = new signalR.HubConnectionBuilder()
  .withUrl(location.origin + '/breweryhub')
  .build();
