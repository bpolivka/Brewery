import { boot } from 'quasar/wrappers';
import { connection } from 'src/services/signalr';
import { Store } from 'vuex';
import { IState } from 'src/store';

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function connect(store: Store<IState>) {
  while (true) {
    try {
      await connection.start();
      break;
    } catch (error) {
      await sleep(1000);
    }
  }

  console.log('connected!');
  await store.dispatch('connection/setConnected', true);
}

export default boot(async ({ store }: { store: Store<IState> }) => {
  connection.onclose(err => {
    console.log('onclose: %s', err);
    void store.dispatch('connection/setConnected', false);
    void connect(store);
  });

  connection.on('RelayStateChanged', (relay: string, enabled: boolean) => {
    void store.dispatch('relays/onStateChanged', {
      relayName: relay,
      enabled: enabled
    });
  });

  connection.on('TemperatureChanged', (probe: string, temperature: number) => {
    void store.dispatch('tempProbes/onTemperatureChanged', {
      probeName: probe,
      temperature: temperature
    });
  });

  await connect(store);
});
