export class WaterTank {
  heatCapacity = 4.182;
  thermalConductivity = 15.0;
  diameter = 41.0;
  volume = 49.0;
  density = 1.0;

  tempAmbient = 25.0;
  heatLossFactor = 1.0;
  efficiency = 0.98;

  elementPower = 0.0;
  temperature = 18.0;

  onUpdate?: (temp: number) => void;

  private _lastTime = new Date();
  private _intervalId = 0;

  update() {
    const time = new Date();
    const dt = (time.getTime() - this._lastTime.getTime()) / 1000.0;
    if (dt <= 0.0) return;

    this._lastTime = time;

    this.heat(dt);
    this.cool(dt);

    if (isNaN(this.temperature)) {
      console.log('NAN!!');
    }
    if (this.onUpdate) {
      this.onUpdate(this.temperature);
    }
  }

  get height() {
    const radius = this.diameter / 2.0;
    return (this.volume * 1000.0) / (Math.PI * Math.pow(radius, 2));
  }

  get surface() {
    const radius = this.diameter / 2.0;

    return (
      (2.0 * Math.PI * Math.pow(radius, 2) +
        2 * Math.PI * radius * this.height) /
      10000.0
    );
  }

  get mass() {
    return this.volume * this.density;
  }

  private _get_deltaT(power: number, duration: number) {
    // P = Q / t
    // Q = c * m * delta T
    // => delta (T) = (P * t) / (c * m)
    return (power * duration) / (this.heatCapacity * this.mass);
  }

  private heat(duration: number) {
    this.temperature += this._get_deltaT(
      this.elementPower * this.efficiency,
      duration
    );
  }

  private cool(duration: number) {
    // Q = k_w * A * (T_kettle - T_ambient)
    // P = Q / t
    let power =
      (this.thermalConductivity *
        this.surface *
        (this.temperature - this.tempAmbient)) /
      duration;

    // W to kW
    power /= 1000;
    this.temperature -= this._get_deltaT(power, duration) * this.heatLossFactor;
  }

  start() {
    this._lastTime = new Date();

    this._intervalId = window.setInterval(() => {
      this.update();
    }, 250);
  }
}
