using System;

namespace Brewery.Simulator
{
    public class SimulatedTimeProvider : ITimeProvider
    {
        private DateTime _currentTime = DateTime.Now;
        
        public void SetCurrentTime(DateTime time)
        {
            _currentTime = time;
        }
        public DateTime GetCurrentTime()
        {
            return _currentTime;
        }
    }
}