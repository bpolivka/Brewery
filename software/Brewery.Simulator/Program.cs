﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Brewery.HeatingStrategies;
using Brewery.Simulation;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using UnitsNet;
using Vibrant.InfluxDB.Client;
using Vibrant.InfluxDB.Client.Rows;
using IContainer = Autofac.IContainer;

namespace Brewery.Simulator
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var kettle = new Simulation.Kettle(
                Length.FromInches(16.125),
                Volume.FromUsGallons(13.0),
                Temperature.FromDegreesFahrenheit(70.0),
                Density.FromKilogramsPerLiter(1.0));
            
            var ambientTemp = Temperature.FromDegreesFahrenheit(67.0);
            var elementPower = Power.FromWatts(5000.0);
            
            var container = BuildContainer();
            
            var strategyManager = container.Resolve<HeatingStrategyManager>();
            var heatingElementManager = container.Resolve<HeatingElementManager>();
            var probeManager = container.Resolve<TemperatureProbeManager>();
            var probeController = container.Resolve<SimulatedTemperatureProbeController>();
            var simTimeProvider = container.Resolve<SimulatedTimeProvider>();
            var influxClient = container.Resolve<IInfluxClient>();

            var qqq = container.Resolve<ITimeProvider>();
            
            await strategyManager.StartAsync(CancellationToken.None);
            
            var strategies = strategyManager.GetStrategiesForElement("hlt");
            var strategy = strategies.First(s => s.Name == "Mash");
            strategy.SetParameters(new JObject(new JProperty("setpoint", 147.0)));

            var testName = "Mash - 1";
            var time = new DateTime(2020, 1, 1);
            var rows = new List<DynamicInfluxRow>();
            
            for (int i = 0; i < 1000; ++i)
            {
                probeController.SetTemperature(probeManager.Hlt, kettle.Temperature.DegreesFahrenheit);
                var powerLevel = (double)strategy.CalcPowerLevel(100);

                var row = new DynamicInfluxRow();
                row.Timestamp = time;
                row.Tags["test"] = testName;
                row.Fields["temp"] = kettle.Temperature.DegreesFahrenheit;
                row.Fields["powerLevel"] = powerLevel;
                rows.Add(row);

                var timeOn = 3.0 * (powerLevel / 100.0);
                var timeOff = 3.0 - timeOn;
            
                if (timeOn > 0)
                {
                    kettle.Heat(elementPower, TimeSpan.FromSeconds(timeOn));
                }
            
                kettle.Cool(TimeSpan.FromSeconds(3.0), ambientTemp, 5.0);
                Console.WriteLine($"{(i + 1) * 3},{kettle.Temperature.DegreesFahrenheit},{powerLevel}");
                time = time.AddSeconds(3);
                simTimeProvider.SetCurrentTime(time);
            }

            await influxClient.WriteAsync("brewery", "sim", rows);
        }

        static IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();

            builder
                .Register(ctx =>
                {
                    return new InfluxClient(new Uri("http://localhost:8086"));
                })
                .As<IInfluxClient>()
                .SingleInstance();
            
            builder.Register(ctx =>
                {
                    return Options.Create(new BreweryOptions
                    {
                        HeatingStrategyScriptPath = "/Users/brett/repos/Brewery/strategies"
                    });
                })
                .SingleInstance();

            builder.RegisterModule(new Brewery.AutofacModule());
            builder.RegisterModule(new Simulation.AutofacModule());

            builder.RegisterType<SimulatedTimeProvider>()
                .As<ITimeProvider>()
                .AsSelf()
                .SingleInstance();
            
            return builder.Build();
        }
    }
}