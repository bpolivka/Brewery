import { IBreweryHub } from './IBreweryHub';
import { HeatingElement } from './HeatingElement';

export interface IHeatingStrategy {
  readonly id: string;
  readonly name: string;
  readonly type: string;
  parameters: Record<string, unknown>;

  setParameters(parameters: Record<string, unknown>): Promise<void>;
  updateParameters(parameters: Record<string, unknown>): void;
}

export class HeatingStrategy implements IHeatingStrategy {
  readonly _hub: IBreweryHub;
  readonly _element: HeatingElement;

  readonly id: string;
  readonly name: string;
  readonly type: string;
  parameters = {};

  constructor(
    hub: IBreweryHub,
    element: HeatingElement,
    id: string,
    name: string,
    type: string
  ) {
    this._hub = hub;
    this._element = element;

    this.id = id;
    this.name = name;
    this.type = type;
  }
  updateParameters(parameters: Record<string, unknown>) {
    const newParams = Object.assign({}, this.parameters);
    Object.assign(newParams, parameters);
    this.parameters = newParams;
  }

  setParameters(parameters: Record<string, unknown>) {
    return this._hub.setHeatingStrategyParameters(
      this._element.name,
      this.id,
      parameters
    );
  }
}

class NoneHeatingStrategy implements IHeatingStrategy {
  id: string;
  name: string;
  type: string;
  parameters: Record<string, unknown>;

  constructor() {
    this.id = '';
    this.name = 'None';
    this.type = 'disabled';
    this.parameters = {};
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  setParameters(parameters: Record<string, unknown>) {
    return Promise.resolve();
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  updateParameters(parameters: Record<string, unknown>) {}
}

export const noneHeatingStrategy = new NoneHeatingStrategy();
