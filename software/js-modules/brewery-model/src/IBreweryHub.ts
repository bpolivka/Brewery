import { ITemperatureRecord } from "./ITemperatureRecord";
import { IHeatingElementHistoryRecord } from "./IHeatingElementHistoryRecord";
import { Signal } from "typed-signals";
import { IHeatingStrategyState, IBreweryState } from "./Brewery";

export interface IBreweryHub {
  readonly connected: boolean;

  readonly relayStateChanged: Signal<(relayName: string, enabled: boolean) => void>;
  readonly temperatureChanged: Signal<(probeName: string, temperature: number) => void>;
  readonly heatingElementPowerLevelChanged: Signal<(elementName: string, powerLevel: number) => void>;
  readonly heatingElementStrategyParametersChanged: Signal<
    (elementName: string, strategyId: string, parameters: Record<string, unknown>) => void
  >;
  readonly heatingElementStrategyChanged: Signal<(elementName: string, strategyId: string) => void>;
  readonly heatingStrategyAdded: Signal<(elementName: string, strategyState: IHeatingStrategyState) => void>;
  readonly heatingStrategyRemoved: Signal<(elementName: string, strategyId: string) => void>;
  readonly heatingStrategyUpdated: Signal<
    (elementName: string, oldStrategyId: string, newStrategyState: IHeatingStrategyState) => void
  >;
  readonly heatingStrategyCompileFailed: Signal<(name: string, message: string) => void>;
  readonly breweryStateUpdated: Signal<(state: IBreweryState) => void>;

  start(): Promise<void>;

  setRelayState(name: string, enabled: boolean): void;

  getTemperatureProbeHistory(name: string, fromTime: Date, toTime: Date): Promise<ITemperatureRecord[]>;

  getHeatingElementHistory(name: string, fromTime: Date, toTime: Date): Promise<IHeatingElementHistoryRecord[]>;

  setHeatingStrategyParameters(
    elementName: string,
    strategyId: string,
    parameters: Record<string, unknown>
  ): Promise<void>;

  setHeatingElementStrategy(elementName: string, strategyId: string): Promise<void>;
}
