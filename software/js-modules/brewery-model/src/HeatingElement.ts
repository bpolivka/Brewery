import { Relay } from "./Relay";
import { TemperatureProbe } from "./TemperatureProbe";
import { IBreweryHub } from "./IBreweryHub";
import {
  IHeatingStrategy,
  noneHeatingStrategy,
  HeatingStrategy
} from "./HeatingStrategy";
import { IHeatingElementState, IHeatingStrategyState } from "./Brewery";

export class HeatingElement {
  private readonly _hub: IBreweryHub;
  readonly name: string;
  readonly title: string;
  readonly ssrRelay: Relay;
  readonly temperatureProbes: TemperatureProbe[];
  powerLevel = 0;
  currentStrategy: IHeatingStrategy = noneHeatingStrategy;
  availableStrategies: IHeatingStrategy[] = [noneHeatingStrategy];
  extensions: Record<string, unknown> = {};

  constructor(
    hub: IBreweryHub,
    name: string,
    title: string,
    ssrRelay: Relay,
    temperatureProbes: TemperatureProbe[]
  ) {
    this._hub = hub;
    this.name = name;
    this.title = title;
    this.ssrRelay = ssrRelay;
    this.temperatureProbes = temperatureProbes;
  }

  getHistory(fromTime: Date, toTime: Date) {
    return this._hub.getHeatingElementHistory(this.name, fromTime, toTime);
  }

  getAvailableStrategy(id: string) {
    return this.availableStrategies.find(s => s.id == id);
  }

  updateState(state: IHeatingElementState) {
    this.powerLevel = state.powerLevel;

    this.availableStrategies = state.availableStrategies.map(s =>
      this.buildHeatingStrategy(s)
    );

    this.currentStrategy =
      this.availableStrategies.find(s => s.id == state.currentStrategyId) ??
      noneHeatingStrategy;
  }

  setCurrentStrategy(strategy: IHeatingStrategy) {
    void this._hub.setHeatingElementStrategy(this.name, strategy.id);
  }

  removeStrategy(strategy: IHeatingStrategy) {
    const idx = this.availableStrategies.indexOf(strategy);
    this.availableStrategies.splice(idx, 1);
  }

  addStrategy(strategyState: IHeatingStrategyState) {
    this.availableStrategies.push(this.buildHeatingStrategy(strategyState));
  }

  private buildHeatingStrategy(state: IHeatingStrategyState) {
    const strategy = new HeatingStrategy(
      this._hub,
      this,
      state.id,
      state.name,
      state.type
    );

    strategy.updateParameters(state.parameters);

    return strategy;
  }
}
