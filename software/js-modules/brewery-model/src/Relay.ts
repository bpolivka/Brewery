import { IBreweryHub } from './IBreweryHub';

export class Relay {
  _hub: IBreweryHub;

  name: string;
  title: string;
  enabled = false;

  constructor(controller: IBreweryHub, name: string, title: string) {
    this._hub = controller;
    this.name = name;
    this.title = title;
  }

  toggle() {
    this._hub.setRelayState(this.name, !this.enabled);
  }
}
