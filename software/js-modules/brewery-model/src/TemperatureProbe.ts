import { IBreweryHub } from './IBreweryHub';

export class TemperatureProbe {
  private _hub: IBreweryHub;

  name: string;
  title: string;
  temperature: number;

  constructor(hub: IBreweryHub, name: string, title: string) {
    this._hub = hub;
    this.name = name;
    this.title = title;
    this.temperature = 0.0;
  }

  getHistory(fromTime: Date, toTime: Date) {
    return this._hub.getTemperatureProbeHistory(this.name, fromTime, toTime);
  }
}
