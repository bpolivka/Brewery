import { Relay } from "./Relay";
import { IBreweryHub } from "./IBreweryHub";
import { Brewery, IBreweryState, IRelayState } from "./Brewery";

export class RelayManager {
  _hub: IBreweryHub;
  hlt: Relay;
  boil: Relay;
  pump1: Relay;
  pump2: Relay;
  stir: Relay;
  fan: Relay;
  alarm: Relay;
  tbd: Relay;
  hltSsr: Relay;
  boilSsr: Relay;

  private _byName = new Map<string, Relay>();

  constructor(brewery: Brewery) {
    this._hub = brewery.hub;

    this.hlt = this.createRelay("hlt", "HLT");
    this.boil = this.createRelay("boil", "Boil");
    this.pump1 = this.createRelay("pump1", "Pump 1");
    this.pump2 = this.createRelay("pump2", "Pump 2");
    this.stir = this.createRelay("stir", "Stir Motor");
    this.fan = this.createRelay("fan", "Fan");
    this.alarm = this.createRelay("alarm", "Alarm");
    this.tbd = this.createRelay("tbd", "TBD");
    this.hltSsr = this.createRelay("hltSsr", "HLT SSR");
    this.boilSsr = this.createRelay("boilSsr", "BOIL SSR");

    this._hub.breweryStateUpdated.connect((...args) => this.onBreweryStateUpdated(...args));
    this._hub.relayStateChanged.connect((...args) => this.onRelayStateChanged(...args));
  }

  private onBreweryStateUpdated(state: IBreweryState) {
    for (const [key, value] of Object.entries(state.relays)) {
      this.onRelayStateChanged(key, (<IRelayState>value).enabled);
    }
  }

  private createRelay(name: string, title: string) {
    const relay = new Relay(this._hub, name, title);
    this._byName.set(name, relay);
    return relay;
  }

  getRelay(name: string): Relay | undefined {
    return this._byName.get(name);
  }

  onRelayStateChanged(name: string, enabled: boolean) {
    const relay = this.getRelay(name);
    if (relay != undefined) {
      relay.enabled = enabled;
    }
  }
}
