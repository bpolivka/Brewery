import * as signalR from "@microsoft/signalr";
import { IBreweryState, IHeatingStrategyState } from "./Brewery";
import { IBreweryHub } from "./IBreweryHub";
import { ITemperatureRecord } from "./ITemperatureRecord";
import { IHeatingElementHistoryRecord } from "./IHeatingElementHistoryRecord";
import { Signal } from "typed-signals";

function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

interface ITemperatureProbeHistoryMessage {
  records: ITemperatureRecord[];
}

interface IHeatingElementHistoryMessage {
  records: IHeatingElementHistoryRecord[];
}

export class SignalRBreweryHub implements IBreweryHub {
  connected = false;
  connection: signalR.HubConnection;

  readonly relayStateChanged = new Signal<(relayName: string, enabled: boolean) => void>();
  readonly temperatureChanged = new Signal<(probeName: string, temperature: number) => void>();
  readonly heatingElementPowerLevelChanged = new Signal<(elementName: string, powerLevel: number) => void>();
  readonly heatingElementStrategyParametersChanged = new Signal<
    (elementName: string, strategyId: string, parameters: Record<string, unknown>) => void
  >();
  readonly heatingElementStrategyChanged = new Signal<(elementName: string, strategyId: string) => void>();
  readonly heatingStrategyAdded = new Signal<(elementName: string, strategyState: IHeatingStrategyState) => void>();
  readonly heatingStrategyRemoved = new Signal<(elementName: string, strategyId: string) => void>();
  readonly heatingStrategyUpdated = new Signal<
    (elementName: string, oldStrategyId: string, newStrategyState: IHeatingStrategyState) => void
  >();
  readonly heatingStrategyCompileFailed = new Signal<(name: string, message: string) => void>();
  readonly breweryStateUpdated = new Signal<(state: IBreweryState) => void>();

  constructor(hubUrl: string) {
    this.connection = new signalR.HubConnectionBuilder().withUrl(hubUrl).build();

    this.connection.onclose((err) => {
      this.connected = false;
      void this.connect();
    });

    this.connection.on("RelayStateChanged", (relay: string, enabled: boolean) => {
      this.relayStateChanged.emit(relay, enabled);
    });

    this.connection.on("TemperatureChanged", (probe: string, temperature: number) => {
      this.temperatureChanged.emit(probe, temperature);
    });

    this.connection.on("HeatingElementPowerLevelChanged", (element: string, powerLevel: number) => {
      this.heatingElementPowerLevelChanged.emit(element, powerLevel);
    });

    this.connection.on(
      "HeatingStrategyParametersChanged",
      (elementName: string, strategyId: string, parameters: Record<string, unknown>) => {
        this.heatingElementStrategyParametersChanged.emit(elementName, strategyId, parameters);
      }
    );

    this.connection.on("HeatingElementStrategyChanged", (elementName: string, strategyId: string) => {
      this.heatingElementStrategyChanged.emit(elementName, strategyId);
    });

    this.connection.on("HeatingStrategyAdded", (elementName: string, strategyState: IHeatingStrategyState) => {
      this.heatingStrategyAdded.emit(elementName, strategyState);
    });

    this.connection.on("HeatingStrategyRemoved", (elementName: string, strategyId: string) => {
      this.heatingStrategyRemoved.emit(elementName, strategyId);
    });

    this.connection.on(
      "HeatingStrategyUpdated",
      (elementName: string, oldStrategyId: string, newStrategyState: IHeatingStrategyState) => {
        this.heatingStrategyUpdated.emit(elementName, oldStrategyId, newStrategyState);
      }
    );

    this.connection.on("HeatingStrategyCompileFailed", (name: string, message: string) => {
      this.heatingStrategyCompileFailed.emit(name, message);
    });
  }

  async getTemperatureProbeHistory(name: string, fromTime: Date, toTime: Date): Promise<ITemperatureRecord[]> {
    const msg = await this.connection.invoke<ITemperatureProbeHistoryMessage>(
      "GetTemperatureProbeHistory",
      name,
      fromTime,
      toTime
    );

    return msg.records;
  }

  async getHeatingElementHistory(name: string, fromTime: Date, toTime: Date): Promise<IHeatingElementHistoryRecord[]> {
    const msg = await this.connection.invoke<IHeatingElementHistoryMessage>(
      "GetHeatingElementHistory",
      name,
      fromTime,
      toTime
    );

    return msg.records;
  }

  setHeatingStrategyParameters(
    elementName: string,
    strategyId: string,
    parameters: Record<string, unknown>
  ): Promise<void> {
    return this.connection.send("SetHeatingStrategyParameters", elementName, strategyId, parameters);
  }

  setHeatingElementStrategy(elementName: string, strategyId: string): Promise<void> {
    return this.connection.send("SetHeatingElementStrategy", elementName, strategyId);
  }

  setRelayState(name: string, enabled: boolean): void {
    void this.connection.send("SetRelayState", name, enabled);
  }

  async start() {
    await this.connect();
  }

  private async updateBreweryState() {
    const state: IBreweryState = await this.connection.invoke("GetBreweryState");
    this.breweryStateUpdated.emit(state);
  }

  private async connect() {
    while (true) {
      try {
        await this.connection.start();
        this.connected = true;
        await this.updateBreweryState();
        break;
      } catch (error) {
        await sleep(1000);
      }
    }
  }
}
