export interface IHeatingElementHistoryRecord {
  time: string;
  powerLevel: number;
}
