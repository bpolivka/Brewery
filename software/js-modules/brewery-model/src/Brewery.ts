import { TemperatureProbeManager } from "./TemperatureProbeManager";
import { RelayManager } from "./RelayManager";
import { IBreweryHub } from "./IBreweryHub";
import { HeatingElementManager } from "./HeatingElementManager";

export interface IRelayState {
  enabled: boolean;
}

export interface ITemperatureProbeState {
  temperature: number;
}

export interface IHeatingStrategyState {
  id: string;
  name: string;
  type: string;
  parameters: Record<string, unknown>;
}

export interface IHeatingElementState {
  powerLevel: number;
  availableStrategies: IHeatingStrategyState[];
  currentStrategyId: string;
}

export interface IBreweryState {
  relays: Map<string, IRelayState>;
  temperatureProbes: Map<string, ITemperatureProbeState>;
  heatingElements: Map<string, IHeatingElementState>;
}

export class Brewery {
  hub: IBreweryHub;
  temperatureProbes: TemperatureProbeManager;
  relays: RelayManager;
  heatingElements: HeatingElementManager;

  constructor(hub: IBreweryHub) {
    this.hub = hub;
    this.temperatureProbes = new TemperatureProbeManager(this);
    this.relays = new RelayManager(this);
    this.heatingElements = new HeatingElementManager(this);
  }

  async start() {
    await this.hub.start();
  }
}
