import { HeatingElement } from "./HeatingElement";
import { Brewery, IHeatingElementState, IHeatingStrategyState, IBreweryState } from "./Brewery";
import { Relay } from "./Relay";
import { TemperatureProbe } from "./TemperatureProbe";

export class HeatingElementManager {
  private _brewery: Brewery;
  hlt: HeatingElement;
  boil: HeatingElement;
  private _byName = new Map<string, HeatingElement>();

  constructor(brewery: Brewery) {
    this._brewery = brewery;

    this.hlt = this.createHeatingElement("hlt", "HLT", brewery.relays.hltSsr, [
      brewery.temperatureProbes.hlt,
      brewery.temperatureProbes.mltIn,
      brewery.temperatureProbes.mltOut,
    ]);

    this.boil = this.createHeatingElement("boil", "Boil", brewery.relays.boilSsr, [brewery.temperatureProbes.boil]);

    this._brewery.hub.breweryStateUpdated.connect((...args) => this.onBreweryStateUpdated(...args));
    this._brewery.hub.heatingElementPowerLevelChanged.connect((...args) =>
      this.onHeatingElementPowerLevelChanged(...args)
    );
    this._brewery.hub.heatingElementStrategyChanged.connect((...args) => this.onHeatingStrategyChanged(...args));
    this._brewery.hub.heatingElementStrategyParametersChanged.connect((...args) =>
      this.onHeatingStrategyParametersChanged(...args)
    );
    this._brewery.hub.heatingStrategyAdded.connect((...args) => this.onHeatingStrategyAdded(...args));
    this._brewery.hub.heatingStrategyRemoved.connect((...args) => this.onHeatingStrategyRemoved(...args));
    this._brewery.hub.heatingStrategyUpdated.connect((...args) => this.onHeatingStrategyUpdated(...args));
  }

  private onBreweryStateUpdated(state: IBreweryState) {
    for (const [key, value] of Object.entries(state.heatingElements)) {
      this.updateElementState(key, <IHeatingElementState>value);
    }
  }

  getByName(name: string) {
    return this._byName.get(name);
  }

  onHeatingElementPowerLevelChanged(name: string, powerLevel: number) {
    const element = this.getByName(name);
    if (element) {
      element.powerLevel = powerLevel;
    }
  }

  onHeatingStrategyParametersChanged(elementName: string, strategyId: string, parameters: Record<string, unknown>) {
    const element = this.getByName(elementName);
    if (!element) {
      return;
    }
    const strategy = element.getAvailableStrategy(strategyId);
    if (!strategy) {
      return;
    }
    strategy.updateParameters(parameters);
  }

  onHeatingStrategyChanged(elementName: string, strategyId: string) {
    const element = this.getByName(elementName);
    if (!element) return;
    const strategy = element.getAvailableStrategy(strategyId);
    if (!strategy) return;
    element.currentStrategy = strategy;
  }

  onHeatingStrategyAdded(elementName: string, strategyState: IHeatingStrategyState) {
    const element = this.getByName(elementName);
    if (!element) return;
    element.addStrategy(strategyState);
  }

  onHeatingStrategyRemoved(elementName: string, strategyId: string) {
    const element = this.getByName(elementName);
    if (!element) return;
    const strategy = element.getAvailableStrategy(strategyId);
    if (!strategy) return;
    element.removeStrategy(strategy);
  }

  onHeatingStrategyUpdated(elementName: string, oldStrategyId: string, newStrategyState: IHeatingStrategyState) {
    const element = this.getByName(elementName);
    if (!element) return;
    const strategy = element.getAvailableStrategy(oldStrategyId);
    if (strategy) {
      element.removeStrategy(strategy);
    }

    element.addStrategy(newStrategyState);
  }

  private createHeatingElement(name: string, title: string, ssrRelay: Relay, temperatureProbes: TemperatureProbe[]) {
    const element = new HeatingElement(this._brewery.hub, name, title, ssrRelay, temperatureProbes);
    this._byName.set(name, element);
    return element;
  }

  updateElementState(name: string, state: IHeatingElementState) {
    const element = this.getByName(name);
    if (!element) return;
    element.updateState(state);
  }
}
