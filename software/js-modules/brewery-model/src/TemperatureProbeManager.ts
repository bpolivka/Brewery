import { TemperatureProbe } from "./TemperatureProbe";
import { IBreweryHub } from "./IBreweryHub";
import { ITemperatureProbeState, IBreweryState, Brewery } from "./Brewery";

export class TemperatureProbeManager {
  private _hub: IBreweryHub;

  hlt: TemperatureProbe;
  mltIn: TemperatureProbe;
  mltOut: TemperatureProbe;
  boil: TemperatureProbe;

  constructor(brewery: Brewery) {
    this._hub = brewery.hub;

    this.hlt = new TemperatureProbe(this._hub, "hlt", "HLT");
    this.mltIn = new TemperatureProbe(this._hub, "mltIn", "MLT In");
    this.mltOut = new TemperatureProbe(this._hub, "mltOut", "MLT Out");
    this.boil = new TemperatureProbe(this._hub, "boil", "Boil");

    this._hub.breweryStateUpdated.connect((...args) => this.onBreweryStateUpdated(...args));
    this._hub.temperatureChanged.connect((...args) => this.onTemperatureChanged(...args));
  }

  private onBreweryStateUpdated(state: IBreweryState) {
    for (const [key, value] of Object.entries(state.temperatureProbes)) {
      this.onTemperatureChanged(key, (<ITemperatureProbeState>value).temperature);
    }
  }

  getProbe(probeName: string): TemperatureProbe | null {
    switch (probeName) {
      case "hlt":
        return this.hlt;
      case "mltIn":
        return this.mltIn;
      case "mltOut":
        return this.mltOut;
      case "boil":
        return this.boil;
      default:
        return null;
    }
  }

  onTemperatureChanged(probeName: string, temperature: number) {
    const probe = this.getProbe(probeName);
    if (probe != null) {
      probe.temperature = temperature;
    }
  }
}
