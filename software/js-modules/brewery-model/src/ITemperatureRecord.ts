export interface ITemperatureRecord {
  time: string;
  temperature: number;
}
