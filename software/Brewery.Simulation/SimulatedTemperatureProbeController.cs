﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Handlers;

namespace Brewery.Simulation
{
    public class SimulatedTemperatureProbeController : ITemperatureProbeController
    {
        public void SetTemperature(TemperatureProbe probe, double temperature)
        {
            ((TemperatureProbeImpl)probe.Impl).Temperature = temperature;
        }
        
        public ITemperatureProbeImpl CreateProbeImpl()
        {
            return new TemperatureProbeImpl(100.0);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        class TemperatureProbeImpl : ITemperatureProbeImpl
        {
            double _temperature;

            public TemperatureProbeImpl(double initialTemperature)
            {
                _temperature = initialTemperature;
            }

            public double Temperature
            {
                get => _temperature;
                set
                {
                    _temperature = value;
                    TemperatureChanged?.Invoke();
                }
            }

            public event Action TemperatureChanged;
        }
    }
}
