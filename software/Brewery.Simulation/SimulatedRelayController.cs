﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Brewery.Simulation
{
    public class SimulatedRelayController : IRelayController
    {
        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public IRelayImpl CreateImpl(string name)
        {
            return new RelayImpl();
        }

        class RelayImpl : IRelayImpl
        {
            public void SetState(bool enabled)
            {
            }
        }
    }
}
