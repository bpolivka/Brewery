﻿using Autofac;

namespace Brewery.Simulation
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<SimulatedRelayController>()
                .AsSelf()
                .As<IRelayController>()
                .SingleInstance();

            builder
                .RegisterType<SimulatedTemperatureProbeController>()
                .AsSelf()
                .As<ITemperatureProbeController>()
                .SingleInstance();
        }
    }
}
