using System;
using System.Net;
using UnitsNet;
using UnitsNet.Units;

namespace Brewery.Simulation
{
    public class Kettle
    {
        private static readonly SpecificEntropy SpecificHeatCapacityWater =
            SpecificEntropy.FromKilojoulesPerKilogramKelvin(4.182);

        private static readonly ThermalConductivity ThermalConductivitySteel =
            ThermalConductivity.FromWattsPerMeterKelvin(15.0);

        private Mass _mass;
        private Area _surface;

        public Temperature Temperature { get; private set; }

        public Kettle(Length diameter, Volume volume, Temperature initalTemperature, Density density)
        {
            _mass = volume * density;
            Temperature = initalTemperature;
            var radius = diameter / 2.0;

            var height = Length.FromCentimeters(
                (volume.Liters * 1000.0) / (Math.PI * Math.Pow(radius.Centimeters, 2.0))
            );

            _surface = Area.FromSquareMeters(
                (2.0 * Math.PI * Math.Pow(radius.Centimeters, 2.0) +
                 2.0 * Math.PI * radius.Centimeters * height.Centimeters) / 10000.0
            );
        }

        // Heat the kettle's content.
        public Temperature Heat(Power power, TimeSpan duration, double efficiency = 0.98)
        {
            var deltaTemp = GetDeltaTemp(power * (decimal)efficiency, duration);
            Temperature += deltaTemp;
            return Temperature;
        }

        // Make the content loose heat.
        public Temperature Cool(TimeSpan duration, Temperature ambientTemp, double heatLossFactor)
        {
            // Q = k_w * A * (T_kettle - T_ambient)
            // P = Q / t
            var q = 
                ThermalConductivitySteel.WattsPerMeterKelvin *
                _surface.SquareMeters *
                (Temperature - ambientTemp).Kelvins;
            
            var power = Power.FromWatts(q / duration.TotalSeconds);

            Temperature -= GetDeltaTemp(power, duration) * heatLossFactor;

            return Temperature;
        }

        private TemperatureDelta GetDeltaTemp(Power power, TimeSpan duration)
        {
            // P = Q / t
            // Q = c * m * delta T
            // => delta(T) = (P * t) / (c * m)
            var qq = SpecificHeatCapacityWater.KilojoulesPerKilogramKelvin * _mass.Kilograms;
            
            return TemperatureDelta.FromKelvins(
                (power * duration).Kilojoules /
                (SpecificHeatCapacityWater.KilojoulesPerKilogramKelvin * _mass.Kilograms)
            );
        }
    }
}