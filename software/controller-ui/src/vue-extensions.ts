import Vue from 'vue';
import { IState } from 'src/model/State';

declare module 'vue/types/vue' {
  interface Vue {
    $state: IState;
    $sass: Record<string, unknown>;
  }
}
