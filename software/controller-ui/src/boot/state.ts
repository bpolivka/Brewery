import { boot } from 'quasar/wrappers';
import { State, IState } from 'src/model/State';

interface IVueWithState extends Vue {
  state: IState;
}

export default boot(async ({ Vue, app }) => {
  const state = new State();

  Object.defineProperty(Vue.prototype, '$state', {
    get: function get() {
      const root = (<Vue>this).$root;
      return (<IVueWithState>root).state;
    }
  });

  app.data = { state: state };

  await state.start();
});
