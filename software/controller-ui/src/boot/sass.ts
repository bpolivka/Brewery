import { boot } from 'quasar/wrappers';
import sass from '../css/quasar.variables.json';

interface IVuePrototypeWithSass {
  $sass: Record<string, unknown>;
}

export default boot(({ Vue }) => {
  (<IVuePrototypeWithSass>Vue.prototype).$sass = sass;
});

export { sass };
