declare module 'vue-echarts' {
  import { Vue } from 'vue-property-decorator';

  export default class ECharts extends Vue {
    chart: echarts.ECharts;
  }
}
