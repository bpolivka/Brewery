export interface IDrawerButtonMetadata {
  icon: string;
}

export interface IRouteMetadata {
  rightDrawerButton?: IDrawerButtonMetadata;
}
