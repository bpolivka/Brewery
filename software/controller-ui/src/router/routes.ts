import { RouteConfig } from 'vue-router';

const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        components: {
          default: () => import('pages/Home.vue'),
          rightDrawer: () => import('pages/HomeSettings.vue')
        },
        meta: {
          rightDrawerButton: {
            icon: 'settings'
          }
        }
      },
      {
        path: 'stuff',
        components: {
          default: () => import('pages/Stuff.vue')
        }
      }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
];

export default routes;
