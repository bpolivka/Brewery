<template>
  <div style="width: 100%; height: 100%;">
    <ECharts id="chart" ref="chart" :options="options" />
    <q-resize-observer @resize="onResize" />
  </div>
</template>

<style lang="scss" scoped>
/**
 * The default size is 600px×400px, for responsive charts
 * you may need to set percentage values as follows (also
 * don't forget to provide a size for the container).
 */
#chart {
  width: 100%;
  height: 100%;
}
</style>

<script lang="ts">
import { Component, Prop, Ref, Vue, Watch } from 'vue-property-decorator';
import ECharts from 'vue-echarts';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/polar';
import 'echarts';
import { EChartOption } from 'echarts';
import { sprintf } from 'sprintf-js';
import { HeatingElement } from '@bolonium/brewery-model';
import { HeatingElementUiState } from 'src/model/State';

@Component({
  components: {
    ECharts
  }
})
export default class HeatingControllerChart extends Vue {
  @Ref() chart!: ECharts;
  @Prop() element!: HeatingElement;

  private _echart!: echarts.ECharts;
  private _chartData: Record<string, unknown>[] = [];

  options: EChartOption = {
    animation: false,
    grid: {
      left: 60,
      right: 60,
      top: 50,
      bottom: 40
    },
    legend: {
      icon: 'roundRect',
      padding: 20,
      textStyle: {
        color: '#777777'
      },
      inactiveColor: '#444444'
    },
    xAxis: {
      type: 'time',
      splitNumber: 10,
      axisLine: {
        lineStyle: {
          color: '#777777'
        }
      },
      splitLine: {
        lineStyle: {
          color: '#444444'
        }
      },
      splitArea: {
        show: true,
        areaStyle: {
          color: ['#262626', '#303030'],
          opacity: 0.5
        }
      }
    },
    yAxis: [
      {
        type: 'value',
        name: 'Power Level',
        min: 0.0,
        max: 110.0,
        axisLine: {
          lineStyle: {
            color: '#777777'
          }
        },
        splitLine: {
          show: false
        },
        axisLabel: {
          showMaxLabel: false,
          formatter: '{value}%'
        }
      },
      {
        type: 'value',
        name: 'Temperature',
        scale: true,
        boundaryGap: ['5%', '5%'],
        axisLine: {
          lineStyle: {
            color: '#777777'
          }
        },
        splitLine: {
          lineStyle: {
            color: '#444444'
          }
        },
        axisLabel: {
          formatter: (value: number) => {
            return sprintf('%.1f°', value);
          }
        }
      }
    ],
    series: [
      {
        name: 'Power Level',
        yAxisIndex: 0,
        animation: false,
        type: 'line',
        step: 'start',
        areaStyle: {
          opacity: 0.1
        },
        showSymbol: false,
        encode: {
          x: 'time',
          y: ['powerLevel']
        }
      },
      ...this.createTemperatureSeries()
    ],
    dataset: {
      dimensions: [
        'time',
        'powerLevel',
        ...this.element.temperatureProbes.map(p => `temp_${p.name}`)
      ],
      source: []
    },
    color: [
      <string>this.$sass['powerLevelColor'],
      ...this.element.temperatureProbes.map(
        p => <string>this.$sass[`${p.name}Color`]
      )
    ]
  };

  private get uiState() {
    return HeatingElementUiState.fromElement(this.element);
  }

  createTemperatureSeries() {
    return this.element.temperatureProbes.map(probe => {
      const series: EChartOption.Series = {
        name: probe.title,
        type: 'line',
        animation: false,
        yAxisIndex: 1,
        showSymbol: false,
        encode: {
          x: 'time',
          y: [`temp_${probe.name}`]
        }
      };

      return series;
    });
  }

  @Watch('uiState.minutesOfHistory')
  onMinutesOfHistoryChanged() {
    void this.loadData();
  }

  async loadData() {
    this.chart.chart.showLoading('Loading...');

    const toTime = new Date();
    const fromTime = new Date(
      toTime.getTime() - this.uiState.minutesOfHistory * 60000 - 1000
    );

    const map = new Map<number, Record<string, unknown>>();

    const addEntry = (name: string, time: Date, value: number) => {
      let t = time.getTime();
      let record = map.get(t);
      if (!record) {
        record = {
          time: t
        };
        map.set(t, record);
      }
      record[name] = value;
    };

    for (const probe of this.element.temperatureProbes) {
      const fieldName = 'temp_' + probe.name;
      const history = await probe.getHistory(fromTime, toTime);
      for (const tempRecord of history) {
        addEntry(fieldName, new Date(tempRecord.time), tempRecord.temperature);
      }
    }

    const elementHistory = await this.element.getHistory(fromTime, toTime);
    for (const record of elementHistory) {
      addEntry('powerLevel', new Date(record.time), record.powerLevel);
    }

    function compareDates(a: number, b: number) {
      if (a < b) {
        return -1;
      }
      if (a > b) {
        return 1;
      }
      return 0;
    }

    const data = Array.from(map.values());
    data.sort((a, b) => compareDates(<number>a.time, <number>b.time));

    this._chartData = data;

    this._echart.setOption({
      xAxis: {
        min: fromTime.getTime()
      },
      dataset: {
        source: this._chartData
      }
    });

    this.chart.chart.hideLoading();
  }

  updateData() {
    const now = Date.now();
    const minTime = now - this.uiState.minutesOfHistory * 60 * 1000;

    while (
      this._chartData.length > 0 &&
      (this._chartData[0].time as number) < minTime
    ) {
      this._chartData.shift();
    }

    const data: Record<string, unknown> = {
      time: now,
      powerLevel: this.element.powerLevel
    };

    for (const probe of this.element.temperatureProbes) {
      data[`temp_${probe.name}`] = probe.temperature;
    }

    this._chartData.push(data);

    this._echart.setOption({
      xAxis: {
        min: minTime
      },
      dataset: {
        source: this._chartData
      }
    });
  }

  async mounted() {
    this._echart = this.chart.chart;
    this._chartData = [];

    await this.loadData();

    setInterval(() => {
      this.updateData();
    }, 250);
  }

  onResize() {
    this.chart.chart.resize();
  }
}
</script>
