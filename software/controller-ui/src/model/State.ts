import {
  Brewery,
  SignalRBreweryHub,
  IBreweryHub,
  IHeatingStrategyState,
  HeatingElement
} from '@bolonium/brewery-model';

import { Notify, SessionStorage } from 'quasar';
import { WebStorageGetMethodReturnType } from 'quasar/dist/types/api/web-storage';

function getState<T extends WebStorageGetMethodReturnType>(
  prefix: string,
  name: string,
  setter: (value: T) => void
) {
  const value = SessionStorage.getItem(`${prefix}.${name}`);
  if (value != null) {
    setter(<T>value);
  }
}

export class HeatingElementUiState {
  private readonly _statePrefix: string;
  private _visible = true;
  private _minutesOfHistory = 3;

  static fromElement(element: HeatingElement) {
    return <HeatingElementUiState>element.extensions['ui'];
  }

  get visible() {
    return this._visible;
  }

  set visible(value: boolean) {
    this._visible = value;
    SessionStorage.set(`${this._statePrefix}.visible`, this._visible);
  }

  get minutesOfHistory() {
    return this._minutesOfHistory;
  }

  set minutesOfHistory(value: number) {
    this._minutesOfHistory = value;
    SessionStorage.set(
      `${this._statePrefix}.minutesOfHistory`,
      this._minutesOfHistory
    );
  }

  constructor(statePrefix: string) {
    this._statePrefix = statePrefix;
    this.loadSavedState();
  }

  private loadSavedState() {
    getState<boolean>(this._statePrefix, 'visible', v => (this._visible = v));
    getState<number>(
      this._statePrefix,
      'minutesOfHistory',
      v => (this._minutesOfHistory = v)
    );
  }
}

export interface IState {
  brewery: Brewery;
  hub: IBreweryHub;

  start(): Promise<void>;
}

export class State implements IState {
  brewery: Brewery;
  hub: SignalRBreweryHub;

  constructor() {
    this.hub = new SignalRBreweryHub(location.origin + '/breweryhub');
    this.brewery = new Brewery(this.hub);

    this.hub.heatingStrategyAdded.connect((...args) =>
      this.onHeatingStrategyAdded(...args)
    );

    this.hub.heatingStrategyRemoved.connect((...args) =>
      this.onHeatingStrategyRemoved(...args)
    );

    this.hub.heatingStrategyUpdated.connect((...args) =>
      this.onHeatingStrategyUpdated(...args)
    );

    this.hub.heatingStrategyCompileFailed.connect((...args) =>
      this.onHeatingStrategyCompileFailed(...args)
    );

    this.brewery.heatingElements.hlt.extensions.ui = new HeatingElementUiState(
      'brewery.ui.heatingElements.hlt'
    );

    this.brewery.heatingElements.boil.extensions.ui = new HeatingElementUiState(
      'brewery.ui.heatingElements.boil'
    );
  }

  onHeatingStrategyAdded(
    elementName: string,
    strategyState: IHeatingStrategyState
  ) {
    const element = this.brewery.heatingElements.getByName(elementName);
    if (!element) return;

    Notify.create({
      type: 'positive',
      message: `Heating strategy "${strategyState.name}" has been added to element "${element.title}"`
    });
  }

  onHeatingStrategyRemoved(elementName: string, strategyId: string) {
    const element = this.brewery.heatingElements.getByName(elementName);
    if (!element) return;
    const strategy = element.getAvailableStrategy(strategyId);

    Notify.create({
      type: 'warning',
      message: `Heating strategy "${strategy.name}" has been removed from element "${element.title}"`
    });
  }

  onHeatingStrategyUpdated(
    elementName: string,
    oldStrategyId: string,
    newStrategyState: IHeatingStrategyState
  ) {
    const element = this.brewery.heatingElements.getByName(elementName);
    if (!element) return;

    Notify.create({
      type: 'positive',
      message: `Heating strategy "${newStrategyState.name}" has been updated on element "${element.title}"`
    });
  }

  onHeatingStrategyCompileFailed(name: string, message: string) {
    Notify.create({
      type: 'negative',
      message: `Failed to compile heating strategy "${name}"`,
      caption: message
    });
  }

  async start() {
    await this.brewery.start();
  }
}
