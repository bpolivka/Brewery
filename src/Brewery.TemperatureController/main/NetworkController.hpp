#ifndef Brewery__NetworkController_hpp
#define Brewery__NetworkController_hpp

#include <esp_err.h>
#include <esp_event.h>

#include "Signal.hpp"

namespace Brewery {

class NetworkController
{
public:

   struct Config
   {
      std::string ssid;
      std::string password;
   };

public:

   enum class Status
   {
      Disconnected,
      Connected
   };

public:

   NetworkController() = delete;

   static void initialize();

   static Signal<Status> statusChanged;

   static Status getStatus()
   {
      return _status;
   }

   static void setConfig(const Config& cfg);

private:

   static void start();
   static void stop();

   static void setStatus(Status status);
   static esp_err_t eventHandler(void* ctx, system_event_t* event);

   static void updateConfig(const Config& cfg);
   static void loadConfig();

private:

   static Status _status;
};

} /* namespace Brewery */

#endif /* Brewery__NetworkController_hpp */
