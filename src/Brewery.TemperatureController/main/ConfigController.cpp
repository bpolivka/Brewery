#include "ConfigController.hpp"

#include <esp_log.h>

#define LOG_SCOPE "ConfigController"

namespace Brewery {

nvs_handle ConfigController::_nvsHandle;

void ConfigController::initialize()
{
   esp_err_t err = nvs_flash_init();

   if (err == ESP_ERR_NVS_NO_FREE_PAGES)
   {
      ESP_ERROR_CHECK(nvs_flash_erase());
      err = nvs_flash_init();
   }
   ESP_ERROR_CHECK(err);

   ESP_ERROR_CHECK(nvs_open("config", NVS_READWRITE, &_nvsHandle));
}

void ConfigController::setString(const char* key, const char* value)
{
   ESP_ERROR_CHECK(nvs_set_str(_nvsHandle, key, value));
   ESP_ERROR_CHECK(nvs_commit(_nvsHandle));
}

std::string ConfigController::getString(const char* key)
{
   char buf[128];
   size_t len = sizeof(buf);
   esp_err_t ret = nvs_get_str(_nvsHandle, key, buf, &len);
   if (ret == ESP_ERR_NVS_NOT_FOUND)
   {
      return std::string {};
   }
   ESP_ERROR_CHECK(ret);
   return std::string(buf, len);
}

union float_union
{
   uint32_t u32;
   float f;
};

void ConfigController::setFloat(const char* key, float value)
{
   float_union u;

   u.f = value;

   ESP_ERROR_CHECK(nvs_set_u32(_nvsHandle, key, u.u32));
   ESP_ERROR_CHECK(nvs_commit(_nvsHandle));
}

bool ConfigController::getFloat(const char* key, float* value)
{
   float_union u;

   auto ret = nvs_get_u32(_nvsHandle, key, &u.u32);

   if (ret == ESP_ERR_NVS_NOT_FOUND)
      return false;

   *value = u.f;
   return true;
}


} // namespace Brewery
