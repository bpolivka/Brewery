#ifndef BREWERY_TEMPERATURESENSORCONTROLLER_HPP
#define BREWERY_TEMPERATURESENSORCONTROLLER_HPP

#include <driver/spi_master.h>

#include <deque>

namespace Brewery {

class TemperatureSensorController
{
private:

   static const int MISO_Pin = 19;
   static const int MOSI_Pin = 23;
   static const int CLK_Pin = 18;
   static const int CS_Pin = 5;
   static const int ConversionDone_Pin = 25;

   static const int NumSensors = 4;

   static constexpr float VoltsPerTick = 2.5 / 4096.0;

   struct CalibrationData
   {
      float scale;
      float offset;
   };

   struct SensorReadings
   {
      TickType_t tickCount;
      uint16_t values[NumSensors];
   };

public:

   TemperatureSensorController() = delete;

   static void initialize();

   static void sampleThreadMain(void* params);
   static void handleConversionDoneInterrupt(void* data);

   static const char* probeNumToName(int probeNum);

   static void setCalibrationParameters(const char* probe, float scale, float offset);
   static void getCalibrationParameters(const char* probe, float* scale, float* offset);

private:

   static void loadCalibrationParameters(const char* probe);

   static spi_device_handle_t _spi;
   static spi_bus_config_t _buscfg;
   static spi_device_interface_config_t _devcfg;
   static SemaphoreHandle_t _conversionDoneSemaphore;
   static spi_transaction_t _tx;
   static uint8_t _txBuffer[NumSensors * 2];
   static uint8_t _rxBuffer[NumSensors * 2];
   static TaskHandle_t _taskHandle;
   static CalibrationData _calibrationData[NumSensors];
   static std::deque<SensorReadings> _readings;
};

} // namespace Brewery

#endif //BREWERY_TEMPERATURESENSORCONTROLLER_HPP
