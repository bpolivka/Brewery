#ifndef __Brewery__BreweryController_hpp__
#define __Brewery__BreweryController_hpp__

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

#include <vector>

#include "Output.hpp"
#include "MqttController.hpp"

namespace Brewery
{

class BreweryController
{
public:

   BreweryController();

public:

   void onConnected();
   void onDisconnected();

public:

   Output ledOutput;
};

}

#endif
