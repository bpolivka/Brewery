#ifndef __Brewery__Output_hpp__
#define __Brewery__Output_hpp__

#include <freertos/FreeRTOS.h>

namespace Brewery
{

class BreweryController;

class Output
{
public:

   Output(BreweryController& controller, const char* name, gpio_num_t gpio, bool inverted) :
         _controller(controller), _name(name), _gpio(gpio), _state(false), _inverted(inverted)
   {
      configureGpio();
   }

   const char* getName() const
   {
      return _name;
   }

   bool getState() const
   {
      return _state;
   }

   void setState(bool state);

private:

   void configureGpio();

private:

   BreweryController& _controller;
   const char* _name;
   gpio_num_t _gpio;
   bool _state;
   bool _inverted;
};

} // namespace Brewery

#endif
