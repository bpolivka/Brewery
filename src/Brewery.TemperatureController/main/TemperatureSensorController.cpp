#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>

#include <algorithm>

#include "MqttController.hpp"
#include "TemperatureSensorController.hpp"
#include "ConfigController.hpp"

#include <messages.pb-c.h>

static const char* TAG = "sensor";

namespace Brewery {

static std::vector<std::string> ProbeNames = {
      "hlt",
      "boil",
      "mlt-in",
      "mlt-out"
};

spi_device_handle_t TemperatureSensorController::_spi;
spi_bus_config_t TemperatureSensorController::_buscfg;
spi_device_interface_config_t TemperatureSensorController::_devcfg;
SemaphoreHandle_t TemperatureSensorController::_conversionDoneSemaphore;
spi_transaction_t TemperatureSensorController::_tx;
uint8_t TemperatureSensorController::_txBuffer[NumSensors * 2];
uint8_t TemperatureSensorController::_rxBuffer[NumSensors * 2];
TaskHandle_t TemperatureSensorController::_taskHandle;
TemperatureSensorController::CalibrationData TemperatureSensorController::_calibrationData[NumSensors];
std::deque<TemperatureSensorController::SensorReadings> TemperatureSensorController::_readings;

void TemperatureSensorController::initialize()
{
   _conversionDoneSemaphore = xSemaphoreCreateBinary();

   // Load calibration data
   for (const std::string& probe : ProbeNames)
   {
      loadCalibrationParameters(probe.c_str());
   }

   _buscfg.miso_io_num = MISO_Pin;
   _buscfg.mosi_io_num = MOSI_Pin;
   _buscfg.sclk_io_num = CLK_Pin;
   _buscfg.quadhd_io_num = -1;
   _buscfg.quadwp_io_num = -1;

   _devcfg.clock_speed_hz = 1*1000*1000;          // Clock out at 1 MHz (TODO: make 10)
   _devcfg.mode           = 0;                    // SPI mode 0
   _devcfg.spics_io_num   = CS_Pin;               // CS pin
   _devcfg.queue_size     = 1;                    // We want to be able to queue 1 transaction at a time

   // Initialize the SPI bus
   ESP_ERROR_CHECK(spi_bus_initialize(HSPI_HOST, &_buscfg, 1));
   ESP_ERROR_CHECK(spi_bus_add_device(HSPI_HOST, &_devcfg, &_spi));

   gpio_config_t io_conf;
   io_conf.intr_type = GPIO_INTR_NEGEDGE;
   io_conf.mode = GPIO_MODE_INPUT;
   io_conf.pin_bit_mask = 1 << ConversionDone_Pin;
   io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
   io_conf.pull_up_en = GPIO_PULLUP_ENABLE;
   gpio_config(&io_conf);

   gpio_isr_handler_add((gpio_num_t)ConversionDone_Pin, handleConversionDoneInterrupt, nullptr);

   xTaskCreate(sampleThreadMain, "SampleThread", 4000, nullptr, 1, &_taskHandle);
}

const char* TemperatureSensorController::probeNumToName(int probeNum)
{
   if (probeNum > ProbeNames.size() - 1) return "";
   return ProbeNames[probeNum].c_str();
}

static Brewery__TemperatureProbe probeNumToProto(int probeNum)
{
   return (Brewery__TemperatureProbe)(probeNum + 1);
}

static int probeNameToNum(const char* name)
{
   for (int i = 0 ; i < ProbeNames.size() ; ++i)
   {
      if (ProbeNames[i] == name) return i;
   }
   return -1;
}

void TemperatureSensorController::setCalibrationParameters(const char* probe, float scale, float offset)
{
   int idx = probeNameToNum(probe);
   if (idx == -1) return;
   _calibrationData[idx].scale = scale;
   _calibrationData[idx].offset = offset;

   char key[64];
   sprintf(key, "%s.scale", probe);
   ConfigController::setFloat(key, scale);
   sprintf(key, "%s.offset", probe);
   ConfigController::setFloat(key, offset);
}

void TemperatureSensorController::getCalibrationParameters(const char* probe, float* scale, float* offset)
{
   int idx = probeNameToNum(probe);
   if (idx == -1)
   {
      *scale = 1.0;
      *offset = 0.0;
   }
   else
   {
      *scale = _calibrationData[idx].scale;
      *offset = _calibrationData[idx].offset;
   }
}

void TemperatureSensorController::loadCalibrationParameters(const char* probe)
{
   float scale = 1.0;
   float offset = 0.0;

   char key[64];
   sprintf(key, "%s.scale", probe);
   ConfigController::getFloat(key, &scale);
   sprintf(key, "%s.offset", probe);
   ConfigController::getFloat(key, &offset);

   int idx = probeNameToNum(probe);
   _calibrationData[idx].scale = scale;
   _calibrationData[idx].offset = offset;
}

void TemperatureSensorController::sampleThreadMain(void* params)
{
   char serializationBuffer[128];

   // Reset
   _tx.tx_data[0] = 0x10;
   _tx.length = 8;
   _tx.flags = SPI_TRANS_USE_TXDATA;
   ESP_ERROR_CHECK(spi_device_transmit(_spi, &_tx));

   // Averaging
   _tx.tx_data[0] = 0x3c;  // average of 32 converions; AVGON=1, NAVG1=1, NAVG0=1, NSCAN1=0, NSCAN0=0
   _tx.length = 8;
   _tx.flags = SPI_TRANS_USE_TXDATA;
   ESP_ERROR_CHECK(spi_device_transmit(_spi, &_tx));


   vTaskDelay(10 / portTICK_PERIOD_MS);

   SensorReadings readings;

   // this is intentionally set in the future to make sure we have enough data before the first send
   TickType_t nextSendTime = xTaskGetTickCount() + (2000 / portTICK_PERIOD_MS);

   while(1)
   {
      // Reset FIFO
      memset(&_tx, 0, sizeof(_tx));
      _tx.tx_data[0] = 0x18;
      _tx.length = 8;
      _tx.flags = SPI_TRANS_USE_TXDATA;
      ESP_ERROR_CHECK(spi_device_transmit(_spi, &_tx));

      vTaskDelay(1 / portTICK_PERIOD_MS);

      // Conversion
      memset(&_tx, 0, sizeof(_tx));
      _tx.tx_data[0] = 0xa0;
      _tx.length = 8;
      _tx.flags = SPI_TRANS_USE_TXDATA;
      ESP_ERROR_CHECK(spi_device_transmit(_spi, &_tx));

      xSemaphoreTake(_conversionDoneSemaphore, portMAX_DELAY);

      TickType_t tickCount = xTaskGetTickCount();

      memset(&_tx, 0, sizeof(_tx));
      _tx.length = NumSensors * 16;
      _tx.rxlength = NumSensors * 16;
      _tx.tx_buffer = _txBuffer;
      _tx.rx_buffer = _rxBuffer;
      ESP_ERROR_CHECK(spi_device_transmit(_spi, &_tx));

      readings.tickCount = tickCount;

      for (int i = 0 ; i < NumSensors ; ++i)
      {
         uint16_t value = (((uint16_t) _rxBuffer[i * 2]) << 8) | ((uint16_t) _rxBuffer[(i * 2) + 1]);
         readings.values[i] = value;
      }

      _readings.push_back(readings);

      // clean out expired readings (over 1s old)

      auto earliestTickCount = tickCount - (1000 / portTICK_PERIOD_MS);

      while (_readings.size() > 0 && _readings.front().tickCount < earliestTickCount)
      {
         _readings.pop_front();
      }

      if (tickCount >= nextSendTime)
      {
         std::vector<uint16_t> values(_readings.size());
         auto toTrim = _readings.size() / 10;

         for (int i = 0 ; i < NumSensors ; ++i)
         {
            const char* name = probeNumToName(i);

            // copy the values for this sensor into the holding vector
            for (int j = 0 ; j < _readings.size() ; ++j)
            {
               values[j] = _readings[j].values[i];
            }

            // sort the vector
            std::sort(values.begin(), values.end());

            // calculate the average, skipping the high and low values
            float totalValue = 0;
            uint32_t count = 0;

            for (int i = toTrim ; i < values.size() - toTrim ; ++i)
            {
               totalValue += (float)values[i];
               ++count;
            }

            float value = totalValue / (float)count;

            float volts = value * VoltsPerTick;
            float rawTemp = volts * 100.0;
            float temp = (rawTemp * _calibrationData[i].scale) + _calibrationData[i].offset;

            temp = std::roundf(temp * 1000.0) / 1000.0;
            rawTemp = std::roundf(rawTemp * 1000.0) / 1000.0;

            ESP_LOGI(TAG, "%8s: temp: %10.2f, raw: %10.2f", name, temp, rawTemp);

            Brewery__TemperatureUpdatedEvent tempUpdate = BREWERY__TEMPERATURE_UPDATED_EVENT__INIT;
            tempUpdate.probe = probeNumToProto(i);
            tempUpdate.temperature = temp;
            tempUpdate.unadjustedtemperature = rawTemp;
            size_t msgSize = brewery__temperature_updated_event__pack(&tempUpdate, reinterpret_cast<uint8_t*>(serializationBuffer));
            MqttController::publish("brewery/temp-update", serializationBuffer, msgSize);
         }

         nextSendTime = tickCount + (500 / portTICK_PERIOD_MS);
      }
   }
}


void TemperatureSensorController::handleConversionDoneInterrupt(void* data)
{
   BaseType_t higherPriorityTaskWoken = pdFALSE;

   xSemaphoreGiveFromISR(_conversionDoneSemaphore, &higherPriorityTaskWoken);

   if (higherPriorityTaskWoken == pdTRUE)
      portYIELD_FROM_ISR();
}

}