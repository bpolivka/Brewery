#ifndef BREWERY_CONFIGCONTROLLER_HPP
#define BREWERY_CONFIGCONTROLLER_HPP

#include <string>

#include <nvs_flash.h>

namespace Brewery {

class ConfigController
{
public:

   ConfigController() = delete;

   static void initialize();

   static void setString(const char* key, const char* value);
   static std::string getString(const char* name);

   static void setFloat(const char* key, float value);
   static bool getFloat(const char* key, float* value);

private:

   static nvs_handle _nvsHandle;
};

}

#endif //BREWERY_CONFIGCONTROLLER_HPP
