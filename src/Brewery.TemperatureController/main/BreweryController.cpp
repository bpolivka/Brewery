#include <cstring>
#include "esp_log.h"

#include "BreweryController.hpp"

static const char* TAG = "BreweryController";

namespace Brewery
{

BreweryController::BreweryController() :
      ledOutput(*this, "LED", GPIO_NUM_12, true)
{
}

void BreweryController::onConnected()
{
   ledOutput.setState(true);
}

void BreweryController::onDisconnected()
{
   ledOutput.setState(false);
}

} // namespace Brewery
