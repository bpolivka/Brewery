#ifndef __Brewery__MqttController_hpp__
#define __Brewery__MqttController_hpp__

#include <string>
#include <mqtt_client.h>

#include "NetworkController.hpp"

namespace Brewery {

class MqttController
{
public:

   struct Config
   {
      std::string uri;
   };

   enum class Status
   {
      Disconnected,
      Connected
   };

public:

   MqttController() = delete;

   static void initialize();

   static void setConfig(const Config& cfg);

   static void publish(const char* topic, const char* data, int len);

   static Status getStatus() { return _status; }
   static void setStatus(Status value);

   static Signal<Status> statusChanged;

private:

   static void loadConfig();
   static void updateConfig(const Config& cfg);

   static void start();
   static void stop();

   static esp_err_t handleEvent(esp_mqtt_event_handle_t event);

   static void onNetworkStatusChanged(NetworkController::Status status);

private:

   static Config _mqttConfig;
   static esp_mqtt_client_config_t _cfg;
   static esp_mqtt_client_handle_t _client;
   static Status _status;
};

} // namespace Brewery

#endif
