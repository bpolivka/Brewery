#ifndef Brewery__ServiceRegistry_hpp
#define Brewery__ServiceRegistry_hpp

#include <map>
#include <functional>

#include <esp_log.h>

namespace Brewery {

class ServiceRegistry
{
public:

   template <class T>
   T* getService()
   {
      return static_cast<registration<T>*>(_registrations[getTypeId<T>()])->resolve(*this);
   }

   template <class T>
   void registerService(const std::function<T*(ServiceRegistry&)>& func)
   {
      auto* reg = new registration<T>;
      reg->func = func;
      reg->obj = nullptr;
      _registrations[getTypeId<T>()] = reg;
   }

private:

   template <class T>
   static void* getTypeId()
   {
      static void* id = nullptr;
      return &id;
   }

   struct registration_base
   {
   };

   template <class T>
   struct registration : registration_base
   {
      T* resolve(ServiceRegistry& svcProvider)
      {
         if (!obj)
            obj = func(svcProvider);
         return obj;
      }

      T* obj;
      std::function<T*(ServiceRegistry&)> func;
   };

private:

   std::map<void*, registration_base*> _registrations;
};

} // namespace Brewery

#endif // Brewery__ServiceRegistry_hpp
