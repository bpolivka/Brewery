import Vue from 'vue'
import Vuex from 'vuex'
import pbBreweryRoot from '@brewery/protobuf'
import Signal from 'signals'

const pbBrewery = pbBreweryRoot.brewery

Vue.use(Vuex)

class Output {
  constructor (name, pbOutput) {
    this.name = name
    this._enabled = false
    this.pbOutput = pbOutput
    this._dependentOutput = null
    this.enabledChanged = new Signal()
  }

  setDependentOutput (output) {
    this._dependentOutput = output

    this._dependentOutput.enabledChanged.add(() => {
      if (!this._dependentOutput.enabled) {
        this.enabled = false
      }
    })
  }

  get enabled () { return this._enabled }

  set enabled (value) {
    if (this.enabled === value) return

    if (value && this._dependentOutput !== null && !this._dependentOutput.enabled) {
      return
    }

    this._enabled = value
    this.enabledChanged.dispatch()
  }

  buildState () {
    return pbBrewery.OutputState.create({
      output: this.pbOutput,
      enabled: this.enabled
    })
  }
}

class TemperatureProbe {
  constructor (name, pbProbe) {
    this.name = name
    this.pbProbe = pbProbe
    this._temperature = 0.0
    this.temperatureChanged = new Signal()
  }

  get temperature () { return this._temperature }

  set temperature (value) {
    if (this.temperature === value) return
    this._temperature = value
    this.temperatureChanged.dispatch()
  }
}

class Tank {
  constructor (name, probe, ssrOutput) {
    this.name = name
    this.probe = probe
    this.ssrOutput = ssrOutput
    this.coolingRate = 0.01
    this.heatingRate = 0.1
    this._heating = false
    this._lastUpdate = (new Date()).getTime()
  }

  runSimulation () {
    const currentTime = (new Date()).getTime()
    const dTime = (currentTime - this._lastUpdate) / 1000.0
    var factor = -this.coolingRate
    if (this._heating) {
      factor += this.heatingRate
    }

    var newTemp = this.probe.temperature + (dTime * factor)
    newTemp = Math.max(newTemp, 65.0)
    newTemp = Math.min(newTemp, 212.0)

    this.probe.temperature = newTemp
    this._lastUpdate = currentTime
    this._heating = this.ssrOutput.enabled
  }
}

class MLTTank {
  constructor (name, hltTank, inProbe, outProbe) {
    this.name = name
    this._hltTank = hltTank
    this.inProbe = inProbe
    this.outProbe = outProbe
    this._lastUpdate = (new Date()).getTime()
    this.inHeatingRate = 0.1
    this.outHeatingRate = 0.1
  }

  runSimulation () {
    const currentTime = (new Date()).getTime()
    const dTime = (currentTime - this._lastUpdate) / 1000.0

    const inFactor =
      (this._hltTank.probe.temperature - this.outProbe.temperature) *
      this.inHeatingRate

    const inTemp = this.outProbe.temperature + (inFactor * dTime)

    const outFactor =
      (inTemp - this.outProbe.temperature) *
      this.outHeatingRate

    const outTemp = this.outProbe.temperature + (outFactor * dTime)

    this.inProbe.temperature = inTemp
    this.outProbe.temperature = outTemp

    this._lastUpdate = currentTime
  }
}

const hltOutput = new Output('HLT', pbBrewery.Output.OUTPUT_HLT)
const hltSsrOutput = new Output('HLTSSR', pbBrewery.Output.OUTPUT_HLT_SSR)
hltSsrOutput.setDependentOutput(hltOutput)
const boilOutput = new Output('Boil', pbBrewery.Output.OUTPUT_BOIL)
const boilSsrOutput = new Output('BoilSSR', pbBrewery.Output.OUTPUT_BOIL_SSR)
boilSsrOutput.setDependentOutput(boilOutput)
const pump1Output = new Output('Pump1', pbBrewery.Output.OUTPUT_PUMP1)
const pump2Output = new Output('Pump2', pbBrewery.Output.OUTPUT_PUMP2)
const stirOutput = new Output('Stir', pbBrewery.Output.OUTPUT_STIR)
const fanOutput = new Output('Fan', pbBrewery.Output.OUTPUT_FAN)
const buzzOutput = new Output('Buzz', pbBrewery.Output.OUTPUT_BUZZ)
const tbdOutput = new Output('TBD', pbBrewery.Output.OUTPUT_TBD)

const allOutputs = [
  hltOutput,
  hltSsrOutput,
  boilOutput,
  boilSsrOutput,
  pump1Output,
  pump2Output,
  stirOutput,
  fanOutput,
  buzzOutput,
  tbdOutput
]

const hltProbe = new TemperatureProbe(
  'hlt',
  pbBrewery.TemperatureProbe.TEMPERATURE_PROBE_HLT)

const mltInProbe = new TemperatureProbe(
  'mlt-in',
  pbBrewery.TemperatureProbe.TEMPERATURE_PROBE_MLT_IN)

const mltOutProbe = new TemperatureProbe(
  'mlt-out',
  pbBrewery.TemperatureProbe.TEMPERATURE_PROBE_MLT_OUT)

const boilProbe = new TemperatureProbe(
  'boil',
  pbBrewery.TemperatureProbe.TEMPERATURE_PROBE_BOIL)

hltProbe.temperature = 123.4
mltInProbe.temperature = 110.1
mltOutProbe.temperature = 108.7
boilProbe.temperature = 201.5

const hltTank = new Tank('MLT', hltProbe, hltSsrOutput)
const mltTank = new MLTTank('MLT', hltTank, mltInProbe, mltOutProbe)
const boilTank = new Tank('Boil', boilProbe, boilSsrOutput)

function getOutput (pbOutput) {
  switch (pbOutput) {
    case pbBrewery.Output.OUTPUT_HLT: return hltOutput
    case pbBrewery.Output.OUTPUT_BOIL: return boilOutput
    case pbBrewery.Output.OUTPUT_PUMP1: return pump1Output
    case pbBrewery.Output.OUTPUT_PUMP2: return pump2Output
    case pbBrewery.Output.OUTPUT_STIR: return stirOutput
    case pbBrewery.Output.OUTPUT_FAN: return fanOutput
    case pbBrewery.Output.OUTPUT_BUZZ: return buzzOutput
    case pbBrewery.Output.OUTPUT_TBD: return tbdOutput
    case pbBrewery.Output.OUTPUT_HLT_SSR: return hltSsrOutput
    case pbBrewery.Output.OUTPUT_BOIL_SSR: return boilSsrOutput
    default: throw new Error('Invalid output')
  }
}

function buildOutputStates () {
  return allOutputs.map(o => o.buildState())
}

function buildSystemStateEvent () {
  return pbBrewery.SystemStateEvent.create({
    outputStates: buildOutputStates()
  })
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const store = new Vuex.Store({
  strict: true,

  state: {
    probes: {
      hlt: hltProbe,
      boil: boilProbe,
      mltIn: mltInProbe,
      mltOut: mltOutProbe
    },
    outputs: {
      hlt: hltOutput,
      boil: boilOutput,
      pump1: pump1Output,
      pump2: pump2Output,
      stir: stirOutput,
      fan: fanOutput,
      buzz: buzzOutput,
      tbd: tbdOutput,
      hltSsr: hltSsrOutput,
      boilSsr: boilSsrOutput
    },
    tanks: {
      hlt: hltTank,
      mlt: mltTank,
      boil: boilTank
    }
  },

  mutations: {
    setOutputEnabled (state, { output, enabled }) {
      output.enabled = enabled
    },

    setHeatingRate (state, { tank, value }) {
      tank.heatingRate = value
    },

    setCoolingRate (state, { tank, value }) {
      tank.coolingRate = value
    },

    setInHeatingRate (state, { tank, value }) {
      tank.inHeatingRate = value
    },

    setOutHeatingRate (state, { tank, value }) {
      tank.outHeatingRate = value
    },

    runSimulation (state) {
      this.state.tanks.hlt.runSimulation()
      this.state.tanks.boil.runSimulation()
      this.state.tanks.mlt.runSimulation()
    },

    updateOutput (state, { output, updateHandler }) {
      updateHandler(output)
    },

    updateProbe (state, { probe, updateHandler }) {
      updateHandler(probe)
    }
  },

  actions: {
    handleSetOutputStateCommand ({ commit }, cmd) {
      const output = getOutput(cmd.state.output)
      const enabled = cmd.state.enabled
      commit('setOutputEnabled', { output, enabled })
    },

    async handleEnableOutputForTimeCommand ({ commit }, cmd) {
      const output = getOutput(cmd.output)
      commit('setOutputEnabled', { output, enabled: true })
      await sleep(cmd.milliseconds)
      commit('setOutputEnabled', { output, enabled: false })

      const evt = pbBrewery.RequestCompletedEvent.create({
        requestId: cmd.requestId
      })

      return this.$mqtt.publish(
        cmd.responseTopic,
        pbBrewery.RequestCompletedEvent.encode(evt).finish()
      )
    },

    async handleSendSystemStateCommand (_, cmd) {
      const evt = buildSystemStateEvent()

      return this.$mqtt.publish(
        cmd.responseTopic,
        pbBrewery.SystemStateEvent.encode(evt).finish()
      )
    }
  }
})

setInterval(() => store.commit('runSimulation'), 1000)

export default store
