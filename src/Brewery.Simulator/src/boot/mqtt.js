import { connect } from 'mqtt'
import { Platform } from 'quasar'
import pbBreweryRoot from '@brewery/protobuf'

const pbBrewery = pbBreweryRoot.brewery

// const rabbitmqUser = 'brewery'
// const rabbitmqPassword = 'brewery'
// const rabbitmqHost = 'rabbitmq.services.bolonium.org'

const rabbitmqUser = 'guest'
const rabbitmqPassword = 'guest'
const rabbitmqHost = 'localhost'

const mqttUrl =
  Platform.is.electron
    ? `tcp://${rabbitmqUser}:${rabbitmqPassword}@${rabbitmqHost}:1883`
    : `wss://${rabbitmqUser}:${rabbitmqPassword}@${rabbitmqHost}:15673/ws`

function handleOutputStateChangeMessage (store, payload) {
  const cmd = pbBrewery.SetOutputStateCommand.decode(payload)
  store.dispatch('handleSetOutputStateCommand', cmd)
}

function handleOutputEnableForTimeMessage (store, payload) {
  const cmd = pbBrewery.EnableOutputForTimeCommand.decode(payload)
  store.dispatch('handleEnableOutputForTimeCommand', cmd)
}

function handleSendSystemStateMessage (store, payload) {
  const cmd = pbBrewery.SendSystemStateCommand.decode(payload)
  store.dispatch('handleSendSystemStateCommand', cmd)
}

const handlerMap = {
  'brewery/output/set-state': handleOutputStateChangeMessage,
  'brewery/output/enable-for-time': handleOutputEnableForTimeMessage,
  'brewery/system/send-state': handleSendSystemStateMessage
}

function handleMessage (store, topic, payload) {
  // console.log('@@@ handleMessage: %s', topic)
  const handler = handlerMap[topic]
  if (handler !== undefined) {
    handler(store, payload)
  }
}

function onOutputEnabledChanged (output, mqtt) {
  const evt = pbBrewery.OutputStateChangedEvent.create({
    state: {
      output: output.pbOutput,
      enabled: output.enabled
    }
  })

  mqtt.publish(
    'brewery/output/state-changed',
    pbBrewery.OutputStateChangedEvent.encode(evt).finish()
  )
}

function onTemperatureChanged (probe, mqtt) {
  const evt = pbBrewery.TemperatureUpdatedEvent.create({
    probe: probe.pbProbe,
    temperature: probe.temperature,
    unadjustedTemperature: probe.temperature
  })

  mqtt.publish(
    'brewery/temp-probe/updated',
    pbBrewery.TemperatureUpdatedEvent.encode(evt).finish()
  )
}

function registerOutputEvents (store) {
  const mqtt = store.$mqtt
  Object.values(store.state.outputs).forEach(output => {
    const handler = o => {
      o.enabledChanged.add(() => onOutputEnabledChanged(o, mqtt))
    }
    store.commit('updateOutput', { output, updateHandler: handler })
  })
}

function registerProbeEvents (store) {
  const mqtt = store.$mqtt
  Object.values(store.state.probes).forEach(probe => {
    const handler = p => {
      p.temperatureChanged.add(() => onTemperatureChanged(p, mqtt))
    }
    store.commit('updateProbe', { probe, updateHandler: handler })
  })
}

export default ({ store }) => {
  const client = connect(mqttUrl)
  store.$mqtt = client

  registerOutputEvents(store)
  registerProbeEvents(store)

  client.subscribe('brewery/output/set-state')
  client.subscribe('brewery/output/enable-for-time')
  client.subscribe('brewery/system/send-state')

  client.on('message', (topic, message) => {
    handleMessage(store, topic, message)
  })
}
