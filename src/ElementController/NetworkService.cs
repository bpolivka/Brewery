﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Extensions.ManagedClient;
using Microsoft.Extensions.Logging;
using MQTTnet.Client.Options;
using System.Linq;
using Google.Protobuf;

namespace Brewery.ElementController
{
  class NetworkService : IHostedService
  {
    readonly MqttSettings _mqttSettings;
    readonly IManagedMqttClient _mqttClient;
    readonly ILogger _logger;
    readonly Brewery _brewery;

    readonly Dictionary<string, Action<MqttApplicationMessage>> _msgHandlers;

    public NetworkService(
            Brewery brewery,
            IOptionsSnapshot<MqttSettings> mqttOptions,
            ILoggerFactory loggerFactory)
    {
      _brewery = brewery;
      _logger = loggerFactory.CreateLogger<NetworkService>();

      _mqttSettings = mqttOptions.Value;

      _msgHandlers = new Dictionary<string, Action<MqttApplicationMessage>>
      {
        ["brewery/temp-probe/updated"] = HandleTemperatureUpdate,
        ["brewery/output/state-changed"] = HandleOutputStateChanged,
        ["brewery/controller/update-strategy"] = HandleUpdateControllerStrategy,
        [_brewery.RequestCompletedTopic] = HandleRequestCompleted
      };

      _mqttClient = new MqttFactory().CreateManagedMqttClient();
      _mqttClient.UseApplicationMessageReceivedHandler(OnApplicationMessageReceived);
    }

    void OnApplicationMessageReceived(MqttApplicationMessageReceivedEventArgs arg)
    {
      if (_msgHandlers.TryGetValue(arg.ApplicationMessage.Topic, out var handler))
      {
        handler(arg.ApplicationMessage);
      }
    }

    void HandleTemperatureUpdate(MqttApplicationMessage msg)
    {
      var protoMsg = Protobuf.TemperatureUpdatedEvent.Parser.ParseFrom(msg.Payload);
      _brewery.TemperatureProbes.Apply(protoMsg);
    }

    void HandleOutputStateChanged(MqttApplicationMessage msg)
    {
      var protoMsg = Protobuf.OutputStateChangedEvent.Parser.ParseFrom(msg.Payload);
      _brewery.Outputs.Apply(protoMsg);
    }

    void HandleRequestCompleted(MqttApplicationMessage msg)
    {
      var protoMsg = Protobuf.RequestCompletedEvent.Parser.ParseFrom(msg.Payload);
      Console.WriteLine($"Request completed: {protoMsg.RequestId}");
      _brewery.OnRequestCompleted(protoMsg.RequestId);
    }

    void HandleUpdateControllerStrategy(MqttApplicationMessage msg)
    {
      var protoMsg = Protobuf.UpdateControllerStategyCommand.Parser.ParseFrom(msg.Payload);
      _brewery.Controllers.Apply(protoMsg);
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
      var options = new ManagedMqttClientOptionsBuilder()
          .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
          .WithClientOptions(new MqttClientOptionsBuilder()
              .WithClientId(_mqttSettings.ClientId)
              .WithTcpServer(_mqttSettings.Server)
              .WithCredentials(_mqttSettings.Username, _mqttSettings.Password)
              .Build())
          .Build();

      await _mqttClient.SubscribeAsync(
          _msgHandlers.Keys.Select(x => new TopicFilter { Topic = x })
      );

      _brewery.MessagePublisher = new MessagePublisher(_mqttClient);

      await _mqttClient.StartAsync(options);
    }

    public async Task StopAsync(CancellationToken cancellationToken)
    {
      await _mqttClient.StopAsync();
      _mqttClient.Dispose();
    }

    class MessagePublisher : IMessagePublisher
    {
      private readonly IApplicationMessagePublisher _publisher;

      public MessagePublisher(IApplicationMessagePublisher publisher)
      {
        _publisher = publisher;
      }
      public async Task PublishMessageAsync(string topic, IMessage msg)
      {
        var mqttMessage = new MqttApplicationMessage
        {
          Topic = topic,
          Payload = msg.ToByteArray()
        };

        await _publisher.PublishAsync(mqttMessage, CancellationToken.None);
      }
    }
  }
}
