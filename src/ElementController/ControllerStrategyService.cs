﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Brewery.ElementController
{
  class ControllerStrategyService : IHostedService
  {
    readonly Brewery _brewery;
    readonly IMongoClient _mongoClient;
    readonly ILogger _logger;
    readonly IMongoDatabase _database;
    readonly IMongoCollection<ControllerStrategyDocument> _collection;

    public ControllerStrategyService(
      Brewery brewery,
      IMongoClient mongoClient,
      ILoggerFactory loggerFactory)
    {
      _brewery = brewery;
      _mongoClient = mongoClient;
      _logger = loggerFactory.CreateLogger<ControllerStrategyService>();

      _database = _mongoClient.GetDatabase("brewery");
      _collection = _database.GetCollection<ControllerStrategyDocument>("controller-strategies");
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
      var strategyDocs = await _collection.AsQueryable().ToListAsync();
      foreach (var doc in strategyDocs)
      {
        ProcessStrategyDocument(doc);
      }

      using (var cursor = await _collection.WatchAsync())
      {
          await cursor.ForEachAsync(change =>
          {
            switch (change.OperationType)
            {
              case ChangeStreamOperationType.Insert:
              case ChangeStreamOperationType.Replace:
              case ChangeStreamOperationType.Update:
                _brewery.Controllers.UpdateStrategy(change.FullDocument);
                break;
              case ChangeStreamOperationType.Delete:
                //_brewery.Controllers.RemoveStrategy(change.DocumentKey);
                break;
            }
          });
      }
    }

    private void ProcessStrategyDocument(ControllerStrategyDocument doc)
    {
      _brewery.Controllers.UpdateStrategy(doc);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
      return Task.FromResult(true);
    }
  }
}
