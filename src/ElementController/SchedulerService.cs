﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Nito.AsyncEx;

namespace Brewery.ElementController
{
  class SchedulerService : IHostedService
  {
    readonly SchedulerSettings _settings;
    readonly CancellationTokenSource _cts = new CancellationTokenSource();
    readonly AsyncMonitor _monitor = new AsyncMonitor();
    readonly Brewery _brewery;

    public SchedulerService(
      IOptionsSnapshot<SchedulerSettings> schedulerSettings,
      Brewery brewery)
    {
      _settings = schedulerSettings.Value;
      _brewery = brewery;
      _brewery.Controllers.ControllerIsActiveChanged += OnControllerIsActiveChanged;
    }

    private void OnControllerIsActiveChanged(Controller obj)
    {
      Console.WriteLine("OnControllerIsActive: enter");
      using (_monitor.Enter())
      {
        _monitor.Pulse();
      }
      Console.WriteLine("OnControllerIsActive: exit");
    }

    public Task StartAsync(CancellationToken cancellationToken)
    {
      SchedulerLoop();
      return Task.FromResult(true);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
      _cts.Cancel();
      return Task.FromResult(true);
    }

    private async void SchedulerLoop()
    {
      try
      {
        var activeControllers = new List<Controller>();
        var prevControllers = new List<Controller>();

        while (true)
        {
          using (_monitor.Enter())
          {
            prevControllers.Clear();
            prevControllers.AddRange(activeControllers);
            activeControllers.Clear();
            activeControllers.AddRange(_brewery.Controllers.GetActiveControllers());

            foreach (var controller in prevControllers)
            {
              if (!activeControllers.Contains(controller))
              {
                await controller.SendUpdateAsync(new Protobuf.ControllerUpdatedEvent
                {
                  DutyCycle = 0.0F,
                  Strategy = "Disabled"
                });
              }
            }

            if (activeControllers.Count == 0)
            {
              await _monitor.WaitAsync(_cts.Token);
              continue;
            }
          }

          var maxDutyCycle = 1.0 / (double)activeControllers.Count;

          foreach (var controller in activeControllers)
          {
            var evt = controller.Calculate(maxDutyCycle);
            await controller.SendUpdateAsync(evt);
            
            var dutyCycle = evt.DutyCycle;
            
            var timeSlice = _settings.PeriodSeconds * maxDutyCycle;
            var timeOn = _settings.PeriodSeconds * dutyCycle;
            var timeOff = timeSlice - timeOn;

            Console.WriteLine($"Controller {controller.Name} on for {timeOn:n3}");
            await controller.EnableForTimeAsync((int)(timeOn * 1000.0));
            Console.WriteLine($"Controller {controller.Name} off");
            Console.WriteLine($"  waiting {timeOff:n3}");
            await Task.Delay((int)(timeOff * 1000.0));
          }
        }
      }
      catch (OperationCanceledException)
      {
      }
    }
  }
}
