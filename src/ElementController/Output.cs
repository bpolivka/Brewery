﻿using System;
using System.Threading.Tasks;
using Brewery.Protobuf;

namespace Brewery.ElementController
{
  public class Output
  {
    readonly Brewery _brewery;
    readonly Protobuf.Output _id;

    bool _enabled;

    public Output(Brewery brewery, Protobuf.Output id)
    {
      _brewery = brewery;
      _id = id;
    }

    public void Apply(Protobuf.OutputState state)
    {
      Enabled = state.Enabled;
    }

    public bool Enabled
    {
      get => _enabled;
      private set
      {
        _enabled = value;
        EnabledChanged?.Invoke();
      }
    }

    public async Task EnableForTimeAsync(int milliseconds)
    {
      var handle = _brewery.GetCompletionHandle();

      var msg = new Protobuf.EnableOutputForTimeCommand
      {
        Output = _id,
        Milliseconds = (uint)milliseconds,
        ResponseTopic = handle.ResponseTopic,
        RequestId = handle.RequestId
      };

      await _brewery.MessagePublisher.PublishMessageAsync("brewery/output/enable-for-time", msg);
      await handle.Task;
    }

    public event Action EnabledChanged;
  }
}
