using Brewery.Protobuf;
using Google.Protobuf.WellKnownTypes;

namespace Brewery.ElementController
{
  public class ControllerStrategy
  {
    public ControllerStrategy(string name)
    {
      Name = name;
    }

    public string Name { get; private set; }
    public string Type => Impl.Type;

    public IControllerStrategyImpl Impl { get; set; }

    public ControllerUpdatedEvent Calculate(double maxDutyCycle)
    {
      return Impl.Calculate(maxDutyCycle);
    }

    public Struct UpdateParameters(Struct parameters)
    {
      return Impl.UpdateParameters(parameters);
    }
  }
}
