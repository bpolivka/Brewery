using System;

namespace Brewery.ElementController
{
  public class ControllerStrategyImplFactory
  {
    private readonly Brewery _brewery;

    public ControllerStrategyImplFactory(Brewery brewery)
    {
      _brewery = brewery;
    }
    public IControllerStrategyImpl Create(string type)
    {
      switch (type)
      {
        case "manual": return new ManualControllerStrategyImpl();
        case "pid": return new PidControllerStrategyImpl(_brewery);
        default: throw new ArgumentException($"Unknown strategy type: '{type}'");
      }
    }
  }
}
