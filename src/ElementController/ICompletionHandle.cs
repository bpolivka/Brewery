using System.Threading.Tasks;

namespace Brewery.ElementController
{
  public interface ICompletionHandle
  {
    Task Task { get; }
    string ResponseTopic { get; }
    uint RequestId { get; }
  }
}
