using System;
using System.Collections.Generic;
using System.Linq;
using Brewery.Protobuf;

namespace Brewery.ElementController
{
  public class TemperatureProbeManager
  {
    readonly Dictionary<Protobuf.TemperatureProbe, TemperatureProbe> _probeDict = 
      new Dictionary<Protobuf.TemperatureProbe, TemperatureProbe>();
    
    public TemperatureProbeManager()
    {
      HLT = CreateTemperatureProbe("hlt", Protobuf.TemperatureProbe.Hlt);
      MLTIn = CreateTemperatureProbe("mlt-in", Protobuf.TemperatureProbe.MltIn);
      MLTOut = CreateTemperatureProbe("mlt-out", Protobuf.TemperatureProbe.MltOut);
      Boil = CreateTemperatureProbe("boil", Protobuf.TemperatureProbe.Boil);
    }

    public TemperatureProbe HLT { get; private set; }
    public TemperatureProbe MLTIn { get; private set; }
    public TemperatureProbe MLTOut { get; private set; }
    public TemperatureProbe Boil { get; private set; }

    public TemperatureProbe GetByName(string name)
    {
      return _probeDict.Values.First(p => p.Name == name);
    }

    public void Apply(Protobuf.TemperatureUpdatedEvent msg)
    {
      _probeDict[msg.Probe].Apply(msg);
    }

    private TemperatureProbe CreateTemperatureProbe(string name, Protobuf.TemperatureProbe id)
    {
      var probe = new TemperatureProbe(name);
      _probeDict[id] = probe;
      return probe;
    }
  }
}
