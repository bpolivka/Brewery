using System;
using System.Collections.Generic;
using Brewery.Protobuf;

namespace Brewery.ElementController
{
  public class OutputManager
  {
    readonly Brewery _brewery;

    readonly Dictionary<Protobuf.Output, Output> _byId = 
      new Dictionary<Protobuf.Output, Output>();
    
    public OutputManager(Brewery brewery)
    {
      _brewery = brewery;

      HLT = CreateOutput(Protobuf.Output.Hlt);
      Boil = CreateOutput(Protobuf.Output.Boil);
      HLTSSR = CreateOutput(Protobuf.Output.HltSsr);
      BoilSSR = CreateOutput(Protobuf.Output.BoilSsr);
    }

    public Output HLT { get; private set; }
    public Output HLTSSR { get; private set; }
    public Output Boil { get; private set; }
    public Output BoilSSR { get; private set; }

    public void Apply(Protobuf.OutputStateChangedEvent msg)
    {
      if (_byId.TryGetValue(msg.State.Output, out var output))
      {
        output.Apply(msg.State);
      }
    }

    private Output CreateOutput(Protobuf.Output id)
    {
      var output = new Output(_brewery, id);
      _byId[id] = output;
      return output;
    }
  }
}
