﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Brewery.Protobuf;

namespace Brewery.ElementController
{
  public class Controller
  {
    readonly Brewery _brewery;
    readonly Output _contactorOutput;
    readonly Output _ssrOutput;
    readonly ControllerStrategy _manualStrategy;
    readonly ConcurrentDictionary<string, ControllerStrategy> _availableStrategies = new ConcurrentDictionary<string, ControllerStrategy>();
    public Controller(Brewery brewery, string name, Protobuf.Controller id, Output contactorOutput, Output ssrOutput)
    {
      _brewery = brewery;

      Name = name;
      Id = id;
      
      _manualStrategy = new ControllerStrategy("Manual")
      {
        Impl = new ManualControllerStrategyImpl()
      };

      AddStrategy(_manualStrategy);

      Strategy = _manualStrategy;

      _contactorOutput = contactorOutput;
      _ssrOutput = ssrOutput;

      _contactorOutput.EnabledChanged += OnContactorEnabledChanged;
    }

    void AddStrategy(ControllerStrategy strategy)
    {
      _availableStrategies[strategy.Name] = strategy;
    }

    public Protobuf.Controller Id { get; private set; }
    public string Name { get; private set; }
    public ControllerStrategy Strategy { get; set; }

    private void OnContactorEnabledChanged()
    {
      IsActiveChanged?.Invoke(this);
    }

    public void Apply(UpdateControllerStategyCommand cmd)
    {
      var parameters = Strategy.UpdateParameters(cmd.Parameters);
      
      var evt = new ControllerStrategyUpdatedEvent
      {
        Controller = Id,
        Strategy = Strategy.Name,
        Parameters = parameters
      };

      _brewery.MessagePublisher.PublishMessageAsync(
        "brewery/controller/strategy-updated", evt);
    }

    void PublishControllerStrategyUpdated()
    {
      //var parameters = Strategy.GetParameters();

      var evt = new ControllerStrategyUpdatedEvent
      {
        Controller = Id,
        Strategy = Strategy.Name,
        //Parameters = parameters
      };

      _brewery.MessagePublisher.PublishMessageAsync(
        "brewery/controller/strategy-updated", evt);
    }

    void PublishControllerStrategyAdded(ControllerStrategy strategy)
    {
      var evt = new ControllerStrategyAddedEvent
      {
        Strategy = new Protobuf.ControllerStrategy
        {
          Controller = Id,
          Name = strategy.Name,
          Type = strategy.Type
        }
      };

      _brewery.MessagePublisher.PublishMessageAsync(
        "brewery/controller/strategy-added", evt);
    }
    void PublishControllerStrategyRemoved(ControllerStrategy strategy)
    {
      var evt = new ControllerStrategyRemovedEvent
      {
        Strategy = new Protobuf.ControllerStrategy
        {
          Controller = Id,
          Name = strategy.Name,
          Type = strategy.Type
        }
      };

      _brewery.MessagePublisher.PublishMessageAsync(
        "brewery/controller/strategy-removed", evt);
    }

    void ChangeStrategy(ControllerStrategy strategy)
    {
      Strategy = strategy;
      PublishControllerStrategyUpdated();
    }

    internal void RemoveStrategy(ControllerStrategyDocument doc)
    {
      if (!_availableStrategies.TryRemove(doc.Name, out var strategy))
        return;

      if (Strategy == strategy)
      {
        ChangeStrategy(_manualStrategy);
      }

      PublishControllerStrategyRemoved(strategy);
    }

    internal void AddOrUpdateStrategy(ControllerStrategyDocument doc)
    {
      if (!_availableStrategies.TryGetValue(doc.Name, out var strategy))
      {
        strategy = new ControllerStrategy(doc.Name)
        {
          Impl = _brewery.StrategyImplFactory.Create(doc.Type)
        };
        AddStrategy(strategy);
        PublishControllerStrategyAdded(strategy);     
      }
      else 
      {
        if (strategy.Type != doc.Type)
        {
          PublishControllerStrategyRemoved(strategy);     
          strategy.Impl = _brewery.StrategyImplFactory.Create(doc.Type);
          PublishControllerStrategyAdded(strategy);     
        }
      }

      strategy.Impl.UpdateSettings(doc.Settings);
    }

    public ControllerUpdatedEvent Calculate(double maxDutyCycle)
    {
      var evt = Strategy.Calculate(maxDutyCycle);
      evt.Strategy = Strategy.Name;
      return evt;
    }

    public Task SendUpdateAsync(ControllerUpdatedEvent evt)
    {
      evt.Controller = Id;
      return _brewery.MessagePublisher.PublishMessageAsync("brewery/controller/updated", evt);
    }

    public Task EnableForTimeAsync(int milliseconds)
    {
      return _ssrOutput.EnableForTimeAsync(milliseconds);
    }

    public bool IsActive => _contactorOutput.Enabled;

    public event Action<Controller> IsActiveChanged;
  }
}
