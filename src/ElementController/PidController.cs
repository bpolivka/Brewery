using System;

namespace Brewery.ElementController
{
  class PidController
  {
    DateTime _lastTime;
    double? _lastInput;

    public double Proportional { get; private set; }
    public double Integral { get; private set; }
    public double Derivative { get; private set; }

    public double MinValue { get; set; } = 0.0;
    public double MaxValue { get; set; } = 100.0;

    public bool ProportionalOnMeasurement { get; set; }
    public double Setpoint { get; set; }
    public double Kp { get; set; }
    public double Ki { get; set; }
    public double Kd { get; set; }

    public double Calculate(double input)
    {
      var now = DateTime.Now;
      var dt = (now - _lastTime).TotalSeconds;

      // compute error terms
      var error = Setpoint - input;
      var dInput = input - (_lastInput ?? input);

      // compute the proportional term
      if (!ProportionalOnMeasurement)
        Proportional = Kp * error;
      else
        Proportional -= Kp * dInput;

      // compute integral and derivative terms
      Integral += Ki * error * dt;
      Integral = Clamp(Integral);

      Derivative = -Kd * dInput / dt;

      // compute final output
      var output = Proportional + Integral + Derivative;
      output = Clamp(output);

      // keep track of state
      _lastInput = input;
      _lastTime = now;

      return output;
    }

    void Reset()
    {
      _lastInput = null;
      _lastTime = DateTime.Now;
      Proportional = 0.0;
      Integral = 0.0;
      Derivative = 0.0;
    }

    double Clamp(double value)
    {
      if (value > MaxValue) return MaxValue;
      if (value < MinValue) return MinValue;
      return value;
    }
  }
}
