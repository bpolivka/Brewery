using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Brewery.Protobuf;
using MongoDB.Driver;

namespace Brewery.ElementController
{
  public class Brewery
  {
    int _requestId = 1;
    readonly Dictionary<uint, CompletionHandle> _completionHandles = new Dictionary<uint, CompletionHandle>();

    public Brewery()
    {
      Outputs = new OutputManager(this);
      Controllers = new ControllerManager(this);
      StrategyImplFactory = new ControllerStrategyImplFactory(this);
    }

    public string RequestCompletedTopic { get; }  = $"brewery/{Guid.NewGuid()}/request-completed";

    public IMessagePublisher MessagePublisher { get; set; }

    public TemperatureProbeManager TemperatureProbes { get; } = new TemperatureProbeManager();
    public OutputManager Outputs { get; private set; }
    public ControllerManager Controllers { get; private set; }
    public ControllerStrategyImplFactory StrategyImplFactory { get; private set; }

    public ICompletionHandle GetCompletionHandle()
    {
      var id = (uint)Interlocked.Increment(ref _requestId);
      var handle = new CompletionHandle(RequestCompletedTopic, id);
      lock (_completionHandles)
      {
        _completionHandles[id] = handle;
      }
      return handle;
    }

    public void OnRequestCompleted(uint requestId)
    {
      lock (_completionHandles)
      {
        if (!_completionHandles.TryGetValue(requestId, out var handle))
          return;
        _completionHandles.Remove(requestId);
        handle.TCS.SetResult(true);
      }
    }

    class CompletionHandle : ICompletionHandle
    {
      public CompletionHandle(string responseTopic, uint requestId)
      {
        ResponseTopic = responseTopic;
        RequestId = requestId;
      }
      public TaskCompletionSource<bool> TCS { get; } = new TaskCompletionSource<bool>();

      public Task Task => TCS.Task;
      public string ResponseTopic { get; private set; }
      public uint RequestId { get; private set;}
    }
  }
}
