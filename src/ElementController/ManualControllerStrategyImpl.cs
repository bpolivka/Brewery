﻿using System;
using Brewery.Protobuf;
using Google.Protobuf.WellKnownTypes;
using MongoDB.Bson;

namespace Brewery.ElementController
{
  class ManualControllerStrategyImpl : IControllerStrategyImpl
  {
    class Parameters
    {
      public double TargetDutyCycle { get; set; }
    }

    Parameters _parameters = new Parameters();
    Struct _values = new Struct();

    public string Type => "manual";

    public ManualControllerStrategyImpl()
    {
      UpdateValues();
    }

    public ControllerUpdatedEvent Calculate(double maxDutyCycle)
    {
      return new ControllerUpdatedEvent
      {
        DutyCycle = Math.Min(_parameters.TargetDutyCycle, maxDutyCycle),
        Values = _values
      };
    }

    void UpdateValues()
    {
      _values.Fields["TargetDutyCycle"] = Value.ForNumber(_parameters.TargetDutyCycle);
    }

    public Struct UpdateParameters(Struct parameters)
    {
      _parameters = StructConverter.Deserialize<Parameters>(parameters);
      UpdateValues();
      return StructConverter.Serialize(_parameters);
    }

    public void UpdateSettings(BsonDocument doc)
    {
    }
  }
}
