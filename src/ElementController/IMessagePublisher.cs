using System.Threading.Tasks;

namespace Brewery.ElementController
{
  public interface IMessagePublisher
  {
    Task PublishMessageAsync(string topic, Google.Protobuf.IMessage msg); 
  }
}
