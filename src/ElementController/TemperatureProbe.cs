﻿using System;
using Brewery.Protobuf;

namespace Brewery.ElementController
{
  public class TemperatureProbe
  {
    public TemperatureProbe(string name)
    {
      Name = name;
    }
    
    public void Apply(Protobuf.TemperatureUpdatedEvent msg)
    {
      Temperature = msg.Temperature;
    }

    public string Name { get; private set; }
    public double Temperature { get; private set; }
  }
}
