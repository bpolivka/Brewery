using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Brewery.Protobuf;
using MongoDB.Bson;

namespace Brewery.ElementController
{
  public class ControllerManager
  {
    readonly ConcurrentDictionary<ObjectId, ControllerStrategyDocument> _strategyDocById = 
      new ConcurrentDictionary<ObjectId, ControllerStrategyDocument>();

    public ControllerManager(Brewery brewery)
    {
      HLT = new Controller(brewery, "hlt", Protobuf.Controller.Hlt, brewery.Outputs.HLT, brewery.Outputs.HLTSSR);
      Boil = new Controller(brewery, "boil", Protobuf.Controller.Boil, brewery.Outputs.Boil, brewery.Outputs.BoilSSR);

      HLT.IsActiveChanged += OnIsActiveChanged;
      Boil.IsActiveChanged += OnIsActiveChanged;
    }

    private void OnIsActiveChanged(Controller controller)
    {
      ControllerIsActiveChanged?.Invoke(controller);
    }

    public Controller HLT { get; private set; }
    public Controller Boil { get; private set; }

    public event Action<Controller> ControllerIsActiveChanged;

    public IEnumerable<Controller> GetActiveControllers()
    {
      if (HLT.IsActive) yield return HLT;
      if (Boil.IsActive) yield return Boil;
    }

    public Controller GetController(string name)
    {
      switch (name)
      {
        case "hlt": return HLT;
        case "boil": return Boil;
        default: return null;
      }
    }

    Controller GetController(Protobuf.Controller id)
    {
      switch (id)
      {
        case Protobuf.Controller.Hlt: return HLT;
        case Protobuf.Controller.Boil: return Boil;
        default: return null;
      }
    }

    public void Apply(UpdateControllerStategyCommand cmd)
    {
      var controller = GetController(cmd.Controller);
      if (controller == null) return;
      controller.Apply(cmd);
    }

    void RemoveStrategy(ControllerStrategyDocument doc)
    {
      var controller = GetController(doc.Controller);
      if (controller == null) return;
      controller.RemoveStrategy(doc);      
    }

    public void AddOrUpdateStrategy(ControllerStrategyDocument doc)
    {
      if (_strategyDocById.TryGetValue(doc.Id, out var existing))
      {
        if (existing.Controller != doc.Controller)
        {
          RemoveStrategy(existing);
        }
      }

      _strategyDocById[doc.Id] = doc;
      var controller = GetController(doc.Controller);
      if (controller == null) return;
      controller.AddOrUpdateStrategy(doc);
    }

    internal void RemoveStrategy(ObjectId docId)
    {
      if (_strategyDocById.TryGetValue(docId, out var existing))
      {
        RemoveStrategy(existing);
      }
    }
  }
}
