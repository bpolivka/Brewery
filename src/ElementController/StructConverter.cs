using Google.Protobuf.WellKnownTypes;

namespace Brewery.ElementController
{
  static class StructConverter
  {
    public static T Deserialize<T>(Struct value)
    {
      var json = Google.Protobuf.JsonFormatter.Default.Format(value);
      var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
      return obj;
    }

    public static Struct Serialize(object value)
    {
      var json = Newtonsoft.Json.JsonConvert.SerializeObject(value);
      var obj = Struct.Parser.ParseJson(json);
      return obj;
    }
  }
}
