using MongoDB.Bson.Serialization.Attributes;

namespace Brewery.ElementController
{
  public class ControllerStrategyDocument
  {
    public MongoDB.Bson.ObjectId Id { get; set; }

    [BsonElement("controller")]
    public string Controller { get; set; }
    
    [BsonElement("name")]
    public string Name { get; set; }
    
    [BsonElement("type")]
    public string Type { get; set; }
    
    [BsonElement("settings")]
    public MongoDB.Bson.BsonDocument Settings { get; set; }
  }
}
