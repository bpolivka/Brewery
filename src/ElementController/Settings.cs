﻿using System;

namespace Brewery.ElementController
{
  class MqttSettings
  {
    public string ClientId { get; set; }
    public string Server { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
  }

  class MongoDBSettings
  {
    public string ConnectionString { get; set; }
  }

  class SchedulerSettings
  {
    public double PeriodSeconds { get; set; }
  }
}
