﻿using Brewery.Protobuf;
using Google.Protobuf.WellKnownTypes;
using MongoDB.Bson;

namespace Brewery.ElementController
{
  public interface IControllerStrategyImpl
  {
    string Type { get; }

    ControllerUpdatedEvent Calculate(double maxDutyCycle);

    Struct UpdateParameters(Struct parameters);

    void UpdateSettings(BsonDocument doc);
  }
}
