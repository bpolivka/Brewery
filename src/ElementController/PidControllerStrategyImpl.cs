﻿using System;
using Brewery.Protobuf;
using Google.Protobuf.WellKnownTypes;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace Brewery.ElementController
{
  class PidControllerStrategyImpl : IControllerStrategyImpl
  {
    class Parameters
    {
      public double Setpoint { get; set; }
    }

    class Settings
    {
      [BsonElement("probe")]
      public string TemperatureProbe { get; set; }
      
      [BsonElement("kp")]
      public double Kp { get; set; }
      
      [BsonElement("ki")]
      public double Ki { get; set; }
      
      [BsonElement("kd")]
      public double Kd { get; set; }

      [BsonElement("proportionalOnMeasurement")]
      public bool ProportionalOnMeasurement { get; set; }
    }

    private Brewery _brewery;
    Parameters _parameters = new Parameters();
    PidController _pid = new PidController();
    TemperatureProbe _tempProbe;

    public string Type => "pid";

    public PidControllerStrategyImpl(Brewery brewery)
    {
      _brewery = brewery;
    }

    public ControllerUpdatedEvent Calculate(double maxDutyCycle)
    {
      var dutyCycle = _pid.Calculate(_tempProbe.Temperature);
      dutyCycle = Math.Min(dutyCycle, maxDutyCycle);

      return new ControllerUpdatedEvent
      {
        DutyCycle = dutyCycle,
        Values = BuildValues()
      };
    }

    private Struct BuildValues()
    {
      var values = new Struct();
      values.Fields.Add("Setpoint", Value.ForNumber(_parameters.Setpoint));
      values.Fields.Add("Kp", Value.ForNumber(_pid.Kp));
      values.Fields.Add("Ki", Value.ForNumber(_pid.Ki));
      values.Fields.Add("Kd", Value.ForNumber(_pid.Kd));
      values.Fields.Add("ProportionalOnMeasurement", Value.ForBool(_pid.ProportionalOnMeasurement));
      values.Fields.Add("Proportional", Value.ForNumber(_pid.Proportional));
      values.Fields.Add("Integral", Value.ForNumber(_pid.Integral));
      values.Fields.Add("Derivative", Value.ForNumber(_pid.Derivative));
      return values;
    }

    void UpdateParameters(Parameters parameters)
    {
      _pid.Setpoint = parameters.Setpoint;
    }

    void UpdateSettings(Settings settings)
    {
      _pid.Kp = settings.Kp;
      _pid.Ki = settings.Ki;
      _pid.Kd = settings.Kd;
      _pid.ProportionalOnMeasurement = settings.ProportionalOnMeasurement;
      _tempProbe = _brewery.TemperatureProbes.GetByName(settings.TemperatureProbe);
    }

    public Struct UpdateParameters(Struct parameters)
    {
      UpdateParameters(StructConverter.Deserialize<Parameters>(parameters));
      return StructConverter.Serialize(_parameters);
    }

    public void UpdateSettings(BsonDocument doc)
    {
      var settings = BsonSerializer.Deserialize<Settings>(doc);
      UpdateSettings(settings);
    }
  }
}
