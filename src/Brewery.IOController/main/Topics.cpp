#include "Topics.hpp"

namespace Brewery {

namespace Topics {

std::string ChangeOutputCommand("brewery/ChangeOutputCommand");
std::string OutputChangedEvent("brewery/OutputChangedEvent");
std::string SendBreweryStateCommand("brewery/SendBreweryStateCommand");
std::string BreweryStateEvent("brewery/BreweryStateEvent");

} // namespace Topics

} // namespace Brewery
