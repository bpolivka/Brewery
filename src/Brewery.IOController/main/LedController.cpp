#include "LedController.hpp"
#include "NetworkController.hpp"
#include "MqttController.hpp"

#include <esp_log.h>

static const char* TAG = "LedController";

namespace Brewery {

TimerHandle_t LedController::_timer;
const std::vector<LedController::BlinkEntry>* LedController::_blinkVector;
int LedController::_blinkVectorIdx;
SemaphoreHandle_t LedController::_blinkVectorMutex;

std::vector<LedController::BlinkEntry> LedController::_networkDisconnectedBlinkVector = {
      { .level = 1, .delayMs = 300 },
      { .level = 0, .delayMs = 200 },
      { .level = 1, .delayMs = 300 },
      { .level = 0, .delayMs = 200 },
      { .level = 1, .delayMs = 300 },
      { .level = 0, .delayMs = 600 }
};

std::vector<LedController::BlinkEntry> LedController::_mqttDisconnectedBlinkVector = {
      { .level = 1, .delayMs = 300 },
      { .level = 0, .delayMs = 200 },
      { .level = 1, .delayMs = 300 },
      { .level = 0, .delayMs = 600 }
};

std::vector<LedController::BlinkEntry> LedController::_connectedBlinkVector = {
      { .level = 1, .delayMs = 1000 }
};

void LedController::initialize()
{
   _blinkVectorMutex = xSemaphoreCreateMutex();
   updateBlinkVector();

   gpio_config_t io_conf;
   io_conf.pin_bit_mask = 1 << Led_Pin;
   io_conf.mode = GPIO_MODE_OUTPUT;
   io_conf.intr_type = GPIO_INTR_DISABLE;
   io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
   io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
   gpio_config(&io_conf);

   NetworkController::statusChanged.attach([](NetworkController::Status _) { updateBlinkVector(); });
   MqttController::statusChanged.attach([](MqttController::Status _) { updateBlinkVector(); });

   _timer = xTimerCreate("LedTimer", 1, true, nullptr, onTimerExpired);
   ESP_LOGI(TAG, "Starting timer");
   xTimerStart(_timer, 0);
}

void LedController::updateBlinkVector()
{
   if (NetworkController::getStatus() == NetworkController::Status::Disconnected)
   {
      setBlinkVector(&_networkDisconnectedBlinkVector);
   }
   else if (MqttController::getStatus() == MqttController::Status::Disconnected)
   {
      setBlinkVector(&_mqttDisconnectedBlinkVector);
   }
   else
   {
      setBlinkVector(&_connectedBlinkVector);
   }
}

void LedController::setBlinkVector(const std::vector<BlinkEntry>* vector)
{
   xSemaphoreTake(_blinkVectorMutex, portMAX_DELAY);
   _blinkVector = vector;
   _blinkVectorIdx = 0;
   xSemaphoreGive(_blinkVectorMutex);
}

void LedController::onTimerExpired(TimerHandle_t timer)
{
   xSemaphoreTake(_blinkVectorMutex, portMAX_DELAY);
   gpio_set_level(Led_Pin, (*_blinkVector)[_blinkVectorIdx].level ? 1 : 0);
   xTimerChangePeriod(_timer, pdMS_TO_TICKS((*_blinkVector)[_blinkVectorIdx].delayMs), 0);
   _blinkVectorIdx = (_blinkVectorIdx + 1) % _blinkVector->size();
   xSemaphoreGive(_blinkVectorMutex);
}

} // namespace Brewery
