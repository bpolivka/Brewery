#ifndef Brewery__Signal_hpp
#define Brewery__Signal_hpp

#include <functional>
#include <vector>

namespace Brewery {

template <class... Args>
class Signal
{
public:

   void attach(const std::function<void (Args...)>& func)
   {
      _funcs.push_back(func);
   }

   void raise(Args... args)
   {
      for (const auto& f : _funcs)
      {
         f(std::forward<Args>(args)...);
      }
   }

private:

   std::vector<std::function<void (Args...)>> _funcs;
};

} // namespace Brewery


#endif // Brewery__Signal_hpp
