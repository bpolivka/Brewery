#include <cstring>
#include <experimental/string_view>

#include "esp_log.h"

#include "BreweryController.hpp"
#include "Topics.hpp"

#include <freertos/timers.h>

static const char* TAG = "BreweryController";

namespace Brewery
{

BreweryController::BreweryController() :
   _hltButton("hlt", GPIO_NUM_36),
   _boilButton("boil", GPIO_NUM_39),
   _pump1Button("pump1", GPIO_NUM_34),
   _pump2Button("pump2", GPIO_NUM_35),
   _stirButton("stir", GPIO_NUM_32),
   _fanButton("fan", GPIO_NUM_33),
   _hltOut("hlt", BREWERY__OUTPUT__OUTPUT_HLT, GPIO_NUM_2, true),
   _boilOut("boil", BREWERY__OUTPUT__OUTPUT_BOIL, GPIO_NUM_25, true),
   _pump1Out("pump1", BREWERY__OUTPUT__OUTPUT_PUMP1, GPIO_NUM_27, false),
   _pump2Out("pump2", BREWERY__OUTPUT__OUTPUT_PUMP2, GPIO_NUM_13, true),
   _stirOut("stir", BREWERY__OUTPUT__OUTPUT_STIR, GPIO_NUM_26, false),
   _fanOut("fan", BREWERY__OUTPUT__OUTPUT_FAN, GPIO_NUM_15, false),
   _buzzOut("buzz", BREWERY__OUTPUT__OUTPUT_BUZZ, GPIO_NUM_12, true),
   _tbdOut("tbd", BREWERY__OUTPUT__OUTPUT_TBD, GPIO_NUM_14, false),
   _hltSsrOut("hlt-ssr", BREWERY__OUTPUT__OUTPUT_HLT_SSR, GPIO_NUM_16, true),
   _boilSsrOut("boil-ssr", BREWERY__OUTPUT__OUTPUT_BOIL_SSR, GPIO_NUM_4, true)
{
   _allOutputs.push_back(&_hltOut);
   _allOutputs.push_back(&_boilOut);
   _allOutputs.push_back(&_pump1Out);
   _allOutputs.push_back(&_pump2Out);
   _allOutputs.push_back(&_stirOut);
   _allOutputs.push_back(&_fanOut);
   _allOutputs.push_back(&_buzzOut);
   _allOutputs.push_back(&_tbdOut);
   _allOutputs.push_back(&_hltSsrOut);
   _allOutputs.push_back(&_boilSsrOut);

   for (auto output : _allOutputs)
   {
      _outputById[output->getId()] = output;
      output->stateChanged.attach([&](Output& out) { sendOutputUpdate(out); });
   }

   _hltButton.stateChanged.attach([&](Input& _) { _hltOut.setState(!_hltOut.getState()); });
   _boilButton.stateChanged.attach([&](Input& _) { _boilOut.setState(!_boilOut.getState()); });
   _pump1Button.stateChanged.attach([&](Input& _) { _pump1Out.setState(!_pump1Out.getState()); });
   _pump2Button.stateChanged.attach([&](Input& _) { _pump2Out.setState(!_pump2Out.getState()); });
   _stirButton.stateChanged.attach([&](Input& _) { _stirOut.setState(!_stirOut.getState()); });
   _fanButton.stateChanged.attach([&](Input& _) { _fanOut.setState(!_fanOut.getState()); });

   _messageHandlerMap = {
         { "brewery/output/set-state", &BreweryController::handleOutputSetStateMessage },
         { "brewery/output/enable-for-time", &BreweryController::handleOutputEnableForTimeMessage },
         { "brewery/system/get-state", &BreweryController::handleSystemGetStateMessage }
   };

   MqttController::statusChanged.attach([&](MqttController::Status status) { onMqttStatusChanged(status); });
   MqttController::messageReceived.attach([&](const esp_mqtt_event_t& msg) { onMessageReceived(msg); });
}

void BreweryController::onMqttStatusChanged(MqttController::Status status)
{
   if (status == MqttController::Status::Connected)
   {
      MqttController::subscribe("brewery/output/set-state");
      MqttController::subscribe("brewery/output/enable-for-time");
      MqttController::subscribe("brewery/system/get-state");
   }
}

Output* BreweryController::getOutput(const char* name) const
{
   for (auto output : _allOutputs)
   {
      if (strcmp(output->getName(), name) == 0)
         return output;
   }

   return nullptr;
}

void BreweryController::sendOutputUpdate(const Output& output)
{
   Brewery__OutputStateChangedEvent evt = BREWERY__OUTPUT_STATE_CHANGED_EVENT__INIT;
   output.populate(evt);

   char serializationBuffer[32];
   size_t msgSize = brewery__output_state_changed_event__pack(&evt, reinterpret_cast<uint8_t*>(serializationBuffer));
   MqttController::publish("brewery/output/state-changed", serializationBuffer, msgSize);
}

void BreweryController::onMessageReceived(const esp_mqtt_event_t& msg)
{
   ESP_LOGD(TAG, "handleMessage: %s", msg.topic);

   std::experimental::string_view topic(msg.topic);

   auto i = _messageHandlerMap.find(topic);
   if (i == _messageHandlerMap.end()) return;

   auto handler = i->second;
   (this->*(handler))(msg);
}

struct OutputEnabledTimerClosure
{
   Output* output;
   std::string responseTopic;
};

void BreweryController::handleOutputSetStateMessage(const esp_mqtt_event_t& msg)
{
   auto cmd = brewery__set_output_state_command__unpack(nullptr, msg.data_len, reinterpret_cast<uint8_t*>(msg.data));
   handle(*cmd);
   brewery__set_output_state_command__free_unpacked(cmd, nullptr);
}

void BreweryController::handle(const Brewery__SetOutputStateCommand& cmd)
{
   _outputById[cmd.output]->setState(cmd.enabled);
}

void BreweryController::handleOutputEnableForTimeMessage(const esp_mqtt_event_t& msg)
{
   auto cmd = brewery__enable_output_for_time_command__unpack(nullptr, msg.data_len, reinterpret_cast<uint8_t*>(msg.data));
   handle(*cmd);
   brewery__enable_output_for_time_command__free_unpacked(cmd, nullptr);
}

void BreweryController::handle(const Brewery__EnableOutputForTimeCommand& cmd)
{
   if (cmd.milliseconds == 0) return;

   auto output = _outputById[cmd.output];

   auto closure = new OutputEnabledTimerClosure{
      .output = output,
      .responseTopic = cmd.response_topic
   };

   output->setState(true);

   auto timerHandle = xTimerCreate("set-enabled-for-timer", cmd.milliseconds / portTICK_PERIOD_MS, pdFALSE, closure, [](TimerHandle_t t)
   {
      auto closure = static_cast<OutputEnabledTimerClosure*>(pvTimerGetTimerID(t));
      closure->output->setState(false);
      char buf = 0x00;
      MqttController::publish(closure->responseTopic.c_str(), &buf, 1);
      delete closure;
      xTimerDelete(t, portMAX_DELAY);
   });

   xTimerStart(timerHandle, portMAX_DELAY);
}

void BreweryController::handleSystemGetStateMessage(const esp_mqtt_event_t& msg)
{
   for (Output* output : _allOutputs)
   {
      sendOutputUpdate(*output);
   }
}

} // namespace Brewery
