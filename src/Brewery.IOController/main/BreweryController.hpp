#ifndef __Brewery__BreweryController_hpp__
#define __Brewery__BreweryController_hpp__

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

#include <vector>
#include <map>
#include <string>

#include "Output.hpp"
#include "Input.hpp"
#include "MqttController.hpp"

namespace Brewery {

class BreweryController
{
public:

   BreweryController();

   Output* getOutput(const char* name) const;

private:

   void onMqttStatusChanged(MqttController::Status status);
   void onMessageReceived(const esp_mqtt_event_t& msg);

   void handleOutputSetStateMessage(const esp_mqtt_event_t& msg);
   void handleOutputEnableForTimeMessage(const esp_mqtt_event_t& msg);
   void handleSystemGetStateMessage(const esp_mqtt_event_t& msg);

   void handle(const Brewery__SetOutputStateCommand& cmd);
   void handle(const Brewery__EnableOutputForTimeCommand& cmd);

   void sendOutputUpdate(const Output& output);

private:

   Input _hltButton;
   Input _boilButton;
   Input _pump1Button;
   Input _pump2Button;
   Input _stirButton;
   Input _fanButton;

   Output _hltOut;
   Output _boilOut;
   Output _pump1Out;
   Output _pump2Out;
   Output _stirOut;
   Output _fanOut;
   Output _buzzOut;
   Output _tbdOut;
   Output _hltSsrOut;
   Output _boilSsrOut;

   std::vector<Output*> _allOutputs;
   typedef void (BreweryController::*MessageHandlerFunc)(const esp_mqtt_event_t&);
   std::map<std::string, MessageHandlerFunc, std::less<>> _messageHandlerMap;

   std::map<Brewery__Output, Output*> _outputById;
};

}

#endif
