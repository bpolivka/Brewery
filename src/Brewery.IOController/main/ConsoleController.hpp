#ifndef BREWERY_CONSOLECONTROLLER_HPP
#define BREWERY_CONSOLECONTROLLER_HPP

#include <esp_console.h>

namespace Brewery {

class ConsoleController
{
public:

   static void initialize();

   static void registerCommand(esp_console_cmd_t* cmd);

   static void run();
};

} // namespace Brewery

#endif //BREWERY_CONSOLECONTROLLER_HPP
