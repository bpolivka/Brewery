#include <esp_event_loop.h>
#include <esp_err.h>
#include <esp_wifi.h>
#include <esp_log.h>

#include <string.h>

#include "ServiceRegistry.hpp"
#include "ConfigController.hpp"

#include "NetworkController.hpp"

#define LOG_SCOPE "NetworkController"

namespace Brewery {

Signal<NetworkController::Status> NetworkController::statusChanged;
NetworkController::Status NetworkController::_status = NetworkController::Status::Disconnected;

void NetworkController::initialize()
{
   ESP_LOGI(LOG_SCOPE, "In NetworkController::NetworkController()");

   tcpip_adapter_init();

   ESP_ERROR_CHECK(esp_event_loop_init(eventHandler, nullptr));

   wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();

   ESP_ERROR_CHECK(esp_wifi_init(&cfg));
   ESP_ERROR_CHECK(esp_wifi_set_storage(WIFI_STORAGE_RAM));
   ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

   loadConfig();

   start();
}

void NetworkController::setConfig(const Config& cfg)
{
   ConfigController::setString("wifi.ssid", cfg.ssid.c_str());
   ConfigController::setString("wifi.password", cfg.password.c_str());

   stop();
   updateConfig(cfg);
   start();
}

void NetworkController::start()
{
   ESP_ERROR_CHECK(esp_wifi_start());
}

void NetworkController::stop()
{
   ESP_ERROR_CHECK(esp_wifi_stop());
}

void NetworkController::setStatus(Status status)
{
   _status = status;
   statusChanged.raise(status);
}

void NetworkController::loadConfig()
{
   Config cfg;
   cfg.ssid = ConfigController::getString("wifi.ssid");
   cfg.password = ConfigController::getString("wifi.password");
   updateConfig(cfg);
}

void NetworkController::updateConfig(const Config& cfg)
{
   if (cfg.ssid.empty() || cfg.password.empty())
      return;

   wifi_config_t wifiConfig;
   memset(&wifiConfig, 0x00, sizeof(wifiConfig));
   strcpy((char*)wifiConfig.sta.ssid, cfg.ssid.c_str());
   strcpy((char*)wifiConfig.sta.password, cfg.password.c_str());

   ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig));
}

esp_err_t NetworkController::eventHandler(void* ctx, system_event_t* event)
{
   switch (event->event_id)
   {
   case SYSTEM_EVENT_STA_START:
      esp_wifi_connect();
      break;

   case SYSTEM_EVENT_STA_GOT_IP:
      setStatus(Status::Connected);
      break;

   case SYSTEM_EVENT_STA_DISCONNECTED:
      setStatus(Status::Disconnected);
      esp_wifi_connect();
      break;

   default:
      break;
   }

   return ESP_OK;

}

} /* namespace Brewery */
