#include "Input.hpp"
//#include "BreweryController.hpp"

#include <freertos/timers.h>

#include <esp_log.h>

namespace Brewery
{

static const char* TAG = "Input";

void IRAM_ATTR Input::isrHandlerTrampoline(void* arg)
{
   BaseType_t higherPriorityTaskWoken = pdFALSE;

   xTimerPendFunctionCallFromISR([](void* param1, uint32_t param2) {
      static_cast<Input*>(param1)->isrHandler();
      },
      arg, 0, &higherPriorityTaskWoken);

   if (higherPriorityTaskWoken == pdTRUE)
      portYIELD_FROM_ISR();
}

void Input::isrHandler()
{
   stateChanged.raise(*this);
}

void Input::configureGpio()
{
  gpio_set_direction(_gpio, GPIO_MODE_INPUT);
  gpio_set_intr_type(_gpio, GPIO_INTR_NEGEDGE);
  gpio_isr_handler_add(_gpio, Input::isrHandlerTrampoline, (void*)this);
}

bool Input::getState() const
{
  auto level = gpio_get_level(_gpio);
  return level == 0;
}


} // namespace Brewery
