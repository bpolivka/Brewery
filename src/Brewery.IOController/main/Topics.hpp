#ifndef __Brewery__Topics_hpp__
#define __Brewery__Topics_hpp__

#include <string>

namespace Brewery {

namespace Topics {

extern std::string ChangeOutputCommand;
extern std::string OutputChangedEvent;
extern std::string SendBreweryStateCommand;
extern std::string BreweryStateEvent;

} // namespace Topics

} // namespace Brewery


#endif //__Brewery__Topics_hpp__
