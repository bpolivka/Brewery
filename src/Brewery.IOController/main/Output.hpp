#ifndef __Brewery__Output_hpp__
#define __Brewery__Output_hpp__

#include <freertos/FreeRTOS.h>
#include <messages.pb-c.h>

#include "Signal.hpp"

namespace Brewery
{

class Output
{
public:

   Output(const char* name, Brewery__Output id, gpio_num_t gpio, bool inverted) :
         _name(name), _id(id), _gpio(gpio), _state(false), _inverted(inverted)
   {
      configureGpio();
   }

   Brewery__Output getId() const
   {
      return _id;
   }

   const char* getName() const
   {
      return _name;
   }

   bool getState() const
   {
      return _state;
   }

   void setState(bool state);
   
   void populate(Brewery__OutputStateChangedEvent& evt) const;

   Signal<Output&> stateChanged;

private:

   void configureGpio();

private:

   const char* _name;
   Brewery__Output _id;
   gpio_num_t _gpio;
   bool _state;
   bool _inverted;
};

} // namespace Brewery

#endif
