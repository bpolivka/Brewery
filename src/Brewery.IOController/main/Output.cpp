#include "Output.hpp"

namespace Brewery
{

void Output::configureGpio()
{
   gpio_config_t io_conf;
   io_conf.pin_bit_mask = 1 << _gpio;
   io_conf.mode = GPIO_MODE_OUTPUT;
   io_conf.intr_type = GPIO_INTR_DISABLE;
   io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
   io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
   gpio_set_level(_gpio, _inverted ? 0 : 1);
   gpio_config(&io_conf);
}

void Output::setState(bool state)
{
   if (state == _state)
      return;
   _state = state;

   int level;
   if (_inverted)
      level = state ? 1 : 0;
   else
      level = state ? 0 : 1;

   gpio_set_level(_gpio, level);

   stateChanged.raise(*this);
}

void Output::populate(Brewery__OutputStateChangedEvent& evt) const
{
   evt.output = _id;
   evt.enabled = getState() ? 1 : 0;
}

} // namespace Brewery
