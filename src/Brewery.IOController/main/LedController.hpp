#ifndef BREWERY_LEDCONTROLLER_HPP
#define BREWERY_LEDCONTROLLER_HPP

#include <freertos/FreeRTOS.h>
#include <freertos/timers.h>
#include <freertos/semphr.h>

#include <vector>

namespace Brewery {

class LedController
{
private:

   static const gpio_num_t Led_Pin = GPIO_NUM_23;

   struct BlinkEntry
   {
      bool level;
      int delayMs;
   };

public:

   LedController() = delete;

   static void initialize();

private:

   static void updateBlinkVector();
   static void setBlinkVector(const std::vector<BlinkEntry>* vector);
   static void onTimerExpired(TimerHandle_t timer);

private:

   static TimerHandle_t _timer;
   static const std::vector<BlinkEntry>* _blinkVector;
   static int _blinkVectorIdx;
   static SemaphoreHandle_t _blinkVectorMutex;

   static std::vector<BlinkEntry> _networkDisconnectedBlinkVector;
   static std::vector<BlinkEntry> _mqttDisconnectedBlinkVector;
   static std::vector<BlinkEntry> _connectedBlinkVector;
};

} // namespace Brewery

#endif //BREWERY_LEDCONTROLLER_HPP
