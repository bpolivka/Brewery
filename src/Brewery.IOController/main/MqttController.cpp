
#include <esp_log.h>

#include <string>

#include "ConfigController.hpp"
#include "NetworkController.hpp"

#include "MqttController.hpp"
//#include "BreweryController.hpp"

static const char* TAG = "MqttController";

namespace Brewery {

Signal<MqttController::Status> MqttController::statusChanged;
Signal<const esp_mqtt_event_t&> MqttController::messageReceived;
MqttController::Config MqttController::_mqttConfig;
esp_mqtt_client_config_t MqttController::_cfg;
esp_mqtt_client_handle_t MqttController::_client;
MqttController::Status MqttController::_status = MqttController::Status::Disconnected;

void MqttController::initialize()
{
   _cfg.event_handle = &MqttController::handleEvent;
   _cfg.client_id = "Brewery.IOController";

   loadConfig();

   NetworkController::statusChanged.attach(onNetworkStatusChanged);

   _client = esp_mqtt_client_init(&_cfg);

   start();
}


void MqttController::start()
{
   if (_mqttConfig.uri.empty())
   {
      ESP_LOGI(TAG, "Cannot start: MQTT URI not set");
      return;
   }

   if (NetworkController::getStatus() != NetworkController::Status::Connected)
   {
      ESP_LOGI(TAG, "Cannot start: network not connected");
      return;
   }

   ESP_LOGI(TAG, "connecting to %s ...", _cfg.uri);
   esp_mqtt_client_start(_client);
}

void MqttController::stop()
{
   esp_mqtt_client_stop(_client);
}

void MqttController::setConfig(const Config& cfg)
{
   ConfigController::setString("mqtt.uri", cfg.uri.c_str());
   updateConfig(cfg);
   stop();
   start();
}

void MqttController::loadConfig()
{
   Config cfg;
   cfg.uri = ConfigController::getString("mqtt.uri");
   updateConfig(cfg);
}

void MqttController::updateConfig(const Config& cfg)
{
   _mqttConfig = cfg;
   _cfg.uri = _mqttConfig.uri.c_str();
}

void MqttController::onNetworkStatusChanged(NetworkController::Status status)
{
   switch (status)
   {
      case NetworkController::Status::Connected:
         start();
         break;
      case NetworkController::Status::Disconnected:
         stop();
         break;
   }
}

void MqttController::publish(const char* topic, const char* data, int len)
{
   esp_mqtt_client_publish(_client, topic, data, len, 0, false);
}

void MqttController::subscribe(const char* topic)
{
   esp_mqtt_client_subscribe(_client, topic, 0);
}

void MqttController::setStatus(MqttController::Status value)
{
   _status = value;
   statusChanged.raise(_status);
}

void MqttController::handleMessage(esp_mqtt_event_t* event)
{
   messageReceived.raise(*event);
}

esp_err_t MqttController::handleEvent(esp_mqtt_event_t* event)
{
   switch (event->event_id)
   {
      case MQTT_EVENT_CONNECTED:
         ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
         setStatus(Status::Connected);
         break;

      case MQTT_EVENT_DISCONNECTED:
         ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
         setStatus(Status::Disconnected);
         break;

      case MQTT_EVENT_SUBSCRIBED:
         ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
         break;

      case MQTT_EVENT_UNSUBSCRIBED:
         ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
         break;

      case MQTT_EVENT_PUBLISHED:
         break;

      case MQTT_EVENT_DATA:
         handleMessage(event);
         break;

      case MQTT_EVENT_ERROR:
         ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
         break;

      case MQTT_EVENT_BEFORE_CONNECT:
         ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
         break;
   }
   return ESP_OK;
}

} // namespace Brewery
