#include <stdio.h>
#include <string.h>

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

#include <esp_event_loop.h>
#include <esp_wifi.h>
#include <esp_log.h>
#include <esp_console.h>
#include <esp_vfs_dev.h>
#include <nvs_flash.h>
#include <driver/uart.h>
#include <driver/spi_master.h>

#include <linenoise/linenoise.h>
#include <argtable3/argtable3.h>

#include <vector>

#include "ServiceRegistry.hpp"
#include "ConfigController.hpp"
#include "ConsoleController.hpp"
#include "NetworkController.hpp"
#include "MqttController.hpp"
#include "LedController.hpp"
#include "BreweryController.hpp"
#include "Signal.hpp"

using namespace Brewery;

BreweryController* breweryController;

ServiceRegistry svcRegistry;

static void initializeLogging()
{
   esp_log_level_set("*", ESP_LOG_INFO);
   //esp_log_level_set("TRANSPORT_TCP", ESP_LOG_INFO);
   //esp_log_level_set("MQTT_CLIENT", ESP_LOG_INFO);
   //esp_log_level_set("TRANSPORT", ESP_LOG_INFO);
}

static struct {
   struct arg_str *ssid;
   struct arg_str *password;
   struct arg_end *end;
} set_wifi_args;

static int handle_set_wifi(int argc, char** argv)
{
   int nerrors = arg_parse(argc, argv, (void**) &set_wifi_args);
   if (nerrors != 0)
   {
      arg_print_errors(stderr, set_wifi_args.end, argv[0]);
      return 1;
   }

   NetworkController::Config cfg;
   cfg.ssid = set_wifi_args.ssid->sval[0];
   cfg.password = set_wifi_args.password->sval[0];
   NetworkController::setConfig(cfg);

   return 0;
}

static void registerSetWifiCommand()
{
   set_wifi_args.ssid = arg_str1(nullptr, nullptr, "<ssid>", "SSID of AP");
   set_wifi_args.password = arg_str1(nullptr, nullptr, "<pass>", "PSK of AP");
   set_wifi_args.end = arg_end(2);

   esp_console_cmd_t cmd = {
         .command = "set-wifi",
         .help = "Set WiFi parameters",
         .hint = nullptr,
         .func = &handle_set_wifi,
         .argtable = &set_wifi_args
   };

   ConsoleController::registerCommand(&cmd);
}

static struct {
   struct arg_str* uri;
   struct arg_end *end;
} mqtt_args;

static int handleSetMqtt(int argc, char** argv)
{
   int nerrors = arg_parse(argc, argv, (void**) &mqtt_args);
   if (nerrors != 0)
   {
      arg_print_errors(stderr, mqtt_args.end, argv[0]);
      return 1;
   }

   MqttController::Config cfg;
   cfg.uri = mqtt_args.uri->sval[0];
   MqttController::setConfig(cfg);

   return 0;
}

static void registerSetMqttCommand()
{
   mqtt_args.uri = arg_str1(nullptr, nullptr, "<uri>", "URI");
   mqtt_args.end = arg_end(2);

   esp_console_cmd_t cmd = {
         .command = "set-mqtt",
         .help = "Set MQTT settings",
         .hint = nullptr,
         .func = &handleSetMqtt,
         .argtable = &mqtt_args
   };

   ConsoleController::registerCommand(&cmd);
}

static struct {
   struct arg_str* tag;
   struct arg_str* level;
   struct arg_end *end;
} setLogLevelArgs_args;

static esp_log_level_t stringToLogLevel(const char* name)
{
   if (strcasecmp(name, "none") == 0)
      return ESP_LOG_NONE;
   if (strcasecmp(name, "error") == 0)
      return ESP_LOG_ERROR;
   if (strcasecmp(name, "warn") == 0)
      return ESP_LOG_WARN;
   if (strcasecmp(name, "info") == 0)
      return ESP_LOG_INFO;
   if (strcasecmp(name, "debug") == 0)
      return ESP_LOG_DEBUG;
   if (strcasecmp(name, "verbose") == 0)
      return ESP_LOG_VERBOSE;

   return (esp_log_level_t)-1;
}

static int handleSetLogLevel(int argc, char** argv)
{
   int nerrors = arg_parse(argc, argv, (void**)&setLogLevelArgs_args);
   if (nerrors != 0)
   {
      arg_print_errors(stderr, setLogLevelArgs_args.end, argv[0]);
      return 1;
   }

   const char* tag = setLogLevelArgs_args.tag->sval[0];
   const char* levelName = setLogLevelArgs_args.level->sval[0];
   esp_log_level_t level = stringToLogLevel(levelName);

   if (level != (esp_log_level_t)-1)
   {
      esp_log_level_set(tag, level);
   }

   return 0;
}

static void registerSetLogLevelCommand()
{
   setLogLevelArgs_args.tag = arg_str1(nullptr, nullptr, "<tag>", "Tag");
   setLogLevelArgs_args.level = arg_str1(nullptr, nullptr, "<level>", "Level");
   setLogLevelArgs_args.end = arg_end(2);

   esp_console_cmd_t cmd = {
         .command = "set-log-level",
         .help = "Set log level",
         .hint = nullptr,
         .func = &handleSetLogLevel,
         .argtable = &setLogLevelArgs_args
   };

   ConsoleController::registerCommand(&cmd);
}

extern "C" void app_main()
{
   gpio_install_isr_service(0);

   initializeLogging();

   ConfigController::initialize();
   LedController::initialize();
   NetworkController::initialize();
   MqttController::initialize();
   ConsoleController::initialize();

   registerSetWifiCommand();
   registerSetMqttCommand();
   registerSetLogLevelCommand();

   breweryController = new BreweryController();

   ConsoleController::run();
}
