#ifndef __Brewery__MqttController_hpp__
#define __Brewery__MqttController_hpp__

#include <string>
#include <mqtt_client.h>

#include "NetworkController.hpp"

namespace Brewery {

class MqttController
{
public:

   struct Config
   {
      std::string uri;
   };

   enum class Status
   {
      Disconnected,
      Connected
   };

public:

   MqttController() = delete;

   static void initialize();

   static void setConfig(const Config& cfg);

   static void publish(const char* topic, const char* data, int len);

   static void subscribe(const char* topic);

   static Status getStatus() { return _status; }
   static void setStatus(Status value);

   static Signal<Status> statusChanged;
   static Signal<const esp_mqtt_event_t&> messageReceived;

private:

   static void loadConfig();
   static void updateConfig(const Config& cfg);

   static void start();
   static void stop();

   static esp_err_t handleEvent(esp_mqtt_event_t* event);
   static void handleMessage(esp_mqtt_event_t* event);

   static void onNetworkStatusChanged(NetworkController::Status status);

private:

   static Config _mqttConfig;
   static esp_mqtt_client_config_t _cfg;
   static esp_mqtt_client_handle_t _client;
   static Status _status;
};

#if false
class BreweryController;

class MqttController
{
public:
   MqttController(BreweryController& breweryController);

   void start(const std::string& uri);
   void stop();

   void subscribe(const char* topic);
   void publish(const char* topic, cJSON* root);
   void publish(const char* topic, const char* data, int len);

private:

   static esp_err_t handleEventTrampoline(esp_mqtt_event_handle_t event);
   esp_err_t handleEvent(esp_mqtt_event_handle_t event);
   void handleMessage(esp_mqtt_event_handle_t event);

private:

   std::string _uri;
   BreweryController& _breweryController;
   esp_mqtt_client_config_t _cfg;
   esp_mqtt_client_handle_t _client;
};
#endif


} // namespace Brewery

#endif
