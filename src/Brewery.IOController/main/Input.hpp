#ifndef __Brewery__Input_hpp__
#define __Brewery__Input_hpp__

#include <freertos/FreeRTOS.h>

#include "Signal.hpp"

namespace Brewery
{

class Input
{
  public:
  Input(const char* name, gpio_num_t gpio)
      : _name(name), _gpio(gpio)
  {
    configureGpio();
  }

  const char* getName() const { return _name; }

  bool getState() const;

   Signal<Input&> stateChanged;

private:

  void configureGpio();
  static void IRAM_ATTR isrHandlerTrampoline(void* arg);
  void isrHandler();

private:

  const char* _name;
  gpio_num_t _gpio;
};

} // namespace Brewery

#endif
