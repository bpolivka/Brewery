import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import { RootState } from './state';
import { controllers } from './controllers';

Vue.use(Vuex);

const store: StoreOptions<RootState> = {
  strict: process.env.DEV === 'true',
  modules: {
    controllers
  }
};

export default new Vuex.Store<RootState>(store);
