import { Module, ActionTree, MutationTree } from 'vuex';
import { RootState } from '../state';

export interface ControllerState {
  visible: boolean;
}

export interface ControllersState {
  hlt: ControllerState;
  boil: ControllerState;
}

export const state: ControllersState = {
  hlt: { visible: true },
  boil: { visible: false }
};

export interface UpdateControllerVisibilityRequest {
  controller: ControllerState;
  visibility: boolean;
}

const actions: ActionTree<ControllersState, RootState> = {
  updateControllerVisibility(
    { commit },
    payload: UpdateControllerVisibilityRequest
  ) {
    commit('setControllerVisibility', payload);
  }
};

const mutations: MutationTree<ControllersState> = {
  setControllerVisibility(state, payload: UpdateControllerVisibilityRequest) {
    payload.controller.visible = payload.visibility;
  }
};

export const controllers: Module<ControllersState, RootState> = {
  namespaced: true,
  state: state,
  actions: actions,
  mutations: mutations
};
