import Brewery from './Brewery';
import { brewery as pbBrewery } from '@brewery/protobuf';
import { Signal } from 'typed-signals';

export default class Output {
  enabled: boolean = false;
  readonly outputStateChanged = new Signal<(output: Output) => void>()

  constructor(
    readonly brewery: Brewery,
    readonly name: string,
    readonly id: pbBrewery.Output
  ) {}

  toggle(): void {
    const cmd = new pbBrewery.SetOutputStateCommand({
      state: {
        output: this.id,
        enabled: !this.enabled  
      }
    });

    this.brewery.commandHandler.handleSetOutputStateCommand(cmd);
  }

  setState(state: pbBrewery.IOutputState) {
    this.enabled = state.enabled!;
    this.outputStateChanged.emit(this)
  }
}
