import ControllerStrategy from "./ControllerStrategy";
import { google } from '@brewery/protobuf';

export class SetpointControllerStrategy extends ControllerStrategy {
  readonly type: string = 'Setpoint'

  setpoint: number = 148.0

  populateParameters(params: google.protobuf.Struct): void {
    params.fields['Setpoint'] = { numberValue: this.setpoint };
  }
}
