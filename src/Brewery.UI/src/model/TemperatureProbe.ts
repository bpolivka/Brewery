import Brewery from './Brewery';
import { brewery as pbBrewery } from '@brewery/protobuf';

export default class TemperatureProbe {
  temperature: number = 0.0;

  readonly cssName: string;

  constructor(
    readonly brewery: Brewery,
    readonly name: string,
    readonly id: pbBrewery.TemperatureProbe
  ) {
    this.cssName = name.replace(' ', '-').toLowerCase();
  }

  handleTemperatureUpdatedEvent(msg: pbBrewery.TemperatureUpdatedEvent) {
    this.temperature = msg.temperature;
  }
}
