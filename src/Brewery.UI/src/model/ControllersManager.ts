import { Controller, IControllerSettings } from './Controller';
import Brewery from './Brewery';
import { brewery as pbBrewery } from '@brewery/protobuf';
import Output from './Output';

export default class ControllersManager {
  hlt: Controller;
  boil: Controller;

  private _byId = new Map<pbBrewery.Controller, Controller>();

  constructor(private brewery: Brewery) {
    this.hlt = this.createController({
      brewery: brewery,
      name: 'HLT',
      id: pbBrewery.Controller.CONTROLLER_HLT,
      contactorOutput: brewery.outputs.hlt,
      ssrOutput: brewery.outputs.hltSsr
    })

    this.boil = this.createController({
      brewery: brewery,
      name: 'Boil',
      id: pbBrewery.Controller.CONTROLLER_BOIL,
      contactorOutput: brewery.outputs.boil,
      ssrOutput: brewery.outputs.boilSsr
    })
  }

  private createController(options: IControllerSettings) {
    const controller = new Controller(options);
    this._byId.set(options.id, controller);
    return controller;
  }

  handleControllerUpdatedEvent(msg: pbBrewery.ControllerUpdatedEvent) {
    const controller = this._byId.get(msg.controller);
    if (controller == null) return;
    controller.handleControllerUpdatedEvent(msg);
  }
}
