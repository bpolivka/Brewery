import ControllerStrategy from './ControllerStrategy';
import { google } from '@brewery/protobuf';

export class ManualControllerStrategy extends ControllerStrategy {
  readonly type: string = 'Manual'

  private _targetDutyCycle: number = 0.5

  get targetDutyCycle() { return this._targetDutyCycle }
  
  set targetDutyCycle(value: number) {
    this._targetDutyCycle = value;
    this.raiseSettingsChanged();
  }

  populateParameters(params: google.protobuf.Struct): void {
    params.fields['TargetDutyCycle'] = { numberValue: this._targetDutyCycle }
  }
}
