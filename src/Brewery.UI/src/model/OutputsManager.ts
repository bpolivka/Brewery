import Output from './Output';
import Brewery from './Brewery';
import { brewery as pbBrewery } from '@brewery/protobuf';

export default class OutputsManager {
  hlt: Output;
  boil: Output;
  pump1: Output;
  pump2: Output;
  stir: Output;
  fan: Output;
  buzz: Output;
  tbd: Output;
  hltSsr: Output;
  boilSsr: Output;

  private _byId = new Map<pbBrewery.Output, Output>();

  constructor(private brewery: Brewery) {
    this.hlt = this.createOutput('HLT', pbBrewery.Output.OUTPUT_HLT);
    this.boil = this.createOutput('Boil', pbBrewery.Output.OUTPUT_BOIL);
    this.pump1 = this.createOutput('Pump 1', pbBrewery.Output.OUTPUT_PUMP1);
    this.pump2 = this.createOutput('Pump 2', pbBrewery.Output.OUTPUT_PUMP2);
    this.stir = this.createOutput('Stir', pbBrewery.Output.OUTPUT_STIR);
    this.fan = this.createOutput('Fan', pbBrewery.Output.OUTPUT_FAN);
    this.buzz = this.createOutput('Buzz', pbBrewery.Output.OUTPUT_BUZZ);
    this.tbd = this.createOutput('TBD', pbBrewery.Output.OUTPUT_TBD);
    this.hltSsr = this.createOutput('HLT SSR', pbBrewery.Output.OUTPUT_HLT_SSR);
    this.boilSsr = this.createOutput(
      'Boil SSR',
      pbBrewery.Output.OUTPUT_BOIL_SSR
    );
  }

  private createOutput(name: string, id: pbBrewery.Output): Output {
    const output = new Output(this.brewery, name, id);
    this._byId.set(id, output);
    return output;
  }

  handleOutputStateChangedEvent(msg: pbBrewery.OutputStateChangedEvent) {
    this.setOutputState(msg.state!)
  }

  setOutputState(state: pbBrewery.IOutputState) {
    const output = this._byId.get(state.output!);
    if (output == null) return;
    output.setState(state);
  }
}
