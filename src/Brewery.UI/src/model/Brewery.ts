import OutputsManager from './OutputsManager';
import TemperatureProbesManager from './TemperatureProbesManager';
import { brewery as pbBrewery } from '@brewery/protobuf';
import ControllersManager from './ControllersManager';

export interface ICommandHandler {
  handleSetOutputStateCommand(cmd: pbBrewery.SetOutputStateCommand): void;
  handleUpdateControllerStategyCommand(cmd: pbBrewery.IUpdateControllerStategyCommand): void;
  handleSendSystemStateCommand(cmd: pbBrewery.SendSystemStateCommand): void;
}

export class Brewery {
  private static _instance: Brewery;

  outputs: OutputsManager;
  temperatureProbes: TemperatureProbesManager;
  controllers: ControllersManager;
  commandHandler!: ICommandHandler;

  static getInstance() {
    if (!this._instance) {
      this._instance = new Brewery();
    }
    return this._instance;
  }

  private constructor() {
    this.outputs = new OutputsManager(this);
    this.temperatureProbes = new TemperatureProbesManager(this);
    this.controllers = new ControllersManager(this);
  }

  handleSystemStateEvent (evt: pbBrewery.SystemStateEvent) {
    for (const outputState of evt.outputStates) {
      this.outputs.setOutputState(outputState);
    }
  }
}

export default Brewery;
