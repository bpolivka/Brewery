import TemperatureProbe from './TemperatureProbe';
import Brewery from './Brewery';
import { brewery as pbBrewery } from '@brewery/protobuf';

export default class TemperatureProbesManager {
  hlt: TemperatureProbe;
  mltIn: TemperatureProbe;
  mltOut: TemperatureProbe;
  boil: TemperatureProbe;

  private _byId = new Map<pbBrewery.TemperatureProbe, TemperatureProbe>();

  constructor(private brewery: Brewery) {
    this.hlt = this.createTemperatureProbe(
      'HLT',
      pbBrewery.TemperatureProbe.TEMPERATURE_PROBE_HLT
    );
    this.mltIn = this.createTemperatureProbe(
      'MLT In',
      pbBrewery.TemperatureProbe.TEMPERATURE_PROBE_MLT_IN
    );
    this.mltOut = this.createTemperatureProbe(
      'MLT Out',
      pbBrewery.TemperatureProbe.TEMPERATURE_PROBE_MLT_OUT
    );
    this.boil = this.createTemperatureProbe(
      'Boil',
      pbBrewery.TemperatureProbe.TEMPERATURE_PROBE_BOIL
    );
  }

  private createTemperatureProbe(
    name: string,
    id: pbBrewery.TemperatureProbe
  ): TemperatureProbe {
    const probe = new TemperatureProbe(this.brewery, name, id);
    this._byId.set(id, probe);
    return probe;
  }

  handleTemperatureUpdatedEvent(msg: pbBrewery.TemperatureUpdatedEvent) {
    const probe = this._byId.get(msg.probe);
    if (probe == null) return;
    probe.handleTemperatureUpdatedEvent(msg);
  }
}
