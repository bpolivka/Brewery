import Brewery from './Brewery';
import Output from './Output';
import { Signal, SignalConnection } from 'typed-signals';
import { brewery as pbBrewery, google } from '@brewery/protobuf';
import ControllerStrategy from './ControllerStrategy';
import { ManualControllerStrategy } from './ManualControllerStrategy';
import { SetpointControllerStrategy } from './SetpointControllerStrategy';

export interface IControllerSettings {
  brewery: Brewery;
  name: string;
  id: pbBrewery.Controller;
  contactorOutput: Output;
  ssrOutput: Output;
}

export class Controller {
  readonly enabledChanged = new Signal<(controller: Controller) => void>()

  readonly brewery: Brewery;
  readonly name: string;
  readonly id: pbBrewery.Controller;
  readonly contactorOutput: Output;
  readonly ssrOutput: Output;

  private _dutyCycle = 0.0

  readonly availableStrategies: ControllerStrategy[] = [
    new ManualControllerStrategy('Manual', this),
    new SetpointControllerStrategy('Preheat', this)
  ]
  
  private _settingsChangedConnection?: SignalConnection;

  currentStrategy!: ControllerStrategy;
    
  constructor(settings: IControllerSettings) {
    this.brewery = settings.brewery;
    this.name = settings.name;
    this.id = settings.id;
    this.contactorOutput = settings.contactorOutput;
    this.ssrOutput = settings.ssrOutput;

    this.ssrOutput.outputStateChanged.connect(_ => this.enabledChanged.emit(this));

    this.updateCurrentStrategy(this.availableStrategies[0])
  }

  updateCurrentStrategy(strategy: ControllerStrategy) {
    if (this._settingsChangedConnection) {
      this._settingsChangedConnection.disconnect()
    }
    this._settingsChangedConnection = strategy.settingsChanged.connect(strategy => {
      this.onStrategySettingsChanged(strategy);
    });

    this.currentStrategy = strategy
  }

  onStrategySettingsChanged(strategy: ControllerStrategy) {
    const parameters = new google.protobuf.Struct()
    strategy.populateParameters(parameters)

    const cmd = new pbBrewery.UpdateControllerStategyCommand({
      controller: this.id,
      strategy: strategy.name,
      parameters: parameters
    });

    this.brewery.commandHandler.handleUpdateControllerStategyCommand(cmd)
  }

  get enabled() { return this.ssrOutput.enabled; }

  get dutyCycle() { return this._dutyCycle; }

  handleControllerUpdatedEvent(msg: pbBrewery.ControllerUpdatedEvent) {
    this._dutyCycle = msg.dutyCycle
  }
}
