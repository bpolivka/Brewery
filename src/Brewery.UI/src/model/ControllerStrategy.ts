import { Signal } from 'typed-signals';
import { google } from '@brewery/protobuf';
import { Controller } from './Controller';

export default abstract class ControllerStrategy {
  readonly settingsChanged = new Signal<(strategy: ControllerStrategy) => void>()

  constructor(readonly name: string, readonly controller: Controller) {
  }

  abstract get type(): string;

  abstract populateParameters(params: google.protobuf.Struct): void;

  protected raiseSettingsChanged() {
    this.settingsChanged.emit(this)
  }
}
