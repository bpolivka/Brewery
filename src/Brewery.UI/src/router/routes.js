const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      {
        path: '',
        components: {
          default: () => import('pages/Home.vue'),
          rightDrawer: () => import('pages/HomeSettings.vue')
        },
        meta: {
          rightDrawerButton: {
            icon: 'settings'
          }
        }
      },
      {
        path: 'stuff',
        components: {
          default: () => import('pages/Stuff.vue')
        }
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  });
}

export default routes;
