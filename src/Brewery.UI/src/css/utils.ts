import * as colors from './colors.json';

export function getApplicationColor(name: string): string {
  return (colors as any).default[name];
}
