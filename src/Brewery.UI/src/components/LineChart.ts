import { Line, mixins } from 'vue-chartjs';
const { reactiveProp } = mixins;
import { Component, Prop, Mixins } from 'vue-property-decorator';
import { ChartOptions } from 'chart.js';

@Component
export default class LineChart extends Mixins(Line, reactiveProp) {
  @Prop() options!: ChartOptions;

  mounted() {
    const self = this as any;
    self.renderChart(self.chartData, this.options);
  }
}
