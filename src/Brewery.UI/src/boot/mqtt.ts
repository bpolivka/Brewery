import { connect, MqttClient } from 'mqtt';
import { Platform } from 'quasar';
import { brewery as pbBrewery } from '@brewery/protobuf';
import { Brewery, ICommandHandler } from 'src/model/Brewery';

// const mqttUrl =
//   Platform.is.electron
//     ? "tcp://brewery:brewery@rabbitmq.bolonium.org:1883"
//     : "wss://brewery:brewery@rabbitmq.bolonium.org:15673/ws"

const mqttUrl = Platform.is.electron
  ? 'tcp://guest:guest@localhost:1883'
  : 'ws://guest:guest@localhost:15675/ws';

function handleTemperatureUpdatedEvent(payload: Buffer) {
  const evt = pbBrewery.TemperatureUpdatedEvent.decode(payload);
  Brewery.getInstance().temperatureProbes.handleTemperatureUpdatedEvent(evt);
}

function handleControllerUpdatedEvent(payload: Buffer) {
  const evt = pbBrewery.ControllerUpdatedEvent.decode(payload);
  Brewery.getInstance().controllers.handleControllerUpdatedEvent(evt);
}

function handleOutputStateChangedEvent(payload: Buffer) {
  const evt = pbBrewery.OutputStateChangedEvent.decode(payload);
  Brewery.getInstance().outputs.handleOutputStateChangedEvent(evt);
}

function handleSystemStateEvent(payload: Buffer) {
  const evt = pbBrewery.SystemStateEvent.decode(payload);
  Brewery.getInstance().handleSystemStateEvent(evt);
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

const uniqueId = uuidv4();
const uniqueSystemStateTopic = `brewery/${uniqueId}/system/state`;

const handlerMap: { [key: string]: (payload: Buffer) => void } = {
  'brewery/temp-probe/updated': handleTemperatureUpdatedEvent,
  'brewery/controller/updated': handleControllerUpdatedEvent,
  'brewery/output/state-changed': handleOutputStateChangedEvent,
  'brewery/system/state': handleSystemStateEvent
};

function handleMessage(topic: string, payload: Buffer) {
  //console.log("@@@ handleMessage: %s", topic)
  const handler = handlerMap[topic];
  if (handler !== undefined) {
    handler(payload);
  }
}

class CommandHandler implements ICommandHandler {
  constructor(private mqttClient: MqttClient) {}

  handleSetOutputStateCommand(cmd: pbBrewery.SetOutputStateCommand): void {
    const buffer = pbBrewery.SetOutputStateCommand.encode(cmd).finish();
    this.mqttClient.publish('brewery/output/set-state', new Buffer(buffer));
  }

  handleUpdateControllerStategyCommand(cmd: pbBrewery.IUpdateControllerStategyCommand): void {
    const buffer = pbBrewery.UpdateControllerStategyCommand.encode(cmd).finish();
    this.mqttClient.publish('brewery/controller/update-strategy', new Buffer(buffer));
  }

  handleSendSystemStateCommand(cmd: pbBrewery.SendSystemStateCommand): void {
    cmd.responseTopic = uniqueSystemStateTopic;
    const buffer = pbBrewery.SendSystemStateCommand.encode(cmd).finish();
    this.mqttClient.publish('brewery/system/send-state', new Buffer(buffer));
  }
}

export default () => {
  const client = connect(mqttUrl);

  const commandHandler = new CommandHandler(client);

  Brewery.getInstance().commandHandler = commandHandler;

  handlerMap[uniqueSystemStateTopic] = handleSystemStateEvent;

  Object.keys(handlerMap).forEach(topic => {
    client.subscribe(topic);
  });

  client.on('message', (topic, payload) => {
    handleMessage(topic, payload);
  });

  commandHandler.handleSendSystemStateCommand(pbBrewery.SendSystemStateCommand.create());
};
