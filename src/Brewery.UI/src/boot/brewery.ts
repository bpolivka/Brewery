import Vue from 'vue';
import Brewery from 'src/model/Brewery';

export default () => {
  Vue.prototype.$brewery = Brewery.getInstance();
};
