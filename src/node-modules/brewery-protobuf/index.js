/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.brewery = (function() {

    /**
     * Namespace brewery.
     * @exports brewery
     * @namespace
     */
    var brewery = {};

    /**
     * TemperatureProbe enum.
     * @name brewery.TemperatureProbe
     * @enum {string}
     * @property {number} TEMPERATURE_PROBE_UNKNOWN=0 TEMPERATURE_PROBE_UNKNOWN value
     * @property {number} TEMPERATURE_PROBE_HLT=1 TEMPERATURE_PROBE_HLT value
     * @property {number} TEMPERATURE_PROBE_BOIL=2 TEMPERATURE_PROBE_BOIL value
     * @property {number} TEMPERATURE_PROBE_MLT_IN=3 TEMPERATURE_PROBE_MLT_IN value
     * @property {number} TEMPERATURE_PROBE_MLT_OUT=4 TEMPERATURE_PROBE_MLT_OUT value
     */
    brewery.TemperatureProbe = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "TEMPERATURE_PROBE_UNKNOWN"] = 0;
        values[valuesById[1] = "TEMPERATURE_PROBE_HLT"] = 1;
        values[valuesById[2] = "TEMPERATURE_PROBE_BOIL"] = 2;
        values[valuesById[3] = "TEMPERATURE_PROBE_MLT_IN"] = 3;
        values[valuesById[4] = "TEMPERATURE_PROBE_MLT_OUT"] = 4;
        return values;
    })();

    /**
     * Output enum.
     * @name brewery.Output
     * @enum {string}
     * @property {number} OUTPUT_UNKNOWN=0 OUTPUT_UNKNOWN value
     * @property {number} OUTPUT_HLT=1 OUTPUT_HLT value
     * @property {number} OUTPUT_BOIL=2 OUTPUT_BOIL value
     * @property {number} OUTPUT_PUMP1=3 OUTPUT_PUMP1 value
     * @property {number} OUTPUT_PUMP2=4 OUTPUT_PUMP2 value
     * @property {number} OUTPUT_STIR=5 OUTPUT_STIR value
     * @property {number} OUTPUT_FAN=6 OUTPUT_FAN value
     * @property {number} OUTPUT_BUZZ=7 OUTPUT_BUZZ value
     * @property {number} OUTPUT_TBD=8 OUTPUT_TBD value
     * @property {number} OUTPUT_HLT_SSR=9 OUTPUT_HLT_SSR value
     * @property {number} OUTPUT_BOIL_SSR=10 OUTPUT_BOIL_SSR value
     */
    brewery.Output = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "OUTPUT_UNKNOWN"] = 0;
        values[valuesById[1] = "OUTPUT_HLT"] = 1;
        values[valuesById[2] = "OUTPUT_BOIL"] = 2;
        values[valuesById[3] = "OUTPUT_PUMP1"] = 3;
        values[valuesById[4] = "OUTPUT_PUMP2"] = 4;
        values[valuesById[5] = "OUTPUT_STIR"] = 5;
        values[valuesById[6] = "OUTPUT_FAN"] = 6;
        values[valuesById[7] = "OUTPUT_BUZZ"] = 7;
        values[valuesById[8] = "OUTPUT_TBD"] = 8;
        values[valuesById[9] = "OUTPUT_HLT_SSR"] = 9;
        values[valuesById[10] = "OUTPUT_BOIL_SSR"] = 10;
        return values;
    })();

    /**
     * Controller enum.
     * @name brewery.Controller
     * @enum {string}
     * @property {number} CONTROLLER_UNKNOWN=0 CONTROLLER_UNKNOWN value
     * @property {number} CONTROLLER_HLT=1 CONTROLLER_HLT value
     * @property {number} CONTROLLER_BOIL=2 CONTROLLER_BOIL value
     */
    brewery.Controller = (function() {
        var valuesById = {}, values = Object.create(valuesById);
        values[valuesById[0] = "CONTROLLER_UNKNOWN"] = 0;
        values[valuesById[1] = "CONTROLLER_HLT"] = 1;
        values[valuesById[2] = "CONTROLLER_BOIL"] = 2;
        return values;
    })();

    brewery.TemperatureUpdatedEvent = (function() {

        /**
         * Properties of a TemperatureUpdatedEvent.
         * @memberof brewery
         * @interface ITemperatureUpdatedEvent
         * @property {brewery.TemperatureProbe|null} [probe] TemperatureUpdatedEvent probe
         * @property {number|null} [temperature] TemperatureUpdatedEvent temperature
         * @property {number|null} [unadjustedTemperature] TemperatureUpdatedEvent unadjustedTemperature
         */

        /**
         * Constructs a new TemperatureUpdatedEvent.
         * @memberof brewery
         * @classdesc Represents a TemperatureUpdatedEvent.
         * @implements ITemperatureUpdatedEvent
         * @constructor
         * @param {brewery.ITemperatureUpdatedEvent=} [properties] Properties to set
         */
        function TemperatureUpdatedEvent(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * TemperatureUpdatedEvent probe.
         * @member {brewery.TemperatureProbe} probe
         * @memberof brewery.TemperatureUpdatedEvent
         * @instance
         */
        TemperatureUpdatedEvent.prototype.probe = 0;

        /**
         * TemperatureUpdatedEvent temperature.
         * @member {number} temperature
         * @memberof brewery.TemperatureUpdatedEvent
         * @instance
         */
        TemperatureUpdatedEvent.prototype.temperature = 0;

        /**
         * TemperatureUpdatedEvent unadjustedTemperature.
         * @member {number} unadjustedTemperature
         * @memberof brewery.TemperatureUpdatedEvent
         * @instance
         */
        TemperatureUpdatedEvent.prototype.unadjustedTemperature = 0;

        /**
         * Creates a new TemperatureUpdatedEvent instance using the specified properties.
         * @function create
         * @memberof brewery.TemperatureUpdatedEvent
         * @static
         * @param {brewery.ITemperatureUpdatedEvent=} [properties] Properties to set
         * @returns {brewery.TemperatureUpdatedEvent} TemperatureUpdatedEvent instance
         */
        TemperatureUpdatedEvent.create = function create(properties) {
            return new TemperatureUpdatedEvent(properties);
        };

        /**
         * Encodes the specified TemperatureUpdatedEvent message. Does not implicitly {@link brewery.TemperatureUpdatedEvent.verify|verify} messages.
         * @function encode
         * @memberof brewery.TemperatureUpdatedEvent
         * @static
         * @param {brewery.ITemperatureUpdatedEvent} message TemperatureUpdatedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        TemperatureUpdatedEvent.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.probe != null && message.hasOwnProperty("probe"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.probe);
            if (message.temperature != null && message.hasOwnProperty("temperature"))
                writer.uint32(/* id 2, wireType 5 =*/21).float(message.temperature);
            if (message.unadjustedTemperature != null && message.hasOwnProperty("unadjustedTemperature"))
                writer.uint32(/* id 3, wireType 5 =*/29).float(message.unadjustedTemperature);
            return writer;
        };

        /**
         * Encodes the specified TemperatureUpdatedEvent message, length delimited. Does not implicitly {@link brewery.TemperatureUpdatedEvent.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.TemperatureUpdatedEvent
         * @static
         * @param {brewery.ITemperatureUpdatedEvent} message TemperatureUpdatedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        TemperatureUpdatedEvent.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a TemperatureUpdatedEvent message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.TemperatureUpdatedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.TemperatureUpdatedEvent} TemperatureUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        TemperatureUpdatedEvent.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.TemperatureUpdatedEvent();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.probe = reader.int32();
                    break;
                case 2:
                    message.temperature = reader.float();
                    break;
                case 3:
                    message.unadjustedTemperature = reader.float();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a TemperatureUpdatedEvent message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.TemperatureUpdatedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.TemperatureUpdatedEvent} TemperatureUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        TemperatureUpdatedEvent.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a TemperatureUpdatedEvent message.
         * @function verify
         * @memberof brewery.TemperatureUpdatedEvent
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        TemperatureUpdatedEvent.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.probe != null && message.hasOwnProperty("probe"))
                switch (message.probe) {
                default:
                    return "probe: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                    break;
                }
            if (message.temperature != null && message.hasOwnProperty("temperature"))
                if (typeof message.temperature !== "number")
                    return "temperature: number expected";
            if (message.unadjustedTemperature != null && message.hasOwnProperty("unadjustedTemperature"))
                if (typeof message.unadjustedTemperature !== "number")
                    return "unadjustedTemperature: number expected";
            return null;
        };

        /**
         * Creates a TemperatureUpdatedEvent message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.TemperatureUpdatedEvent
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.TemperatureUpdatedEvent} TemperatureUpdatedEvent
         */
        TemperatureUpdatedEvent.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.TemperatureUpdatedEvent)
                return object;
            var message = new $root.brewery.TemperatureUpdatedEvent();
            switch (object.probe) {
            case "TEMPERATURE_PROBE_UNKNOWN":
            case 0:
                message.probe = 0;
                break;
            case "TEMPERATURE_PROBE_HLT":
            case 1:
                message.probe = 1;
                break;
            case "TEMPERATURE_PROBE_BOIL":
            case 2:
                message.probe = 2;
                break;
            case "TEMPERATURE_PROBE_MLT_IN":
            case 3:
                message.probe = 3;
                break;
            case "TEMPERATURE_PROBE_MLT_OUT":
            case 4:
                message.probe = 4;
                break;
            }
            if (object.temperature != null)
                message.temperature = Number(object.temperature);
            if (object.unadjustedTemperature != null)
                message.unadjustedTemperature = Number(object.unadjustedTemperature);
            return message;
        };

        /**
         * Creates a plain object from a TemperatureUpdatedEvent message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.TemperatureUpdatedEvent
         * @static
         * @param {brewery.TemperatureUpdatedEvent} message TemperatureUpdatedEvent
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        TemperatureUpdatedEvent.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.probe = options.enums === String ? "TEMPERATURE_PROBE_UNKNOWN" : 0;
                object.temperature = 0;
                object.unadjustedTemperature = 0;
            }
            if (message.probe != null && message.hasOwnProperty("probe"))
                object.probe = options.enums === String ? $root.brewery.TemperatureProbe[message.probe] : message.probe;
            if (message.temperature != null && message.hasOwnProperty("temperature"))
                object.temperature = options.json && !isFinite(message.temperature) ? String(message.temperature) : message.temperature;
            if (message.unadjustedTemperature != null && message.hasOwnProperty("unadjustedTemperature"))
                object.unadjustedTemperature = options.json && !isFinite(message.unadjustedTemperature) ? String(message.unadjustedTemperature) : message.unadjustedTemperature;
            return object;
        };

        /**
         * Converts this TemperatureUpdatedEvent to JSON.
         * @function toJSON
         * @memberof brewery.TemperatureUpdatedEvent
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        TemperatureUpdatedEvent.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return TemperatureUpdatedEvent;
    })();

    brewery.OutputState = (function() {

        /**
         * Properties of an OutputState.
         * @memberof brewery
         * @interface IOutputState
         * @property {brewery.Output|null} [output] OutputState output
         * @property {boolean|null} [enabled] OutputState enabled
         */

        /**
         * Constructs a new OutputState.
         * @memberof brewery
         * @classdesc Represents an OutputState.
         * @implements IOutputState
         * @constructor
         * @param {brewery.IOutputState=} [properties] Properties to set
         */
        function OutputState(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * OutputState output.
         * @member {brewery.Output} output
         * @memberof brewery.OutputState
         * @instance
         */
        OutputState.prototype.output = 0;

        /**
         * OutputState enabled.
         * @member {boolean} enabled
         * @memberof brewery.OutputState
         * @instance
         */
        OutputState.prototype.enabled = false;

        /**
         * Creates a new OutputState instance using the specified properties.
         * @function create
         * @memberof brewery.OutputState
         * @static
         * @param {brewery.IOutputState=} [properties] Properties to set
         * @returns {brewery.OutputState} OutputState instance
         */
        OutputState.create = function create(properties) {
            return new OutputState(properties);
        };

        /**
         * Encodes the specified OutputState message. Does not implicitly {@link brewery.OutputState.verify|verify} messages.
         * @function encode
         * @memberof brewery.OutputState
         * @static
         * @param {brewery.IOutputState} message OutputState message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        OutputState.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.output != null && message.hasOwnProperty("output"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.output);
            if (message.enabled != null && message.hasOwnProperty("enabled"))
                writer.uint32(/* id 2, wireType 0 =*/16).bool(message.enabled);
            return writer;
        };

        /**
         * Encodes the specified OutputState message, length delimited. Does not implicitly {@link brewery.OutputState.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.OutputState
         * @static
         * @param {brewery.IOutputState} message OutputState message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        OutputState.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an OutputState message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.OutputState
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.OutputState} OutputState
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        OutputState.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.OutputState();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.output = reader.int32();
                    break;
                case 2:
                    message.enabled = reader.bool();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an OutputState message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.OutputState
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.OutputState} OutputState
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        OutputState.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an OutputState message.
         * @function verify
         * @memberof brewery.OutputState
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        OutputState.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.output != null && message.hasOwnProperty("output"))
                switch (message.output) {
                default:
                    return "output: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                }
            if (message.enabled != null && message.hasOwnProperty("enabled"))
                if (typeof message.enabled !== "boolean")
                    return "enabled: boolean expected";
            return null;
        };

        /**
         * Creates an OutputState message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.OutputState
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.OutputState} OutputState
         */
        OutputState.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.OutputState)
                return object;
            var message = new $root.brewery.OutputState();
            switch (object.output) {
            case "OUTPUT_UNKNOWN":
            case 0:
                message.output = 0;
                break;
            case "OUTPUT_HLT":
            case 1:
                message.output = 1;
                break;
            case "OUTPUT_BOIL":
            case 2:
                message.output = 2;
                break;
            case "OUTPUT_PUMP1":
            case 3:
                message.output = 3;
                break;
            case "OUTPUT_PUMP2":
            case 4:
                message.output = 4;
                break;
            case "OUTPUT_STIR":
            case 5:
                message.output = 5;
                break;
            case "OUTPUT_FAN":
            case 6:
                message.output = 6;
                break;
            case "OUTPUT_BUZZ":
            case 7:
                message.output = 7;
                break;
            case "OUTPUT_TBD":
            case 8:
                message.output = 8;
                break;
            case "OUTPUT_HLT_SSR":
            case 9:
                message.output = 9;
                break;
            case "OUTPUT_BOIL_SSR":
            case 10:
                message.output = 10;
                break;
            }
            if (object.enabled != null)
                message.enabled = Boolean(object.enabled);
            return message;
        };

        /**
         * Creates a plain object from an OutputState message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.OutputState
         * @static
         * @param {brewery.OutputState} message OutputState
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        OutputState.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.output = options.enums === String ? "OUTPUT_UNKNOWN" : 0;
                object.enabled = false;
            }
            if (message.output != null && message.hasOwnProperty("output"))
                object.output = options.enums === String ? $root.brewery.Output[message.output] : message.output;
            if (message.enabled != null && message.hasOwnProperty("enabled"))
                object.enabled = message.enabled;
            return object;
        };

        /**
         * Converts this OutputState to JSON.
         * @function toJSON
         * @memberof brewery.OutputState
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        OutputState.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return OutputState;
    })();

    brewery.OutputStateChangedEvent = (function() {

        /**
         * Properties of an OutputStateChangedEvent.
         * @memberof brewery
         * @interface IOutputStateChangedEvent
         * @property {brewery.IOutputState|null} [state] OutputStateChangedEvent state
         */

        /**
         * Constructs a new OutputStateChangedEvent.
         * @memberof brewery
         * @classdesc Represents an OutputStateChangedEvent.
         * @implements IOutputStateChangedEvent
         * @constructor
         * @param {brewery.IOutputStateChangedEvent=} [properties] Properties to set
         */
        function OutputStateChangedEvent(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * OutputStateChangedEvent state.
         * @member {brewery.IOutputState|null|undefined} state
         * @memberof brewery.OutputStateChangedEvent
         * @instance
         */
        OutputStateChangedEvent.prototype.state = null;

        /**
         * Creates a new OutputStateChangedEvent instance using the specified properties.
         * @function create
         * @memberof brewery.OutputStateChangedEvent
         * @static
         * @param {brewery.IOutputStateChangedEvent=} [properties] Properties to set
         * @returns {brewery.OutputStateChangedEvent} OutputStateChangedEvent instance
         */
        OutputStateChangedEvent.create = function create(properties) {
            return new OutputStateChangedEvent(properties);
        };

        /**
         * Encodes the specified OutputStateChangedEvent message. Does not implicitly {@link brewery.OutputStateChangedEvent.verify|verify} messages.
         * @function encode
         * @memberof brewery.OutputStateChangedEvent
         * @static
         * @param {brewery.IOutputStateChangedEvent} message OutputStateChangedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        OutputStateChangedEvent.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.state != null && message.hasOwnProperty("state"))
                $root.brewery.OutputState.encode(message.state, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified OutputStateChangedEvent message, length delimited. Does not implicitly {@link brewery.OutputStateChangedEvent.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.OutputStateChangedEvent
         * @static
         * @param {brewery.IOutputStateChangedEvent} message OutputStateChangedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        OutputStateChangedEvent.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an OutputStateChangedEvent message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.OutputStateChangedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.OutputStateChangedEvent} OutputStateChangedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        OutputStateChangedEvent.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.OutputStateChangedEvent();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.state = $root.brewery.OutputState.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an OutputStateChangedEvent message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.OutputStateChangedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.OutputStateChangedEvent} OutputStateChangedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        OutputStateChangedEvent.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an OutputStateChangedEvent message.
         * @function verify
         * @memberof brewery.OutputStateChangedEvent
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        OutputStateChangedEvent.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.state != null && message.hasOwnProperty("state")) {
                var error = $root.brewery.OutputState.verify(message.state);
                if (error)
                    return "state." + error;
            }
            return null;
        };

        /**
         * Creates an OutputStateChangedEvent message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.OutputStateChangedEvent
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.OutputStateChangedEvent} OutputStateChangedEvent
         */
        OutputStateChangedEvent.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.OutputStateChangedEvent)
                return object;
            var message = new $root.brewery.OutputStateChangedEvent();
            if (object.state != null) {
                if (typeof object.state !== "object")
                    throw TypeError(".brewery.OutputStateChangedEvent.state: object expected");
                message.state = $root.brewery.OutputState.fromObject(object.state);
            }
            return message;
        };

        /**
         * Creates a plain object from an OutputStateChangedEvent message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.OutputStateChangedEvent
         * @static
         * @param {brewery.OutputStateChangedEvent} message OutputStateChangedEvent
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        OutputStateChangedEvent.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.state = null;
            if (message.state != null && message.hasOwnProperty("state"))
                object.state = $root.brewery.OutputState.toObject(message.state, options);
            return object;
        };

        /**
         * Converts this OutputStateChangedEvent to JSON.
         * @function toJSON
         * @memberof brewery.OutputStateChangedEvent
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        OutputStateChangedEvent.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return OutputStateChangedEvent;
    })();

    brewery.SetOutputStateCommand = (function() {

        /**
         * Properties of a SetOutputStateCommand.
         * @memberof brewery
         * @interface ISetOutputStateCommand
         * @property {brewery.IOutputState|null} [state] SetOutputStateCommand state
         */

        /**
         * Constructs a new SetOutputStateCommand.
         * @memberof brewery
         * @classdesc Represents a SetOutputStateCommand.
         * @implements ISetOutputStateCommand
         * @constructor
         * @param {brewery.ISetOutputStateCommand=} [properties] Properties to set
         */
        function SetOutputStateCommand(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SetOutputStateCommand state.
         * @member {brewery.IOutputState|null|undefined} state
         * @memberof brewery.SetOutputStateCommand
         * @instance
         */
        SetOutputStateCommand.prototype.state = null;

        /**
         * Creates a new SetOutputStateCommand instance using the specified properties.
         * @function create
         * @memberof brewery.SetOutputStateCommand
         * @static
         * @param {brewery.ISetOutputStateCommand=} [properties] Properties to set
         * @returns {brewery.SetOutputStateCommand} SetOutputStateCommand instance
         */
        SetOutputStateCommand.create = function create(properties) {
            return new SetOutputStateCommand(properties);
        };

        /**
         * Encodes the specified SetOutputStateCommand message. Does not implicitly {@link brewery.SetOutputStateCommand.verify|verify} messages.
         * @function encode
         * @memberof brewery.SetOutputStateCommand
         * @static
         * @param {brewery.ISetOutputStateCommand} message SetOutputStateCommand message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetOutputStateCommand.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.state != null && message.hasOwnProperty("state"))
                $root.brewery.OutputState.encode(message.state, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified SetOutputStateCommand message, length delimited. Does not implicitly {@link brewery.SetOutputStateCommand.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.SetOutputStateCommand
         * @static
         * @param {brewery.ISetOutputStateCommand} message SetOutputStateCommand message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SetOutputStateCommand.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SetOutputStateCommand message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.SetOutputStateCommand
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.SetOutputStateCommand} SetOutputStateCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetOutputStateCommand.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.SetOutputStateCommand();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.state = $root.brewery.OutputState.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SetOutputStateCommand message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.SetOutputStateCommand
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.SetOutputStateCommand} SetOutputStateCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SetOutputStateCommand.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SetOutputStateCommand message.
         * @function verify
         * @memberof brewery.SetOutputStateCommand
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SetOutputStateCommand.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.state != null && message.hasOwnProperty("state")) {
                var error = $root.brewery.OutputState.verify(message.state);
                if (error)
                    return "state." + error;
            }
            return null;
        };

        /**
         * Creates a SetOutputStateCommand message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.SetOutputStateCommand
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.SetOutputStateCommand} SetOutputStateCommand
         */
        SetOutputStateCommand.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.SetOutputStateCommand)
                return object;
            var message = new $root.brewery.SetOutputStateCommand();
            if (object.state != null) {
                if (typeof object.state !== "object")
                    throw TypeError(".brewery.SetOutputStateCommand.state: object expected");
                message.state = $root.brewery.OutputState.fromObject(object.state);
            }
            return message;
        };

        /**
         * Creates a plain object from a SetOutputStateCommand message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.SetOutputStateCommand
         * @static
         * @param {brewery.SetOutputStateCommand} message SetOutputStateCommand
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SetOutputStateCommand.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.state = null;
            if (message.state != null && message.hasOwnProperty("state"))
                object.state = $root.brewery.OutputState.toObject(message.state, options);
            return object;
        };

        /**
         * Converts this SetOutputStateCommand to JSON.
         * @function toJSON
         * @memberof brewery.SetOutputStateCommand
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SetOutputStateCommand.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return SetOutputStateCommand;
    })();

    brewery.EnableOutputForTimeCommand = (function() {

        /**
         * Properties of an EnableOutputForTimeCommand.
         * @memberof brewery
         * @interface IEnableOutputForTimeCommand
         * @property {brewery.Output|null} [output] EnableOutputForTimeCommand output
         * @property {number|null} [milliseconds] EnableOutputForTimeCommand milliseconds
         * @property {string|null} [responseTopic] EnableOutputForTimeCommand responseTopic
         * @property {number|null} [requestId] EnableOutputForTimeCommand requestId
         */

        /**
         * Constructs a new EnableOutputForTimeCommand.
         * @memberof brewery
         * @classdesc Represents an EnableOutputForTimeCommand.
         * @implements IEnableOutputForTimeCommand
         * @constructor
         * @param {brewery.IEnableOutputForTimeCommand=} [properties] Properties to set
         */
        function EnableOutputForTimeCommand(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * EnableOutputForTimeCommand output.
         * @member {brewery.Output} output
         * @memberof brewery.EnableOutputForTimeCommand
         * @instance
         */
        EnableOutputForTimeCommand.prototype.output = 0;

        /**
         * EnableOutputForTimeCommand milliseconds.
         * @member {number} milliseconds
         * @memberof brewery.EnableOutputForTimeCommand
         * @instance
         */
        EnableOutputForTimeCommand.prototype.milliseconds = 0;

        /**
         * EnableOutputForTimeCommand responseTopic.
         * @member {string} responseTopic
         * @memberof brewery.EnableOutputForTimeCommand
         * @instance
         */
        EnableOutputForTimeCommand.prototype.responseTopic = "";

        /**
         * EnableOutputForTimeCommand requestId.
         * @member {number} requestId
         * @memberof brewery.EnableOutputForTimeCommand
         * @instance
         */
        EnableOutputForTimeCommand.prototype.requestId = 0;

        /**
         * Creates a new EnableOutputForTimeCommand instance using the specified properties.
         * @function create
         * @memberof brewery.EnableOutputForTimeCommand
         * @static
         * @param {brewery.IEnableOutputForTimeCommand=} [properties] Properties to set
         * @returns {brewery.EnableOutputForTimeCommand} EnableOutputForTimeCommand instance
         */
        EnableOutputForTimeCommand.create = function create(properties) {
            return new EnableOutputForTimeCommand(properties);
        };

        /**
         * Encodes the specified EnableOutputForTimeCommand message. Does not implicitly {@link brewery.EnableOutputForTimeCommand.verify|verify} messages.
         * @function encode
         * @memberof brewery.EnableOutputForTimeCommand
         * @static
         * @param {brewery.IEnableOutputForTimeCommand} message EnableOutputForTimeCommand message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        EnableOutputForTimeCommand.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.output != null && message.hasOwnProperty("output"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.output);
            if (message.milliseconds != null && message.hasOwnProperty("milliseconds"))
                writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.milliseconds);
            if (message.responseTopic != null && message.hasOwnProperty("responseTopic"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.responseTopic);
            if (message.requestId != null && message.hasOwnProperty("requestId"))
                writer.uint32(/* id 4, wireType 0 =*/32).uint32(message.requestId);
            return writer;
        };

        /**
         * Encodes the specified EnableOutputForTimeCommand message, length delimited. Does not implicitly {@link brewery.EnableOutputForTimeCommand.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.EnableOutputForTimeCommand
         * @static
         * @param {brewery.IEnableOutputForTimeCommand} message EnableOutputForTimeCommand message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        EnableOutputForTimeCommand.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an EnableOutputForTimeCommand message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.EnableOutputForTimeCommand
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.EnableOutputForTimeCommand} EnableOutputForTimeCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        EnableOutputForTimeCommand.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.EnableOutputForTimeCommand();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.output = reader.int32();
                    break;
                case 2:
                    message.milliseconds = reader.uint32();
                    break;
                case 3:
                    message.responseTopic = reader.string();
                    break;
                case 4:
                    message.requestId = reader.uint32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an EnableOutputForTimeCommand message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.EnableOutputForTimeCommand
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.EnableOutputForTimeCommand} EnableOutputForTimeCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        EnableOutputForTimeCommand.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an EnableOutputForTimeCommand message.
         * @function verify
         * @memberof brewery.EnableOutputForTimeCommand
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        EnableOutputForTimeCommand.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.output != null && message.hasOwnProperty("output"))
                switch (message.output) {
                default:
                    return "output: enum value expected";
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                    break;
                }
            if (message.milliseconds != null && message.hasOwnProperty("milliseconds"))
                if (!$util.isInteger(message.milliseconds))
                    return "milliseconds: integer expected";
            if (message.responseTopic != null && message.hasOwnProperty("responseTopic"))
                if (!$util.isString(message.responseTopic))
                    return "responseTopic: string expected";
            if (message.requestId != null && message.hasOwnProperty("requestId"))
                if (!$util.isInteger(message.requestId))
                    return "requestId: integer expected";
            return null;
        };

        /**
         * Creates an EnableOutputForTimeCommand message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.EnableOutputForTimeCommand
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.EnableOutputForTimeCommand} EnableOutputForTimeCommand
         */
        EnableOutputForTimeCommand.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.EnableOutputForTimeCommand)
                return object;
            var message = new $root.brewery.EnableOutputForTimeCommand();
            switch (object.output) {
            case "OUTPUT_UNKNOWN":
            case 0:
                message.output = 0;
                break;
            case "OUTPUT_HLT":
            case 1:
                message.output = 1;
                break;
            case "OUTPUT_BOIL":
            case 2:
                message.output = 2;
                break;
            case "OUTPUT_PUMP1":
            case 3:
                message.output = 3;
                break;
            case "OUTPUT_PUMP2":
            case 4:
                message.output = 4;
                break;
            case "OUTPUT_STIR":
            case 5:
                message.output = 5;
                break;
            case "OUTPUT_FAN":
            case 6:
                message.output = 6;
                break;
            case "OUTPUT_BUZZ":
            case 7:
                message.output = 7;
                break;
            case "OUTPUT_TBD":
            case 8:
                message.output = 8;
                break;
            case "OUTPUT_HLT_SSR":
            case 9:
                message.output = 9;
                break;
            case "OUTPUT_BOIL_SSR":
            case 10:
                message.output = 10;
                break;
            }
            if (object.milliseconds != null)
                message.milliseconds = object.milliseconds >>> 0;
            if (object.responseTopic != null)
                message.responseTopic = String(object.responseTopic);
            if (object.requestId != null)
                message.requestId = object.requestId >>> 0;
            return message;
        };

        /**
         * Creates a plain object from an EnableOutputForTimeCommand message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.EnableOutputForTimeCommand
         * @static
         * @param {brewery.EnableOutputForTimeCommand} message EnableOutputForTimeCommand
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        EnableOutputForTimeCommand.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.output = options.enums === String ? "OUTPUT_UNKNOWN" : 0;
                object.milliseconds = 0;
                object.responseTopic = "";
                object.requestId = 0;
            }
            if (message.output != null && message.hasOwnProperty("output"))
                object.output = options.enums === String ? $root.brewery.Output[message.output] : message.output;
            if (message.milliseconds != null && message.hasOwnProperty("milliseconds"))
                object.milliseconds = message.milliseconds;
            if (message.responseTopic != null && message.hasOwnProperty("responseTopic"))
                object.responseTopic = message.responseTopic;
            if (message.requestId != null && message.hasOwnProperty("requestId"))
                object.requestId = message.requestId;
            return object;
        };

        /**
         * Converts this EnableOutputForTimeCommand to JSON.
         * @function toJSON
         * @memberof brewery.EnableOutputForTimeCommand
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        EnableOutputForTimeCommand.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return EnableOutputForTimeCommand;
    })();

    brewery.RequestCompletedEvent = (function() {

        /**
         * Properties of a RequestCompletedEvent.
         * @memberof brewery
         * @interface IRequestCompletedEvent
         * @property {number|null} [requestId] RequestCompletedEvent requestId
         */

        /**
         * Constructs a new RequestCompletedEvent.
         * @memberof brewery
         * @classdesc Represents a RequestCompletedEvent.
         * @implements IRequestCompletedEvent
         * @constructor
         * @param {brewery.IRequestCompletedEvent=} [properties] Properties to set
         */
        function RequestCompletedEvent(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * RequestCompletedEvent requestId.
         * @member {number} requestId
         * @memberof brewery.RequestCompletedEvent
         * @instance
         */
        RequestCompletedEvent.prototype.requestId = 0;

        /**
         * Creates a new RequestCompletedEvent instance using the specified properties.
         * @function create
         * @memberof brewery.RequestCompletedEvent
         * @static
         * @param {brewery.IRequestCompletedEvent=} [properties] Properties to set
         * @returns {brewery.RequestCompletedEvent} RequestCompletedEvent instance
         */
        RequestCompletedEvent.create = function create(properties) {
            return new RequestCompletedEvent(properties);
        };

        /**
         * Encodes the specified RequestCompletedEvent message. Does not implicitly {@link brewery.RequestCompletedEvent.verify|verify} messages.
         * @function encode
         * @memberof brewery.RequestCompletedEvent
         * @static
         * @param {brewery.IRequestCompletedEvent} message RequestCompletedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RequestCompletedEvent.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.requestId != null && message.hasOwnProperty("requestId"))
                writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.requestId);
            return writer;
        };

        /**
         * Encodes the specified RequestCompletedEvent message, length delimited. Does not implicitly {@link brewery.RequestCompletedEvent.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.RequestCompletedEvent
         * @static
         * @param {brewery.IRequestCompletedEvent} message RequestCompletedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        RequestCompletedEvent.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a RequestCompletedEvent message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.RequestCompletedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.RequestCompletedEvent} RequestCompletedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RequestCompletedEvent.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.RequestCompletedEvent();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.requestId = reader.uint32();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a RequestCompletedEvent message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.RequestCompletedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.RequestCompletedEvent} RequestCompletedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        RequestCompletedEvent.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a RequestCompletedEvent message.
         * @function verify
         * @memberof brewery.RequestCompletedEvent
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        RequestCompletedEvent.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.requestId != null && message.hasOwnProperty("requestId"))
                if (!$util.isInteger(message.requestId))
                    return "requestId: integer expected";
            return null;
        };

        /**
         * Creates a RequestCompletedEvent message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.RequestCompletedEvent
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.RequestCompletedEvent} RequestCompletedEvent
         */
        RequestCompletedEvent.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.RequestCompletedEvent)
                return object;
            var message = new $root.brewery.RequestCompletedEvent();
            if (object.requestId != null)
                message.requestId = object.requestId >>> 0;
            return message;
        };

        /**
         * Creates a plain object from a RequestCompletedEvent message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.RequestCompletedEvent
         * @static
         * @param {brewery.RequestCompletedEvent} message RequestCompletedEvent
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        RequestCompletedEvent.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.requestId = 0;
            if (message.requestId != null && message.hasOwnProperty("requestId"))
                object.requestId = message.requestId;
            return object;
        };

        /**
         * Converts this RequestCompletedEvent to JSON.
         * @function toJSON
         * @memberof brewery.RequestCompletedEvent
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        RequestCompletedEvent.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return RequestCompletedEvent;
    })();

    brewery.ControllerUpdatedEvent = (function() {

        /**
         * Properties of a ControllerUpdatedEvent.
         * @memberof brewery
         * @interface IControllerUpdatedEvent
         * @property {brewery.Controller|null} [controller] ControllerUpdatedEvent controller
         * @property {number|null} [dutyCycle] ControllerUpdatedEvent dutyCycle
         * @property {string|null} [strategy] ControllerUpdatedEvent strategy
         * @property {google.protobuf.IStruct|null} [values] ControllerUpdatedEvent values
         */

        /**
         * Constructs a new ControllerUpdatedEvent.
         * @memberof brewery
         * @classdesc Represents a ControllerUpdatedEvent.
         * @implements IControllerUpdatedEvent
         * @constructor
         * @param {brewery.IControllerUpdatedEvent=} [properties] Properties to set
         */
        function ControllerUpdatedEvent(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * ControllerUpdatedEvent controller.
         * @member {brewery.Controller} controller
         * @memberof brewery.ControllerUpdatedEvent
         * @instance
         */
        ControllerUpdatedEvent.prototype.controller = 0;

        /**
         * ControllerUpdatedEvent dutyCycle.
         * @member {number} dutyCycle
         * @memberof brewery.ControllerUpdatedEvent
         * @instance
         */
        ControllerUpdatedEvent.prototype.dutyCycle = 0;

        /**
         * ControllerUpdatedEvent strategy.
         * @member {string} strategy
         * @memberof brewery.ControllerUpdatedEvent
         * @instance
         */
        ControllerUpdatedEvent.prototype.strategy = "";

        /**
         * ControllerUpdatedEvent values.
         * @member {google.protobuf.IStruct|null|undefined} values
         * @memberof brewery.ControllerUpdatedEvent
         * @instance
         */
        ControllerUpdatedEvent.prototype.values = null;

        /**
         * Creates a new ControllerUpdatedEvent instance using the specified properties.
         * @function create
         * @memberof brewery.ControllerUpdatedEvent
         * @static
         * @param {brewery.IControllerUpdatedEvent=} [properties] Properties to set
         * @returns {brewery.ControllerUpdatedEvent} ControllerUpdatedEvent instance
         */
        ControllerUpdatedEvent.create = function create(properties) {
            return new ControllerUpdatedEvent(properties);
        };

        /**
         * Encodes the specified ControllerUpdatedEvent message. Does not implicitly {@link brewery.ControllerUpdatedEvent.verify|verify} messages.
         * @function encode
         * @memberof brewery.ControllerUpdatedEvent
         * @static
         * @param {brewery.IControllerUpdatedEvent} message ControllerUpdatedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ControllerUpdatedEvent.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.controller != null && message.hasOwnProperty("controller"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.controller);
            if (message.dutyCycle != null && message.hasOwnProperty("dutyCycle"))
                writer.uint32(/* id 2, wireType 1 =*/17).double(message.dutyCycle);
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.strategy);
            if (message.values != null && message.hasOwnProperty("values"))
                $root.google.protobuf.Struct.encode(message.values, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified ControllerUpdatedEvent message, length delimited. Does not implicitly {@link brewery.ControllerUpdatedEvent.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.ControllerUpdatedEvent
         * @static
         * @param {brewery.IControllerUpdatedEvent} message ControllerUpdatedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ControllerUpdatedEvent.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a ControllerUpdatedEvent message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.ControllerUpdatedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.ControllerUpdatedEvent} ControllerUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ControllerUpdatedEvent.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.ControllerUpdatedEvent();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.controller = reader.int32();
                    break;
                case 2:
                    message.dutyCycle = reader.double();
                    break;
                case 3:
                    message.strategy = reader.string();
                    break;
                case 4:
                    message.values = $root.google.protobuf.Struct.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a ControllerUpdatedEvent message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.ControllerUpdatedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.ControllerUpdatedEvent} ControllerUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ControllerUpdatedEvent.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a ControllerUpdatedEvent message.
         * @function verify
         * @memberof brewery.ControllerUpdatedEvent
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        ControllerUpdatedEvent.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.controller != null && message.hasOwnProperty("controller"))
                switch (message.controller) {
                default:
                    return "controller: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.dutyCycle != null && message.hasOwnProperty("dutyCycle"))
                if (typeof message.dutyCycle !== "number")
                    return "dutyCycle: number expected";
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                if (!$util.isString(message.strategy))
                    return "strategy: string expected";
            if (message.values != null && message.hasOwnProperty("values")) {
                var error = $root.google.protobuf.Struct.verify(message.values);
                if (error)
                    return "values." + error;
            }
            return null;
        };

        /**
         * Creates a ControllerUpdatedEvent message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.ControllerUpdatedEvent
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.ControllerUpdatedEvent} ControllerUpdatedEvent
         */
        ControllerUpdatedEvent.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.ControllerUpdatedEvent)
                return object;
            var message = new $root.brewery.ControllerUpdatedEvent();
            switch (object.controller) {
            case "CONTROLLER_UNKNOWN":
            case 0:
                message.controller = 0;
                break;
            case "CONTROLLER_HLT":
            case 1:
                message.controller = 1;
                break;
            case "CONTROLLER_BOIL":
            case 2:
                message.controller = 2;
                break;
            }
            if (object.dutyCycle != null)
                message.dutyCycle = Number(object.dutyCycle);
            if (object.strategy != null)
                message.strategy = String(object.strategy);
            if (object.values != null) {
                if (typeof object.values !== "object")
                    throw TypeError(".brewery.ControllerUpdatedEvent.values: object expected");
                message.values = $root.google.protobuf.Struct.fromObject(object.values);
            }
            return message;
        };

        /**
         * Creates a plain object from a ControllerUpdatedEvent message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.ControllerUpdatedEvent
         * @static
         * @param {brewery.ControllerUpdatedEvent} message ControllerUpdatedEvent
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ControllerUpdatedEvent.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.controller = options.enums === String ? "CONTROLLER_UNKNOWN" : 0;
                object.dutyCycle = 0;
                object.strategy = "";
                object.values = null;
            }
            if (message.controller != null && message.hasOwnProperty("controller"))
                object.controller = options.enums === String ? $root.brewery.Controller[message.controller] : message.controller;
            if (message.dutyCycle != null && message.hasOwnProperty("dutyCycle"))
                object.dutyCycle = options.json && !isFinite(message.dutyCycle) ? String(message.dutyCycle) : message.dutyCycle;
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                object.strategy = message.strategy;
            if (message.values != null && message.hasOwnProperty("values"))
                object.values = $root.google.protobuf.Struct.toObject(message.values, options);
            return object;
        };

        /**
         * Converts this ControllerUpdatedEvent to JSON.
         * @function toJSON
         * @memberof brewery.ControllerUpdatedEvent
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        ControllerUpdatedEvent.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return ControllerUpdatedEvent;
    })();

    brewery.UpdateControllerStategyCommand = (function() {

        /**
         * Properties of an UpdateControllerStategyCommand.
         * @memberof brewery
         * @interface IUpdateControllerStategyCommand
         * @property {brewery.Controller|null} [controller] UpdateControllerStategyCommand controller
         * @property {string|null} [strategy] UpdateControllerStategyCommand strategy
         * @property {google.protobuf.IStruct|null} [parameters] UpdateControllerStategyCommand parameters
         */

        /**
         * Constructs a new UpdateControllerStategyCommand.
         * @memberof brewery
         * @classdesc Represents an UpdateControllerStategyCommand.
         * @implements IUpdateControllerStategyCommand
         * @constructor
         * @param {brewery.IUpdateControllerStategyCommand=} [properties] Properties to set
         */
        function UpdateControllerStategyCommand(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * UpdateControllerStategyCommand controller.
         * @member {brewery.Controller} controller
         * @memberof brewery.UpdateControllerStategyCommand
         * @instance
         */
        UpdateControllerStategyCommand.prototype.controller = 0;

        /**
         * UpdateControllerStategyCommand strategy.
         * @member {string} strategy
         * @memberof brewery.UpdateControllerStategyCommand
         * @instance
         */
        UpdateControllerStategyCommand.prototype.strategy = "";

        /**
         * UpdateControllerStategyCommand parameters.
         * @member {google.protobuf.IStruct|null|undefined} parameters
         * @memberof brewery.UpdateControllerStategyCommand
         * @instance
         */
        UpdateControllerStategyCommand.prototype.parameters = null;

        /**
         * Creates a new UpdateControllerStategyCommand instance using the specified properties.
         * @function create
         * @memberof brewery.UpdateControllerStategyCommand
         * @static
         * @param {brewery.IUpdateControllerStategyCommand=} [properties] Properties to set
         * @returns {brewery.UpdateControllerStategyCommand} UpdateControllerStategyCommand instance
         */
        UpdateControllerStategyCommand.create = function create(properties) {
            return new UpdateControllerStategyCommand(properties);
        };

        /**
         * Encodes the specified UpdateControllerStategyCommand message. Does not implicitly {@link brewery.UpdateControllerStategyCommand.verify|verify} messages.
         * @function encode
         * @memberof brewery.UpdateControllerStategyCommand
         * @static
         * @param {brewery.IUpdateControllerStategyCommand} message UpdateControllerStategyCommand message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        UpdateControllerStategyCommand.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.controller != null && message.hasOwnProperty("controller"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.controller);
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.strategy);
            if (message.parameters != null && message.hasOwnProperty("parameters"))
                $root.google.protobuf.Struct.encode(message.parameters, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified UpdateControllerStategyCommand message, length delimited. Does not implicitly {@link brewery.UpdateControllerStategyCommand.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.UpdateControllerStategyCommand
         * @static
         * @param {brewery.IUpdateControllerStategyCommand} message UpdateControllerStategyCommand message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        UpdateControllerStategyCommand.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an UpdateControllerStategyCommand message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.UpdateControllerStategyCommand
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.UpdateControllerStategyCommand} UpdateControllerStategyCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        UpdateControllerStategyCommand.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.UpdateControllerStategyCommand();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.controller = reader.int32();
                    break;
                case 2:
                    message.strategy = reader.string();
                    break;
                case 3:
                    message.parameters = $root.google.protobuf.Struct.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an UpdateControllerStategyCommand message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.UpdateControllerStategyCommand
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.UpdateControllerStategyCommand} UpdateControllerStategyCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        UpdateControllerStategyCommand.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an UpdateControllerStategyCommand message.
         * @function verify
         * @memberof brewery.UpdateControllerStategyCommand
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        UpdateControllerStategyCommand.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.controller != null && message.hasOwnProperty("controller"))
                switch (message.controller) {
                default:
                    return "controller: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                if (!$util.isString(message.strategy))
                    return "strategy: string expected";
            if (message.parameters != null && message.hasOwnProperty("parameters")) {
                var error = $root.google.protobuf.Struct.verify(message.parameters);
                if (error)
                    return "parameters." + error;
            }
            return null;
        };

        /**
         * Creates an UpdateControllerStategyCommand message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.UpdateControllerStategyCommand
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.UpdateControllerStategyCommand} UpdateControllerStategyCommand
         */
        UpdateControllerStategyCommand.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.UpdateControllerStategyCommand)
                return object;
            var message = new $root.brewery.UpdateControllerStategyCommand();
            switch (object.controller) {
            case "CONTROLLER_UNKNOWN":
            case 0:
                message.controller = 0;
                break;
            case "CONTROLLER_HLT":
            case 1:
                message.controller = 1;
                break;
            case "CONTROLLER_BOIL":
            case 2:
                message.controller = 2;
                break;
            }
            if (object.strategy != null)
                message.strategy = String(object.strategy);
            if (object.parameters != null) {
                if (typeof object.parameters !== "object")
                    throw TypeError(".brewery.UpdateControllerStategyCommand.parameters: object expected");
                message.parameters = $root.google.protobuf.Struct.fromObject(object.parameters);
            }
            return message;
        };

        /**
         * Creates a plain object from an UpdateControllerStategyCommand message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.UpdateControllerStategyCommand
         * @static
         * @param {brewery.UpdateControllerStategyCommand} message UpdateControllerStategyCommand
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        UpdateControllerStategyCommand.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.controller = options.enums === String ? "CONTROLLER_UNKNOWN" : 0;
                object.strategy = "";
                object.parameters = null;
            }
            if (message.controller != null && message.hasOwnProperty("controller"))
                object.controller = options.enums === String ? $root.brewery.Controller[message.controller] : message.controller;
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                object.strategy = message.strategy;
            if (message.parameters != null && message.hasOwnProperty("parameters"))
                object.parameters = $root.google.protobuf.Struct.toObject(message.parameters, options);
            return object;
        };

        /**
         * Converts this UpdateControllerStategyCommand to JSON.
         * @function toJSON
         * @memberof brewery.UpdateControllerStategyCommand
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        UpdateControllerStategyCommand.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return UpdateControllerStategyCommand;
    })();

    brewery.ControllerStrategyUpdatedEvent = (function() {

        /**
         * Properties of a ControllerStrategyUpdatedEvent.
         * @memberof brewery
         * @interface IControllerStrategyUpdatedEvent
         * @property {brewery.Controller|null} [controller] ControllerStrategyUpdatedEvent controller
         * @property {string|null} [strategy] ControllerStrategyUpdatedEvent strategy
         * @property {google.protobuf.IStruct|null} [parameters] ControllerStrategyUpdatedEvent parameters
         */

        /**
         * Constructs a new ControllerStrategyUpdatedEvent.
         * @memberof brewery
         * @classdesc Represents a ControllerStrategyUpdatedEvent.
         * @implements IControllerStrategyUpdatedEvent
         * @constructor
         * @param {brewery.IControllerStrategyUpdatedEvent=} [properties] Properties to set
         */
        function ControllerStrategyUpdatedEvent(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * ControllerStrategyUpdatedEvent controller.
         * @member {brewery.Controller} controller
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @instance
         */
        ControllerStrategyUpdatedEvent.prototype.controller = 0;

        /**
         * ControllerStrategyUpdatedEvent strategy.
         * @member {string} strategy
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @instance
         */
        ControllerStrategyUpdatedEvent.prototype.strategy = "";

        /**
         * ControllerStrategyUpdatedEvent parameters.
         * @member {google.protobuf.IStruct|null|undefined} parameters
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @instance
         */
        ControllerStrategyUpdatedEvent.prototype.parameters = null;

        /**
         * Creates a new ControllerStrategyUpdatedEvent instance using the specified properties.
         * @function create
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @static
         * @param {brewery.IControllerStrategyUpdatedEvent=} [properties] Properties to set
         * @returns {brewery.ControllerStrategyUpdatedEvent} ControllerStrategyUpdatedEvent instance
         */
        ControllerStrategyUpdatedEvent.create = function create(properties) {
            return new ControllerStrategyUpdatedEvent(properties);
        };

        /**
         * Encodes the specified ControllerStrategyUpdatedEvent message. Does not implicitly {@link brewery.ControllerStrategyUpdatedEvent.verify|verify} messages.
         * @function encode
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @static
         * @param {brewery.IControllerStrategyUpdatedEvent} message ControllerStrategyUpdatedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ControllerStrategyUpdatedEvent.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.controller != null && message.hasOwnProperty("controller"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.controller);
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                writer.uint32(/* id 2, wireType 2 =*/18).string(message.strategy);
            if (message.parameters != null && message.hasOwnProperty("parameters"))
                $root.google.protobuf.Struct.encode(message.parameters, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified ControllerStrategyUpdatedEvent message, length delimited. Does not implicitly {@link brewery.ControllerStrategyUpdatedEvent.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @static
         * @param {brewery.IControllerStrategyUpdatedEvent} message ControllerStrategyUpdatedEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        ControllerStrategyUpdatedEvent.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a ControllerStrategyUpdatedEvent message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.ControllerStrategyUpdatedEvent} ControllerStrategyUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ControllerStrategyUpdatedEvent.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.ControllerStrategyUpdatedEvent();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.controller = reader.int32();
                    break;
                case 2:
                    message.strategy = reader.string();
                    break;
                case 3:
                    message.parameters = $root.google.protobuf.Struct.decode(reader, reader.uint32());
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a ControllerStrategyUpdatedEvent message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.ControllerStrategyUpdatedEvent} ControllerStrategyUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        ControllerStrategyUpdatedEvent.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a ControllerStrategyUpdatedEvent message.
         * @function verify
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        ControllerStrategyUpdatedEvent.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.controller != null && message.hasOwnProperty("controller"))
                switch (message.controller) {
                default:
                    return "controller: enum value expected";
                case 0:
                case 1:
                case 2:
                    break;
                }
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                if (!$util.isString(message.strategy))
                    return "strategy: string expected";
            if (message.parameters != null && message.hasOwnProperty("parameters")) {
                var error = $root.google.protobuf.Struct.verify(message.parameters);
                if (error)
                    return "parameters." + error;
            }
            return null;
        };

        /**
         * Creates a ControllerStrategyUpdatedEvent message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.ControllerStrategyUpdatedEvent} ControllerStrategyUpdatedEvent
         */
        ControllerStrategyUpdatedEvent.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.ControllerStrategyUpdatedEvent)
                return object;
            var message = new $root.brewery.ControllerStrategyUpdatedEvent();
            switch (object.controller) {
            case "CONTROLLER_UNKNOWN":
            case 0:
                message.controller = 0;
                break;
            case "CONTROLLER_HLT":
            case 1:
                message.controller = 1;
                break;
            case "CONTROLLER_BOIL":
            case 2:
                message.controller = 2;
                break;
            }
            if (object.strategy != null)
                message.strategy = String(object.strategy);
            if (object.parameters != null) {
                if (typeof object.parameters !== "object")
                    throw TypeError(".brewery.ControllerStrategyUpdatedEvent.parameters: object expected");
                message.parameters = $root.google.protobuf.Struct.fromObject(object.parameters);
            }
            return message;
        };

        /**
         * Creates a plain object from a ControllerStrategyUpdatedEvent message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @static
         * @param {brewery.ControllerStrategyUpdatedEvent} message ControllerStrategyUpdatedEvent
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        ControllerStrategyUpdatedEvent.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.controller = options.enums === String ? "CONTROLLER_UNKNOWN" : 0;
                object.strategy = "";
                object.parameters = null;
            }
            if (message.controller != null && message.hasOwnProperty("controller"))
                object.controller = options.enums === String ? $root.brewery.Controller[message.controller] : message.controller;
            if (message.strategy != null && message.hasOwnProperty("strategy"))
                object.strategy = message.strategy;
            if (message.parameters != null && message.hasOwnProperty("parameters"))
                object.parameters = $root.google.protobuf.Struct.toObject(message.parameters, options);
            return object;
        };

        /**
         * Converts this ControllerStrategyUpdatedEvent to JSON.
         * @function toJSON
         * @memberof brewery.ControllerStrategyUpdatedEvent
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        ControllerStrategyUpdatedEvent.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return ControllerStrategyUpdatedEvent;
    })();

    brewery.SendSystemStateCommand = (function() {

        /**
         * Properties of a SendSystemStateCommand.
         * @memberof brewery
         * @interface ISendSystemStateCommand
         * @property {string|null} [responseTopic] SendSystemStateCommand responseTopic
         */

        /**
         * Constructs a new SendSystemStateCommand.
         * @memberof brewery
         * @classdesc Represents a SendSystemStateCommand.
         * @implements ISendSystemStateCommand
         * @constructor
         * @param {brewery.ISendSystemStateCommand=} [properties] Properties to set
         */
        function SendSystemStateCommand(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SendSystemStateCommand responseTopic.
         * @member {string} responseTopic
         * @memberof brewery.SendSystemStateCommand
         * @instance
         */
        SendSystemStateCommand.prototype.responseTopic = "";

        /**
         * Creates a new SendSystemStateCommand instance using the specified properties.
         * @function create
         * @memberof brewery.SendSystemStateCommand
         * @static
         * @param {brewery.ISendSystemStateCommand=} [properties] Properties to set
         * @returns {brewery.SendSystemStateCommand} SendSystemStateCommand instance
         */
        SendSystemStateCommand.create = function create(properties) {
            return new SendSystemStateCommand(properties);
        };

        /**
         * Encodes the specified SendSystemStateCommand message. Does not implicitly {@link brewery.SendSystemStateCommand.verify|verify} messages.
         * @function encode
         * @memberof brewery.SendSystemStateCommand
         * @static
         * @param {brewery.ISendSystemStateCommand} message SendSystemStateCommand message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SendSystemStateCommand.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.responseTopic != null && message.hasOwnProperty("responseTopic"))
                writer.uint32(/* id 1, wireType 2 =*/10).string(message.responseTopic);
            return writer;
        };

        /**
         * Encodes the specified SendSystemStateCommand message, length delimited. Does not implicitly {@link brewery.SendSystemStateCommand.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.SendSystemStateCommand
         * @static
         * @param {brewery.ISendSystemStateCommand} message SendSystemStateCommand message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SendSystemStateCommand.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SendSystemStateCommand message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.SendSystemStateCommand
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.SendSystemStateCommand} SendSystemStateCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SendSystemStateCommand.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.SendSystemStateCommand();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    message.responseTopic = reader.string();
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SendSystemStateCommand message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.SendSystemStateCommand
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.SendSystemStateCommand} SendSystemStateCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SendSystemStateCommand.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SendSystemStateCommand message.
         * @function verify
         * @memberof brewery.SendSystemStateCommand
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SendSystemStateCommand.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.responseTopic != null && message.hasOwnProperty("responseTopic"))
                if (!$util.isString(message.responseTopic))
                    return "responseTopic: string expected";
            return null;
        };

        /**
         * Creates a SendSystemStateCommand message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.SendSystemStateCommand
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.SendSystemStateCommand} SendSystemStateCommand
         */
        SendSystemStateCommand.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.SendSystemStateCommand)
                return object;
            var message = new $root.brewery.SendSystemStateCommand();
            if (object.responseTopic != null)
                message.responseTopic = String(object.responseTopic);
            return message;
        };

        /**
         * Creates a plain object from a SendSystemStateCommand message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.SendSystemStateCommand
         * @static
         * @param {brewery.SendSystemStateCommand} message SendSystemStateCommand
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SendSystemStateCommand.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults)
                object.responseTopic = "";
            if (message.responseTopic != null && message.hasOwnProperty("responseTopic"))
                object.responseTopic = message.responseTopic;
            return object;
        };

        /**
         * Converts this SendSystemStateCommand to JSON.
         * @function toJSON
         * @memberof brewery.SendSystemStateCommand
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SendSystemStateCommand.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return SendSystemStateCommand;
    })();

    brewery.SystemStateEvent = (function() {

        /**
         * Properties of a SystemStateEvent.
         * @memberof brewery
         * @interface ISystemStateEvent
         * @property {Array.<brewery.IOutputState>|null} [outputStates] SystemStateEvent outputStates
         */

        /**
         * Constructs a new SystemStateEvent.
         * @memberof brewery
         * @classdesc Represents a SystemStateEvent.
         * @implements ISystemStateEvent
         * @constructor
         * @param {brewery.ISystemStateEvent=} [properties] Properties to set
         */
        function SystemStateEvent(properties) {
            this.outputStates = [];
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SystemStateEvent outputStates.
         * @member {Array.<brewery.IOutputState>} outputStates
         * @memberof brewery.SystemStateEvent
         * @instance
         */
        SystemStateEvent.prototype.outputStates = $util.emptyArray;

        /**
         * Creates a new SystemStateEvent instance using the specified properties.
         * @function create
         * @memberof brewery.SystemStateEvent
         * @static
         * @param {brewery.ISystemStateEvent=} [properties] Properties to set
         * @returns {brewery.SystemStateEvent} SystemStateEvent instance
         */
        SystemStateEvent.create = function create(properties) {
            return new SystemStateEvent(properties);
        };

        /**
         * Encodes the specified SystemStateEvent message. Does not implicitly {@link brewery.SystemStateEvent.verify|verify} messages.
         * @function encode
         * @memberof brewery.SystemStateEvent
         * @static
         * @param {brewery.ISystemStateEvent} message SystemStateEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SystemStateEvent.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.outputStates != null && message.outputStates.length)
                for (var i = 0; i < message.outputStates.length; ++i)
                    $root.brewery.OutputState.encode(message.outputStates[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
            return writer;
        };

        /**
         * Encodes the specified SystemStateEvent message, length delimited. Does not implicitly {@link brewery.SystemStateEvent.verify|verify} messages.
         * @function encodeDelimited
         * @memberof brewery.SystemStateEvent
         * @static
         * @param {brewery.ISystemStateEvent} message SystemStateEvent message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SystemStateEvent.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SystemStateEvent message from the specified reader or buffer.
         * @function decode
         * @memberof brewery.SystemStateEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {brewery.SystemStateEvent} SystemStateEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SystemStateEvent.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.brewery.SystemStateEvent();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1:
                    if (!(message.outputStates && message.outputStates.length))
                        message.outputStates = [];
                    message.outputStates.push($root.brewery.OutputState.decode(reader, reader.uint32()));
                    break;
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SystemStateEvent message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof brewery.SystemStateEvent
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {brewery.SystemStateEvent} SystemStateEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SystemStateEvent.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SystemStateEvent message.
         * @function verify
         * @memberof brewery.SystemStateEvent
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SystemStateEvent.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.outputStates != null && message.hasOwnProperty("outputStates")) {
                if (!Array.isArray(message.outputStates))
                    return "outputStates: array expected";
                for (var i = 0; i < message.outputStates.length; ++i) {
                    var error = $root.brewery.OutputState.verify(message.outputStates[i]);
                    if (error)
                        return "outputStates." + error;
                }
            }
            return null;
        };

        /**
         * Creates a SystemStateEvent message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof brewery.SystemStateEvent
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {brewery.SystemStateEvent} SystemStateEvent
         */
        SystemStateEvent.fromObject = function fromObject(object) {
            if (object instanceof $root.brewery.SystemStateEvent)
                return object;
            var message = new $root.brewery.SystemStateEvent();
            if (object.outputStates) {
                if (!Array.isArray(object.outputStates))
                    throw TypeError(".brewery.SystemStateEvent.outputStates: array expected");
                message.outputStates = [];
                for (var i = 0; i < object.outputStates.length; ++i) {
                    if (typeof object.outputStates[i] !== "object")
                        throw TypeError(".brewery.SystemStateEvent.outputStates: object expected");
                    message.outputStates[i] = $root.brewery.OutputState.fromObject(object.outputStates[i]);
                }
            }
            return message;
        };

        /**
         * Creates a plain object from a SystemStateEvent message. Also converts values to other types if specified.
         * @function toObject
         * @memberof brewery.SystemStateEvent
         * @static
         * @param {brewery.SystemStateEvent} message SystemStateEvent
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SystemStateEvent.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.arrays || options.defaults)
                object.outputStates = [];
            if (message.outputStates && message.outputStates.length) {
                object.outputStates = [];
                for (var j = 0; j < message.outputStates.length; ++j)
                    object.outputStates[j] = $root.brewery.OutputState.toObject(message.outputStates[j], options);
            }
            return object;
        };

        /**
         * Converts this SystemStateEvent to JSON.
         * @function toJSON
         * @memberof brewery.SystemStateEvent
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SystemStateEvent.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        return SystemStateEvent;
    })();

    return brewery;
})();

$root.google = (function() {

    /**
     * Namespace google.
     * @exports google
     * @namespace
     */
    var google = {};

    google.protobuf = (function() {

        /**
         * Namespace protobuf.
         * @memberof google
         * @namespace
         */
        var protobuf = {};

        protobuf.DoubleValue = (function() {

            /**
             * Properties of a DoubleValue.
             * @memberof google.protobuf
             * @interface IDoubleValue
             * @property {number|null} [value] DoubleValue value
             */

            /**
             * Constructs a new DoubleValue.
             * @memberof google.protobuf
             * @classdesc Represents a DoubleValue.
             * @implements IDoubleValue
             * @constructor
             * @param {google.protobuf.IDoubleValue=} [properties] Properties to set
             */
            function DoubleValue(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * DoubleValue value.
             * @member {number} value
             * @memberof google.protobuf.DoubleValue
             * @instance
             */
            DoubleValue.prototype.value = 0;

            /**
             * Creates a new DoubleValue instance using the specified properties.
             * @function create
             * @memberof google.protobuf.DoubleValue
             * @static
             * @param {google.protobuf.IDoubleValue=} [properties] Properties to set
             * @returns {google.protobuf.DoubleValue} DoubleValue instance
             */
            DoubleValue.create = function create(properties) {
                return new DoubleValue(properties);
            };

            /**
             * Encodes the specified DoubleValue message. Does not implicitly {@link google.protobuf.DoubleValue.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.DoubleValue
             * @static
             * @param {google.protobuf.IDoubleValue} message DoubleValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            DoubleValue.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 1 =*/9).double(message.value);
                return writer;
            };

            /**
             * Encodes the specified DoubleValue message, length delimited. Does not implicitly {@link google.protobuf.DoubleValue.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.DoubleValue
             * @static
             * @param {google.protobuf.IDoubleValue} message DoubleValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            DoubleValue.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a DoubleValue message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.DoubleValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.DoubleValue} DoubleValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            DoubleValue.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.DoubleValue();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.double();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a DoubleValue message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.DoubleValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.DoubleValue} DoubleValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            DoubleValue.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a DoubleValue message.
             * @function verify
             * @memberof google.protobuf.DoubleValue
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            DoubleValue.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (typeof message.value !== "number")
                        return "value: number expected";
                return null;
            };

            /**
             * Creates a DoubleValue message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.DoubleValue
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.DoubleValue} DoubleValue
             */
            DoubleValue.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.DoubleValue)
                    return object;
                var message = new $root.google.protobuf.DoubleValue();
                if (object.value != null)
                    message.value = Number(object.value);
                return message;
            };

            /**
             * Creates a plain object from a DoubleValue message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.DoubleValue
             * @static
             * @param {google.protobuf.DoubleValue} message DoubleValue
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            DoubleValue.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    object.value = 0;
                if (message.value != null && message.hasOwnProperty("value"))
                    object.value = options.json && !isFinite(message.value) ? String(message.value) : message.value;
                return object;
            };

            /**
             * Converts this DoubleValue to JSON.
             * @function toJSON
             * @memberof google.protobuf.DoubleValue
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            DoubleValue.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return DoubleValue;
        })();

        protobuf.FloatValue = (function() {

            /**
             * Properties of a FloatValue.
             * @memberof google.protobuf
             * @interface IFloatValue
             * @property {number|null} [value] FloatValue value
             */

            /**
             * Constructs a new FloatValue.
             * @memberof google.protobuf
             * @classdesc Represents a FloatValue.
             * @implements IFloatValue
             * @constructor
             * @param {google.protobuf.IFloatValue=} [properties] Properties to set
             */
            function FloatValue(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * FloatValue value.
             * @member {number} value
             * @memberof google.protobuf.FloatValue
             * @instance
             */
            FloatValue.prototype.value = 0;

            /**
             * Creates a new FloatValue instance using the specified properties.
             * @function create
             * @memberof google.protobuf.FloatValue
             * @static
             * @param {google.protobuf.IFloatValue=} [properties] Properties to set
             * @returns {google.protobuf.FloatValue} FloatValue instance
             */
            FloatValue.create = function create(properties) {
                return new FloatValue(properties);
            };

            /**
             * Encodes the specified FloatValue message. Does not implicitly {@link google.protobuf.FloatValue.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.FloatValue
             * @static
             * @param {google.protobuf.IFloatValue} message FloatValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            FloatValue.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 5 =*/13).float(message.value);
                return writer;
            };

            /**
             * Encodes the specified FloatValue message, length delimited. Does not implicitly {@link google.protobuf.FloatValue.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.FloatValue
             * @static
             * @param {google.protobuf.IFloatValue} message FloatValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            FloatValue.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a FloatValue message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.FloatValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.FloatValue} FloatValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            FloatValue.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.FloatValue();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.float();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a FloatValue message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.FloatValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.FloatValue} FloatValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            FloatValue.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a FloatValue message.
             * @function verify
             * @memberof google.protobuf.FloatValue
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            FloatValue.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (typeof message.value !== "number")
                        return "value: number expected";
                return null;
            };

            /**
             * Creates a FloatValue message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.FloatValue
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.FloatValue} FloatValue
             */
            FloatValue.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.FloatValue)
                    return object;
                var message = new $root.google.protobuf.FloatValue();
                if (object.value != null)
                    message.value = Number(object.value);
                return message;
            };

            /**
             * Creates a plain object from a FloatValue message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.FloatValue
             * @static
             * @param {google.protobuf.FloatValue} message FloatValue
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            FloatValue.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    object.value = 0;
                if (message.value != null && message.hasOwnProperty("value"))
                    object.value = options.json && !isFinite(message.value) ? String(message.value) : message.value;
                return object;
            };

            /**
             * Converts this FloatValue to JSON.
             * @function toJSON
             * @memberof google.protobuf.FloatValue
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            FloatValue.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return FloatValue;
        })();

        protobuf.Int64Value = (function() {

            /**
             * Properties of an Int64Value.
             * @memberof google.protobuf
             * @interface IInt64Value
             * @property {number|Long|null} [value] Int64Value value
             */

            /**
             * Constructs a new Int64Value.
             * @memberof google.protobuf
             * @classdesc Represents an Int64Value.
             * @implements IInt64Value
             * @constructor
             * @param {google.protobuf.IInt64Value=} [properties] Properties to set
             */
            function Int64Value(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Int64Value value.
             * @member {number|Long} value
             * @memberof google.protobuf.Int64Value
             * @instance
             */
            Int64Value.prototype.value = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

            /**
             * Creates a new Int64Value instance using the specified properties.
             * @function create
             * @memberof google.protobuf.Int64Value
             * @static
             * @param {google.protobuf.IInt64Value=} [properties] Properties to set
             * @returns {google.protobuf.Int64Value} Int64Value instance
             */
            Int64Value.create = function create(properties) {
                return new Int64Value(properties);
            };

            /**
             * Encodes the specified Int64Value message. Does not implicitly {@link google.protobuf.Int64Value.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.Int64Value
             * @static
             * @param {google.protobuf.IInt64Value} message Int64Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Int64Value.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 0 =*/8).int64(message.value);
                return writer;
            };

            /**
             * Encodes the specified Int64Value message, length delimited. Does not implicitly {@link google.protobuf.Int64Value.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.Int64Value
             * @static
             * @param {google.protobuf.IInt64Value} message Int64Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Int64Value.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes an Int64Value message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.Int64Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.Int64Value} Int64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Int64Value.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Int64Value();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.int64();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes an Int64Value message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.Int64Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.Int64Value} Int64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Int64Value.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies an Int64Value message.
             * @function verify
             * @memberof google.protobuf.Int64Value
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Int64Value.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (!$util.isInteger(message.value) && !(message.value && $util.isInteger(message.value.low) && $util.isInteger(message.value.high)))
                        return "value: integer|Long expected";
                return null;
            };

            /**
             * Creates an Int64Value message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.Int64Value
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.Int64Value} Int64Value
             */
            Int64Value.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.Int64Value)
                    return object;
                var message = new $root.google.protobuf.Int64Value();
                if (object.value != null)
                    if ($util.Long)
                        (message.value = $util.Long.fromValue(object.value)).unsigned = false;
                    else if (typeof object.value === "string")
                        message.value = parseInt(object.value, 10);
                    else if (typeof object.value === "number")
                        message.value = object.value;
                    else if (typeof object.value === "object")
                        message.value = new $util.LongBits(object.value.low >>> 0, object.value.high >>> 0).toNumber();
                return message;
            };

            /**
             * Creates a plain object from an Int64Value message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.Int64Value
             * @static
             * @param {google.protobuf.Int64Value} message Int64Value
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Int64Value.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    if ($util.Long) {
                        var long = new $util.Long(0, 0, false);
                        object.value = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                    } else
                        object.value = options.longs === String ? "0" : 0;
                if (message.value != null && message.hasOwnProperty("value"))
                    if (typeof message.value === "number")
                        object.value = options.longs === String ? String(message.value) : message.value;
                    else
                        object.value = options.longs === String ? $util.Long.prototype.toString.call(message.value) : options.longs === Number ? new $util.LongBits(message.value.low >>> 0, message.value.high >>> 0).toNumber() : message.value;
                return object;
            };

            /**
             * Converts this Int64Value to JSON.
             * @function toJSON
             * @memberof google.protobuf.Int64Value
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Int64Value.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return Int64Value;
        })();

        protobuf.UInt64Value = (function() {

            /**
             * Properties of a UInt64Value.
             * @memberof google.protobuf
             * @interface IUInt64Value
             * @property {number|Long|null} [value] UInt64Value value
             */

            /**
             * Constructs a new UInt64Value.
             * @memberof google.protobuf
             * @classdesc Represents a UInt64Value.
             * @implements IUInt64Value
             * @constructor
             * @param {google.protobuf.IUInt64Value=} [properties] Properties to set
             */
            function UInt64Value(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * UInt64Value value.
             * @member {number|Long} value
             * @memberof google.protobuf.UInt64Value
             * @instance
             */
            UInt64Value.prototype.value = $util.Long ? $util.Long.fromBits(0,0,true) : 0;

            /**
             * Creates a new UInt64Value instance using the specified properties.
             * @function create
             * @memberof google.protobuf.UInt64Value
             * @static
             * @param {google.protobuf.IUInt64Value=} [properties] Properties to set
             * @returns {google.protobuf.UInt64Value} UInt64Value instance
             */
            UInt64Value.create = function create(properties) {
                return new UInt64Value(properties);
            };

            /**
             * Encodes the specified UInt64Value message. Does not implicitly {@link google.protobuf.UInt64Value.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.UInt64Value
             * @static
             * @param {google.protobuf.IUInt64Value} message UInt64Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            UInt64Value.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 0 =*/8).uint64(message.value);
                return writer;
            };

            /**
             * Encodes the specified UInt64Value message, length delimited. Does not implicitly {@link google.protobuf.UInt64Value.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.UInt64Value
             * @static
             * @param {google.protobuf.IUInt64Value} message UInt64Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            UInt64Value.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a UInt64Value message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.UInt64Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.UInt64Value} UInt64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            UInt64Value.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.UInt64Value();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.uint64();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a UInt64Value message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.UInt64Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.UInt64Value} UInt64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            UInt64Value.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a UInt64Value message.
             * @function verify
             * @memberof google.protobuf.UInt64Value
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            UInt64Value.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (!$util.isInteger(message.value) && !(message.value && $util.isInteger(message.value.low) && $util.isInteger(message.value.high)))
                        return "value: integer|Long expected";
                return null;
            };

            /**
             * Creates a UInt64Value message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.UInt64Value
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.UInt64Value} UInt64Value
             */
            UInt64Value.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.UInt64Value)
                    return object;
                var message = new $root.google.protobuf.UInt64Value();
                if (object.value != null)
                    if ($util.Long)
                        (message.value = $util.Long.fromValue(object.value)).unsigned = true;
                    else if (typeof object.value === "string")
                        message.value = parseInt(object.value, 10);
                    else if (typeof object.value === "number")
                        message.value = object.value;
                    else if (typeof object.value === "object")
                        message.value = new $util.LongBits(object.value.low >>> 0, object.value.high >>> 0).toNumber(true);
                return message;
            };

            /**
             * Creates a plain object from a UInt64Value message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.UInt64Value
             * @static
             * @param {google.protobuf.UInt64Value} message UInt64Value
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            UInt64Value.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    if ($util.Long) {
                        var long = new $util.Long(0, 0, true);
                        object.value = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                    } else
                        object.value = options.longs === String ? "0" : 0;
                if (message.value != null && message.hasOwnProperty("value"))
                    if (typeof message.value === "number")
                        object.value = options.longs === String ? String(message.value) : message.value;
                    else
                        object.value = options.longs === String ? $util.Long.prototype.toString.call(message.value) : options.longs === Number ? new $util.LongBits(message.value.low >>> 0, message.value.high >>> 0).toNumber(true) : message.value;
                return object;
            };

            /**
             * Converts this UInt64Value to JSON.
             * @function toJSON
             * @memberof google.protobuf.UInt64Value
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            UInt64Value.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return UInt64Value;
        })();

        protobuf.Int32Value = (function() {

            /**
             * Properties of an Int32Value.
             * @memberof google.protobuf
             * @interface IInt32Value
             * @property {number|null} [value] Int32Value value
             */

            /**
             * Constructs a new Int32Value.
             * @memberof google.protobuf
             * @classdesc Represents an Int32Value.
             * @implements IInt32Value
             * @constructor
             * @param {google.protobuf.IInt32Value=} [properties] Properties to set
             */
            function Int32Value(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Int32Value value.
             * @member {number} value
             * @memberof google.protobuf.Int32Value
             * @instance
             */
            Int32Value.prototype.value = 0;

            /**
             * Creates a new Int32Value instance using the specified properties.
             * @function create
             * @memberof google.protobuf.Int32Value
             * @static
             * @param {google.protobuf.IInt32Value=} [properties] Properties to set
             * @returns {google.protobuf.Int32Value} Int32Value instance
             */
            Int32Value.create = function create(properties) {
                return new Int32Value(properties);
            };

            /**
             * Encodes the specified Int32Value message. Does not implicitly {@link google.protobuf.Int32Value.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.Int32Value
             * @static
             * @param {google.protobuf.IInt32Value} message Int32Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Int32Value.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 0 =*/8).int32(message.value);
                return writer;
            };

            /**
             * Encodes the specified Int32Value message, length delimited. Does not implicitly {@link google.protobuf.Int32Value.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.Int32Value
             * @static
             * @param {google.protobuf.IInt32Value} message Int32Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Int32Value.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes an Int32Value message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.Int32Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.Int32Value} Int32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Int32Value.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Int32Value();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.int32();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes an Int32Value message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.Int32Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.Int32Value} Int32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Int32Value.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies an Int32Value message.
             * @function verify
             * @memberof google.protobuf.Int32Value
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Int32Value.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (!$util.isInteger(message.value))
                        return "value: integer expected";
                return null;
            };

            /**
             * Creates an Int32Value message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.Int32Value
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.Int32Value} Int32Value
             */
            Int32Value.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.Int32Value)
                    return object;
                var message = new $root.google.protobuf.Int32Value();
                if (object.value != null)
                    message.value = object.value | 0;
                return message;
            };

            /**
             * Creates a plain object from an Int32Value message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.Int32Value
             * @static
             * @param {google.protobuf.Int32Value} message Int32Value
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Int32Value.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    object.value = 0;
                if (message.value != null && message.hasOwnProperty("value"))
                    object.value = message.value;
                return object;
            };

            /**
             * Converts this Int32Value to JSON.
             * @function toJSON
             * @memberof google.protobuf.Int32Value
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Int32Value.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return Int32Value;
        })();

        protobuf.UInt32Value = (function() {

            /**
             * Properties of a UInt32Value.
             * @memberof google.protobuf
             * @interface IUInt32Value
             * @property {number|null} [value] UInt32Value value
             */

            /**
             * Constructs a new UInt32Value.
             * @memberof google.protobuf
             * @classdesc Represents a UInt32Value.
             * @implements IUInt32Value
             * @constructor
             * @param {google.protobuf.IUInt32Value=} [properties] Properties to set
             */
            function UInt32Value(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * UInt32Value value.
             * @member {number} value
             * @memberof google.protobuf.UInt32Value
             * @instance
             */
            UInt32Value.prototype.value = 0;

            /**
             * Creates a new UInt32Value instance using the specified properties.
             * @function create
             * @memberof google.protobuf.UInt32Value
             * @static
             * @param {google.protobuf.IUInt32Value=} [properties] Properties to set
             * @returns {google.protobuf.UInt32Value} UInt32Value instance
             */
            UInt32Value.create = function create(properties) {
                return new UInt32Value(properties);
            };

            /**
             * Encodes the specified UInt32Value message. Does not implicitly {@link google.protobuf.UInt32Value.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.UInt32Value
             * @static
             * @param {google.protobuf.IUInt32Value} message UInt32Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            UInt32Value.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.value);
                return writer;
            };

            /**
             * Encodes the specified UInt32Value message, length delimited. Does not implicitly {@link google.protobuf.UInt32Value.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.UInt32Value
             * @static
             * @param {google.protobuf.IUInt32Value} message UInt32Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            UInt32Value.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a UInt32Value message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.UInt32Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.UInt32Value} UInt32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            UInt32Value.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.UInt32Value();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.uint32();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a UInt32Value message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.UInt32Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.UInt32Value} UInt32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            UInt32Value.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a UInt32Value message.
             * @function verify
             * @memberof google.protobuf.UInt32Value
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            UInt32Value.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (!$util.isInteger(message.value))
                        return "value: integer expected";
                return null;
            };

            /**
             * Creates a UInt32Value message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.UInt32Value
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.UInt32Value} UInt32Value
             */
            UInt32Value.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.UInt32Value)
                    return object;
                var message = new $root.google.protobuf.UInt32Value();
                if (object.value != null)
                    message.value = object.value >>> 0;
                return message;
            };

            /**
             * Creates a plain object from a UInt32Value message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.UInt32Value
             * @static
             * @param {google.protobuf.UInt32Value} message UInt32Value
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            UInt32Value.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    object.value = 0;
                if (message.value != null && message.hasOwnProperty("value"))
                    object.value = message.value;
                return object;
            };

            /**
             * Converts this UInt32Value to JSON.
             * @function toJSON
             * @memberof google.protobuf.UInt32Value
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            UInt32Value.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return UInt32Value;
        })();

        protobuf.BoolValue = (function() {

            /**
             * Properties of a BoolValue.
             * @memberof google.protobuf
             * @interface IBoolValue
             * @property {boolean|null} [value] BoolValue value
             */

            /**
             * Constructs a new BoolValue.
             * @memberof google.protobuf
             * @classdesc Represents a BoolValue.
             * @implements IBoolValue
             * @constructor
             * @param {google.protobuf.IBoolValue=} [properties] Properties to set
             */
            function BoolValue(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * BoolValue value.
             * @member {boolean} value
             * @memberof google.protobuf.BoolValue
             * @instance
             */
            BoolValue.prototype.value = false;

            /**
             * Creates a new BoolValue instance using the specified properties.
             * @function create
             * @memberof google.protobuf.BoolValue
             * @static
             * @param {google.protobuf.IBoolValue=} [properties] Properties to set
             * @returns {google.protobuf.BoolValue} BoolValue instance
             */
            BoolValue.create = function create(properties) {
                return new BoolValue(properties);
            };

            /**
             * Encodes the specified BoolValue message. Does not implicitly {@link google.protobuf.BoolValue.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.BoolValue
             * @static
             * @param {google.protobuf.IBoolValue} message BoolValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BoolValue.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 0 =*/8).bool(message.value);
                return writer;
            };

            /**
             * Encodes the specified BoolValue message, length delimited. Does not implicitly {@link google.protobuf.BoolValue.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.BoolValue
             * @static
             * @param {google.protobuf.IBoolValue} message BoolValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BoolValue.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a BoolValue message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.BoolValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.BoolValue} BoolValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BoolValue.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.BoolValue();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.bool();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a BoolValue message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.BoolValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.BoolValue} BoolValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BoolValue.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a BoolValue message.
             * @function verify
             * @memberof google.protobuf.BoolValue
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            BoolValue.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (typeof message.value !== "boolean")
                        return "value: boolean expected";
                return null;
            };

            /**
             * Creates a BoolValue message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.BoolValue
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.BoolValue} BoolValue
             */
            BoolValue.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.BoolValue)
                    return object;
                var message = new $root.google.protobuf.BoolValue();
                if (object.value != null)
                    message.value = Boolean(object.value);
                return message;
            };

            /**
             * Creates a plain object from a BoolValue message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.BoolValue
             * @static
             * @param {google.protobuf.BoolValue} message BoolValue
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            BoolValue.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    object.value = false;
                if (message.value != null && message.hasOwnProperty("value"))
                    object.value = message.value;
                return object;
            };

            /**
             * Converts this BoolValue to JSON.
             * @function toJSON
             * @memberof google.protobuf.BoolValue
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            BoolValue.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return BoolValue;
        })();

        protobuf.StringValue = (function() {

            /**
             * Properties of a StringValue.
             * @memberof google.protobuf
             * @interface IStringValue
             * @property {string|null} [value] StringValue value
             */

            /**
             * Constructs a new StringValue.
             * @memberof google.protobuf
             * @classdesc Represents a StringValue.
             * @implements IStringValue
             * @constructor
             * @param {google.protobuf.IStringValue=} [properties] Properties to set
             */
            function StringValue(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * StringValue value.
             * @member {string} value
             * @memberof google.protobuf.StringValue
             * @instance
             */
            StringValue.prototype.value = "";

            /**
             * Creates a new StringValue instance using the specified properties.
             * @function create
             * @memberof google.protobuf.StringValue
             * @static
             * @param {google.protobuf.IStringValue=} [properties] Properties to set
             * @returns {google.protobuf.StringValue} StringValue instance
             */
            StringValue.create = function create(properties) {
                return new StringValue(properties);
            };

            /**
             * Encodes the specified StringValue message. Does not implicitly {@link google.protobuf.StringValue.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.StringValue
             * @static
             * @param {google.protobuf.IStringValue} message StringValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            StringValue.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 2 =*/10).string(message.value);
                return writer;
            };

            /**
             * Encodes the specified StringValue message, length delimited. Does not implicitly {@link google.protobuf.StringValue.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.StringValue
             * @static
             * @param {google.protobuf.IStringValue} message StringValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            StringValue.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a StringValue message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.StringValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.StringValue} StringValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            StringValue.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.StringValue();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.string();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a StringValue message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.StringValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.StringValue} StringValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            StringValue.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a StringValue message.
             * @function verify
             * @memberof google.protobuf.StringValue
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            StringValue.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (!$util.isString(message.value))
                        return "value: string expected";
                return null;
            };

            /**
             * Creates a StringValue message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.StringValue
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.StringValue} StringValue
             */
            StringValue.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.StringValue)
                    return object;
                var message = new $root.google.protobuf.StringValue();
                if (object.value != null)
                    message.value = String(object.value);
                return message;
            };

            /**
             * Creates a plain object from a StringValue message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.StringValue
             * @static
             * @param {google.protobuf.StringValue} message StringValue
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            StringValue.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    object.value = "";
                if (message.value != null && message.hasOwnProperty("value"))
                    object.value = message.value;
                return object;
            };

            /**
             * Converts this StringValue to JSON.
             * @function toJSON
             * @memberof google.protobuf.StringValue
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            StringValue.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return StringValue;
        })();

        protobuf.BytesValue = (function() {

            /**
             * Properties of a BytesValue.
             * @memberof google.protobuf
             * @interface IBytesValue
             * @property {Uint8Array|null} [value] BytesValue value
             */

            /**
             * Constructs a new BytesValue.
             * @memberof google.protobuf
             * @classdesc Represents a BytesValue.
             * @implements IBytesValue
             * @constructor
             * @param {google.protobuf.IBytesValue=} [properties] Properties to set
             */
            function BytesValue(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * BytesValue value.
             * @member {Uint8Array} value
             * @memberof google.protobuf.BytesValue
             * @instance
             */
            BytesValue.prototype.value = $util.newBuffer([]);

            /**
             * Creates a new BytesValue instance using the specified properties.
             * @function create
             * @memberof google.protobuf.BytesValue
             * @static
             * @param {google.protobuf.IBytesValue=} [properties] Properties to set
             * @returns {google.protobuf.BytesValue} BytesValue instance
             */
            BytesValue.create = function create(properties) {
                return new BytesValue(properties);
            };

            /**
             * Encodes the specified BytesValue message. Does not implicitly {@link google.protobuf.BytesValue.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.BytesValue
             * @static
             * @param {google.protobuf.IBytesValue} message BytesValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BytesValue.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.value != null && message.hasOwnProperty("value"))
                    writer.uint32(/* id 1, wireType 2 =*/10).bytes(message.value);
                return writer;
            };

            /**
             * Encodes the specified BytesValue message, length delimited. Does not implicitly {@link google.protobuf.BytesValue.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.BytesValue
             * @static
             * @param {google.protobuf.IBytesValue} message BytesValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            BytesValue.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a BytesValue message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.BytesValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.BytesValue} BytesValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BytesValue.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.BytesValue();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.value = reader.bytes();
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a BytesValue message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.BytesValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.BytesValue} BytesValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            BytesValue.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a BytesValue message.
             * @function verify
             * @memberof google.protobuf.BytesValue
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            BytesValue.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.value != null && message.hasOwnProperty("value"))
                    if (!(message.value && typeof message.value.length === "number" || $util.isString(message.value)))
                        return "value: buffer expected";
                return null;
            };

            /**
             * Creates a BytesValue message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.BytesValue
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.BytesValue} BytesValue
             */
            BytesValue.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.BytesValue)
                    return object;
                var message = new $root.google.protobuf.BytesValue();
                if (object.value != null)
                    if (typeof object.value === "string")
                        $util.base64.decode(object.value, message.value = $util.newBuffer($util.base64.length(object.value)), 0);
                    else if (object.value.length)
                        message.value = object.value;
                return message;
            };

            /**
             * Creates a plain object from a BytesValue message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.BytesValue
             * @static
             * @param {google.protobuf.BytesValue} message BytesValue
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            BytesValue.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.defaults)
                    if (options.bytes === String)
                        object.value = "";
                    else {
                        object.value = [];
                        if (options.bytes !== Array)
                            object.value = $util.newBuffer(object.value);
                    }
                if (message.value != null && message.hasOwnProperty("value"))
                    object.value = options.bytes === String ? $util.base64.encode(message.value, 0, message.value.length) : options.bytes === Array ? Array.prototype.slice.call(message.value) : message.value;
                return object;
            };

            /**
             * Converts this BytesValue to JSON.
             * @function toJSON
             * @memberof google.protobuf.BytesValue
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            BytesValue.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return BytesValue;
        })();

        protobuf.Struct = (function() {

            /**
             * Properties of a Struct.
             * @memberof google.protobuf
             * @interface IStruct
             * @property {Object.<string,google.protobuf.IValue>|null} [fields] Struct fields
             */

            /**
             * Constructs a new Struct.
             * @memberof google.protobuf
             * @classdesc Represents a Struct.
             * @implements IStruct
             * @constructor
             * @param {google.protobuf.IStruct=} [properties] Properties to set
             */
            function Struct(properties) {
                this.fields = {};
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Struct fields.
             * @member {Object.<string,google.protobuf.IValue>} fields
             * @memberof google.protobuf.Struct
             * @instance
             */
            Struct.prototype.fields = $util.emptyObject;

            /**
             * Creates a new Struct instance using the specified properties.
             * @function create
             * @memberof google.protobuf.Struct
             * @static
             * @param {google.protobuf.IStruct=} [properties] Properties to set
             * @returns {google.protobuf.Struct} Struct instance
             */
            Struct.create = function create(properties) {
                return new Struct(properties);
            };

            /**
             * Encodes the specified Struct message. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.Struct
             * @static
             * @param {google.protobuf.IStruct} message Struct message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Struct.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.fields != null && message.hasOwnProperty("fields"))
                    for (var keys = Object.keys(message.fields), i = 0; i < keys.length; ++i) {
                        writer.uint32(/* id 1, wireType 2 =*/10).fork().uint32(/* id 1, wireType 2 =*/10).string(keys[i]);
                        $root.google.protobuf.Value.encode(message.fields[keys[i]], writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim().ldelim();
                    }
                return writer;
            };

            /**
             * Encodes the specified Struct message, length delimited. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.Struct
             * @static
             * @param {google.protobuf.IStruct} message Struct message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Struct.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a Struct message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.Struct
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.Struct} Struct
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Struct.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Struct(), key;
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        reader.skip().pos++;
                        if (message.fields === $util.emptyObject)
                            message.fields = {};
                        key = reader.string();
                        reader.pos++;
                        message.fields[key] = $root.google.protobuf.Value.decode(reader, reader.uint32());
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a Struct message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.Struct
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.Struct} Struct
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Struct.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a Struct message.
             * @function verify
             * @memberof google.protobuf.Struct
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Struct.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.fields != null && message.hasOwnProperty("fields")) {
                    if (!$util.isObject(message.fields))
                        return "fields: object expected";
                    var key = Object.keys(message.fields);
                    for (var i = 0; i < key.length; ++i) {
                        var error = $root.google.protobuf.Value.verify(message.fields[key[i]]);
                        if (error)
                            return "fields." + error;
                    }
                }
                return null;
            };

            /**
             * Creates a Struct message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.Struct
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.Struct} Struct
             */
            Struct.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.Struct)
                    return object;
                var message = new $root.google.protobuf.Struct();
                if (object.fields) {
                    if (typeof object.fields !== "object")
                        throw TypeError(".google.protobuf.Struct.fields: object expected");
                    message.fields = {};
                    for (var keys = Object.keys(object.fields), i = 0; i < keys.length; ++i) {
                        if (typeof object.fields[keys[i]] !== "object")
                            throw TypeError(".google.protobuf.Struct.fields: object expected");
                        message.fields[keys[i]] = $root.google.protobuf.Value.fromObject(object.fields[keys[i]]);
                    }
                }
                return message;
            };

            /**
             * Creates a plain object from a Struct message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.Struct
             * @static
             * @param {google.protobuf.Struct} message Struct
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Struct.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.objects || options.defaults)
                    object.fields = {};
                var keys2;
                if (message.fields && (keys2 = Object.keys(message.fields)).length) {
                    object.fields = {};
                    for (var j = 0; j < keys2.length; ++j)
                        object.fields[keys2[j]] = $root.google.protobuf.Value.toObject(message.fields[keys2[j]], options);
                }
                return object;
            };

            /**
             * Converts this Struct to JSON.
             * @function toJSON
             * @memberof google.protobuf.Struct
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Struct.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return Struct;
        })();

        protobuf.Value = (function() {

            /**
             * Properties of a Value.
             * @memberof google.protobuf
             * @interface IValue
             * @property {google.protobuf.NullValue|null} [nullValue] Value nullValue
             * @property {number|null} [numberValue] Value numberValue
             * @property {string|null} [stringValue] Value stringValue
             * @property {boolean|null} [boolValue] Value boolValue
             * @property {google.protobuf.IStruct|null} [structValue] Value structValue
             * @property {google.protobuf.IListValue|null} [listValue] Value listValue
             */

            /**
             * Constructs a new Value.
             * @memberof google.protobuf
             * @classdesc Represents a Value.
             * @implements IValue
             * @constructor
             * @param {google.protobuf.IValue=} [properties] Properties to set
             */
            function Value(properties) {
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * Value nullValue.
             * @member {google.protobuf.NullValue} nullValue
             * @memberof google.protobuf.Value
             * @instance
             */
            Value.prototype.nullValue = 0;

            /**
             * Value numberValue.
             * @member {number} numberValue
             * @memberof google.protobuf.Value
             * @instance
             */
            Value.prototype.numberValue = 0;

            /**
             * Value stringValue.
             * @member {string} stringValue
             * @memberof google.protobuf.Value
             * @instance
             */
            Value.prototype.stringValue = "";

            /**
             * Value boolValue.
             * @member {boolean} boolValue
             * @memberof google.protobuf.Value
             * @instance
             */
            Value.prototype.boolValue = false;

            /**
             * Value structValue.
             * @member {google.protobuf.IStruct|null|undefined} structValue
             * @memberof google.protobuf.Value
             * @instance
             */
            Value.prototype.structValue = null;

            /**
             * Value listValue.
             * @member {google.protobuf.IListValue|null|undefined} listValue
             * @memberof google.protobuf.Value
             * @instance
             */
            Value.prototype.listValue = null;

            // OneOf field names bound to virtual getters and setters
            var $oneOfFields;

            /**
             * Value kind.
             * @member {"nullValue"|"numberValue"|"stringValue"|"boolValue"|"structValue"|"listValue"|undefined} kind
             * @memberof google.protobuf.Value
             * @instance
             */
            Object.defineProperty(Value.prototype, "kind", {
                get: $util.oneOfGetter($oneOfFields = ["nullValue", "numberValue", "stringValue", "boolValue", "structValue", "listValue"]),
                set: $util.oneOfSetter($oneOfFields)
            });

            /**
             * Creates a new Value instance using the specified properties.
             * @function create
             * @memberof google.protobuf.Value
             * @static
             * @param {google.protobuf.IValue=} [properties] Properties to set
             * @returns {google.protobuf.Value} Value instance
             */
            Value.create = function create(properties) {
                return new Value(properties);
            };

            /**
             * Encodes the specified Value message. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.Value
             * @static
             * @param {google.protobuf.IValue} message Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Value.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.nullValue != null && message.hasOwnProperty("nullValue"))
                    writer.uint32(/* id 1, wireType 0 =*/8).int32(message.nullValue);
                if (message.numberValue != null && message.hasOwnProperty("numberValue"))
                    writer.uint32(/* id 2, wireType 1 =*/17).double(message.numberValue);
                if (message.stringValue != null && message.hasOwnProperty("stringValue"))
                    writer.uint32(/* id 3, wireType 2 =*/26).string(message.stringValue);
                if (message.boolValue != null && message.hasOwnProperty("boolValue"))
                    writer.uint32(/* id 4, wireType 0 =*/32).bool(message.boolValue);
                if (message.structValue != null && message.hasOwnProperty("structValue"))
                    $root.google.protobuf.Struct.encode(message.structValue, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
                if (message.listValue != null && message.hasOwnProperty("listValue"))
                    $root.google.protobuf.ListValue.encode(message.listValue, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
                return writer;
            };

            /**
             * Encodes the specified Value message, length delimited. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.Value
             * @static
             * @param {google.protobuf.IValue} message Value message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            Value.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a Value message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.Value} Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Value.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.Value();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        message.nullValue = reader.int32();
                        break;
                    case 2:
                        message.numberValue = reader.double();
                        break;
                    case 3:
                        message.stringValue = reader.string();
                        break;
                    case 4:
                        message.boolValue = reader.bool();
                        break;
                    case 5:
                        message.structValue = $root.google.protobuf.Struct.decode(reader, reader.uint32());
                        break;
                    case 6:
                        message.listValue = $root.google.protobuf.ListValue.decode(reader, reader.uint32());
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a Value message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.Value
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.Value} Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            Value.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a Value message.
             * @function verify
             * @memberof google.protobuf.Value
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            Value.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                var properties = {};
                if (message.nullValue != null && message.hasOwnProperty("nullValue")) {
                    properties.kind = 1;
                    switch (message.nullValue) {
                    default:
                        return "nullValue: enum value expected";
                    case 0:
                        break;
                    }
                }
                if (message.numberValue != null && message.hasOwnProperty("numberValue")) {
                    if (properties.kind === 1)
                        return "kind: multiple values";
                    properties.kind = 1;
                    if (typeof message.numberValue !== "number")
                        return "numberValue: number expected";
                }
                if (message.stringValue != null && message.hasOwnProperty("stringValue")) {
                    if (properties.kind === 1)
                        return "kind: multiple values";
                    properties.kind = 1;
                    if (!$util.isString(message.stringValue))
                        return "stringValue: string expected";
                }
                if (message.boolValue != null && message.hasOwnProperty("boolValue")) {
                    if (properties.kind === 1)
                        return "kind: multiple values";
                    properties.kind = 1;
                    if (typeof message.boolValue !== "boolean")
                        return "boolValue: boolean expected";
                }
                if (message.structValue != null && message.hasOwnProperty("structValue")) {
                    if (properties.kind === 1)
                        return "kind: multiple values";
                    properties.kind = 1;
                    {
                        var error = $root.google.protobuf.Struct.verify(message.structValue);
                        if (error)
                            return "structValue." + error;
                    }
                }
                if (message.listValue != null && message.hasOwnProperty("listValue")) {
                    if (properties.kind === 1)
                        return "kind: multiple values";
                    properties.kind = 1;
                    {
                        var error = $root.google.protobuf.ListValue.verify(message.listValue);
                        if (error)
                            return "listValue." + error;
                    }
                }
                return null;
            };

            /**
             * Creates a Value message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.Value
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.Value} Value
             */
            Value.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.Value)
                    return object;
                var message = new $root.google.protobuf.Value();
                switch (object.nullValue) {
                case "NULL_VALUE":
                case 0:
                    message.nullValue = 0;
                    break;
                }
                if (object.numberValue != null)
                    message.numberValue = Number(object.numberValue);
                if (object.stringValue != null)
                    message.stringValue = String(object.stringValue);
                if (object.boolValue != null)
                    message.boolValue = Boolean(object.boolValue);
                if (object.structValue != null) {
                    if (typeof object.structValue !== "object")
                        throw TypeError(".google.protobuf.Value.structValue: object expected");
                    message.structValue = $root.google.protobuf.Struct.fromObject(object.structValue);
                }
                if (object.listValue != null) {
                    if (typeof object.listValue !== "object")
                        throw TypeError(".google.protobuf.Value.listValue: object expected");
                    message.listValue = $root.google.protobuf.ListValue.fromObject(object.listValue);
                }
                return message;
            };

            /**
             * Creates a plain object from a Value message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.Value
             * @static
             * @param {google.protobuf.Value} message Value
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            Value.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (message.nullValue != null && message.hasOwnProperty("nullValue")) {
                    object.nullValue = options.enums === String ? $root.google.protobuf.NullValue[message.nullValue] : message.nullValue;
                    if (options.oneofs)
                        object.kind = "nullValue";
                }
                if (message.numberValue != null && message.hasOwnProperty("numberValue")) {
                    object.numberValue = options.json && !isFinite(message.numberValue) ? String(message.numberValue) : message.numberValue;
                    if (options.oneofs)
                        object.kind = "numberValue";
                }
                if (message.stringValue != null && message.hasOwnProperty("stringValue")) {
                    object.stringValue = message.stringValue;
                    if (options.oneofs)
                        object.kind = "stringValue";
                }
                if (message.boolValue != null && message.hasOwnProperty("boolValue")) {
                    object.boolValue = message.boolValue;
                    if (options.oneofs)
                        object.kind = "boolValue";
                }
                if (message.structValue != null && message.hasOwnProperty("structValue")) {
                    object.structValue = $root.google.protobuf.Struct.toObject(message.structValue, options);
                    if (options.oneofs)
                        object.kind = "structValue";
                }
                if (message.listValue != null && message.hasOwnProperty("listValue")) {
                    object.listValue = $root.google.protobuf.ListValue.toObject(message.listValue, options);
                    if (options.oneofs)
                        object.kind = "listValue";
                }
                return object;
            };

            /**
             * Converts this Value to JSON.
             * @function toJSON
             * @memberof google.protobuf.Value
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            Value.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return Value;
        })();

        /**
         * NullValue enum.
         * @name google.protobuf.NullValue
         * @enum {string}
         * @property {number} NULL_VALUE=0 NULL_VALUE value
         */
        protobuf.NullValue = (function() {
            var valuesById = {}, values = Object.create(valuesById);
            values[valuesById[0] = "NULL_VALUE"] = 0;
            return values;
        })();

        protobuf.ListValue = (function() {

            /**
             * Properties of a ListValue.
             * @memberof google.protobuf
             * @interface IListValue
             * @property {Array.<google.protobuf.IValue>|null} [values] ListValue values
             */

            /**
             * Constructs a new ListValue.
             * @memberof google.protobuf
             * @classdesc Represents a ListValue.
             * @implements IListValue
             * @constructor
             * @param {google.protobuf.IListValue=} [properties] Properties to set
             */
            function ListValue(properties) {
                this.values = [];
                if (properties)
                    for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                        if (properties[keys[i]] != null)
                            this[keys[i]] = properties[keys[i]];
            }

            /**
             * ListValue values.
             * @member {Array.<google.protobuf.IValue>} values
             * @memberof google.protobuf.ListValue
             * @instance
             */
            ListValue.prototype.values = $util.emptyArray;

            /**
             * Creates a new ListValue instance using the specified properties.
             * @function create
             * @memberof google.protobuf.ListValue
             * @static
             * @param {google.protobuf.IListValue=} [properties] Properties to set
             * @returns {google.protobuf.ListValue} ListValue instance
             */
            ListValue.create = function create(properties) {
                return new ListValue(properties);
            };

            /**
             * Encodes the specified ListValue message. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
             * @function encode
             * @memberof google.protobuf.ListValue
             * @static
             * @param {google.protobuf.IListValue} message ListValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            ListValue.encode = function encode(message, writer) {
                if (!writer)
                    writer = $Writer.create();
                if (message.values != null && message.values.length)
                    for (var i = 0; i < message.values.length; ++i)
                        $root.google.protobuf.Value.encode(message.values[i], writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
                return writer;
            };

            /**
             * Encodes the specified ListValue message, length delimited. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
             * @function encodeDelimited
             * @memberof google.protobuf.ListValue
             * @static
             * @param {google.protobuf.IListValue} message ListValue message or plain object to encode
             * @param {$protobuf.Writer} [writer] Writer to encode to
             * @returns {$protobuf.Writer} Writer
             */
            ListValue.encodeDelimited = function encodeDelimited(message, writer) {
                return this.encode(message, writer).ldelim();
            };

            /**
             * Decodes a ListValue message from the specified reader or buffer.
             * @function decode
             * @memberof google.protobuf.ListValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @param {number} [length] Message length if known beforehand
             * @returns {google.protobuf.ListValue} ListValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            ListValue.decode = function decode(reader, length) {
                if (!(reader instanceof $Reader))
                    reader = $Reader.create(reader);
                var end = length === undefined ? reader.len : reader.pos + length, message = new $root.google.protobuf.ListValue();
                while (reader.pos < end) {
                    var tag = reader.uint32();
                    switch (tag >>> 3) {
                    case 1:
                        if (!(message.values && message.values.length))
                            message.values = [];
                        message.values.push($root.google.protobuf.Value.decode(reader, reader.uint32()));
                        break;
                    default:
                        reader.skipType(tag & 7);
                        break;
                    }
                }
                return message;
            };

            /**
             * Decodes a ListValue message from the specified reader or buffer, length delimited.
             * @function decodeDelimited
             * @memberof google.protobuf.ListValue
             * @static
             * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
             * @returns {google.protobuf.ListValue} ListValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            ListValue.decodeDelimited = function decodeDelimited(reader) {
                if (!(reader instanceof $Reader))
                    reader = new $Reader(reader);
                return this.decode(reader, reader.uint32());
            };

            /**
             * Verifies a ListValue message.
             * @function verify
             * @memberof google.protobuf.ListValue
             * @static
             * @param {Object.<string,*>} message Plain object to verify
             * @returns {string|null} `null` if valid, otherwise the reason why it is not
             */
            ListValue.verify = function verify(message) {
                if (typeof message !== "object" || message === null)
                    return "object expected";
                if (message.values != null && message.hasOwnProperty("values")) {
                    if (!Array.isArray(message.values))
                        return "values: array expected";
                    for (var i = 0; i < message.values.length; ++i) {
                        var error = $root.google.protobuf.Value.verify(message.values[i]);
                        if (error)
                            return "values." + error;
                    }
                }
                return null;
            };

            /**
             * Creates a ListValue message from a plain object. Also converts values to their respective internal types.
             * @function fromObject
             * @memberof google.protobuf.ListValue
             * @static
             * @param {Object.<string,*>} object Plain object
             * @returns {google.protobuf.ListValue} ListValue
             */
            ListValue.fromObject = function fromObject(object) {
                if (object instanceof $root.google.protobuf.ListValue)
                    return object;
                var message = new $root.google.protobuf.ListValue();
                if (object.values) {
                    if (!Array.isArray(object.values))
                        throw TypeError(".google.protobuf.ListValue.values: array expected");
                    message.values = [];
                    for (var i = 0; i < object.values.length; ++i) {
                        if (typeof object.values[i] !== "object")
                            throw TypeError(".google.protobuf.ListValue.values: object expected");
                        message.values[i] = $root.google.protobuf.Value.fromObject(object.values[i]);
                    }
                }
                return message;
            };

            /**
             * Creates a plain object from a ListValue message. Also converts values to other types if specified.
             * @function toObject
             * @memberof google.protobuf.ListValue
             * @static
             * @param {google.protobuf.ListValue} message ListValue
             * @param {$protobuf.IConversionOptions} [options] Conversion options
             * @returns {Object.<string,*>} Plain object
             */
            ListValue.toObject = function toObject(message, options) {
                if (!options)
                    options = {};
                var object = {};
                if (options.arrays || options.defaults)
                    object.values = [];
                if (message.values && message.values.length) {
                    object.values = [];
                    for (var j = 0; j < message.values.length; ++j)
                        object.values[j] = $root.google.protobuf.Value.toObject(message.values[j], options);
                }
                return object;
            };

            /**
             * Converts this ListValue to JSON.
             * @function toJSON
             * @memberof google.protobuf.ListValue
             * @instance
             * @returns {Object.<string,*>} JSON object
             */
            ListValue.prototype.toJSON = function toJSON() {
                return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
            };

            return ListValue;
        })();

        return protobuf;
    })();

    return google;
})();

module.exports = $root;
