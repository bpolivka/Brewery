import * as $protobuf from "protobufjs";
/** Namespace brewery. */
export namespace brewery {

    /** TemperatureProbe enum. */
    enum TemperatureProbe {
        TEMPERATURE_PROBE_UNKNOWN = 0,
        TEMPERATURE_PROBE_HLT = 1,
        TEMPERATURE_PROBE_BOIL = 2,
        TEMPERATURE_PROBE_MLT_IN = 3,
        TEMPERATURE_PROBE_MLT_OUT = 4
    }

    /** Output enum. */
    enum Output {
        OUTPUT_UNKNOWN = 0,
        OUTPUT_HLT = 1,
        OUTPUT_BOIL = 2,
        OUTPUT_PUMP1 = 3,
        OUTPUT_PUMP2 = 4,
        OUTPUT_STIR = 5,
        OUTPUT_FAN = 6,
        OUTPUT_BUZZ = 7,
        OUTPUT_TBD = 8,
        OUTPUT_HLT_SSR = 9,
        OUTPUT_BOIL_SSR = 10
    }

    /** Controller enum. */
    enum Controller {
        CONTROLLER_UNKNOWN = 0,
        CONTROLLER_HLT = 1,
        CONTROLLER_BOIL = 2
    }

    /** Properties of a TemperatureUpdatedEvent. */
    interface ITemperatureUpdatedEvent {

        /** TemperatureUpdatedEvent probe */
        probe?: (brewery.TemperatureProbe|null);

        /** TemperatureUpdatedEvent temperature */
        temperature?: (number|null);

        /** TemperatureUpdatedEvent unadjustedTemperature */
        unadjustedTemperature?: (number|null);
    }

    /** Represents a TemperatureUpdatedEvent. */
    class TemperatureUpdatedEvent implements ITemperatureUpdatedEvent {

        /**
         * Constructs a new TemperatureUpdatedEvent.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.ITemperatureUpdatedEvent);

        /** TemperatureUpdatedEvent probe. */
        public probe: brewery.TemperatureProbe;

        /** TemperatureUpdatedEvent temperature. */
        public temperature: number;

        /** TemperatureUpdatedEvent unadjustedTemperature. */
        public unadjustedTemperature: number;

        /**
         * Creates a new TemperatureUpdatedEvent instance using the specified properties.
         * @param [properties] Properties to set
         * @returns TemperatureUpdatedEvent instance
         */
        public static create(properties?: brewery.ITemperatureUpdatedEvent): brewery.TemperatureUpdatedEvent;

        /**
         * Encodes the specified TemperatureUpdatedEvent message. Does not implicitly {@link brewery.TemperatureUpdatedEvent.verify|verify} messages.
         * @param message TemperatureUpdatedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.ITemperatureUpdatedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified TemperatureUpdatedEvent message, length delimited. Does not implicitly {@link brewery.TemperatureUpdatedEvent.verify|verify} messages.
         * @param message TemperatureUpdatedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.ITemperatureUpdatedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a TemperatureUpdatedEvent message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns TemperatureUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.TemperatureUpdatedEvent;

        /**
         * Decodes a TemperatureUpdatedEvent message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns TemperatureUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.TemperatureUpdatedEvent;

        /**
         * Verifies a TemperatureUpdatedEvent message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a TemperatureUpdatedEvent message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns TemperatureUpdatedEvent
         */
        public static fromObject(object: { [k: string]: any }): brewery.TemperatureUpdatedEvent;

        /**
         * Creates a plain object from a TemperatureUpdatedEvent message. Also converts values to other types if specified.
         * @param message TemperatureUpdatedEvent
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.TemperatureUpdatedEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this TemperatureUpdatedEvent to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an OutputState. */
    interface IOutputState {

        /** OutputState output */
        output?: (brewery.Output|null);

        /** OutputState enabled */
        enabled?: (boolean|null);
    }

    /** Represents an OutputState. */
    class OutputState implements IOutputState {

        /**
         * Constructs a new OutputState.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.IOutputState);

        /** OutputState output. */
        public output: brewery.Output;

        /** OutputState enabled. */
        public enabled: boolean;

        /**
         * Creates a new OutputState instance using the specified properties.
         * @param [properties] Properties to set
         * @returns OutputState instance
         */
        public static create(properties?: brewery.IOutputState): brewery.OutputState;

        /**
         * Encodes the specified OutputState message. Does not implicitly {@link brewery.OutputState.verify|verify} messages.
         * @param message OutputState message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.IOutputState, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified OutputState message, length delimited. Does not implicitly {@link brewery.OutputState.verify|verify} messages.
         * @param message OutputState message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.IOutputState, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an OutputState message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns OutputState
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.OutputState;

        /**
         * Decodes an OutputState message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns OutputState
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.OutputState;

        /**
         * Verifies an OutputState message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an OutputState message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns OutputState
         */
        public static fromObject(object: { [k: string]: any }): brewery.OutputState;

        /**
         * Creates a plain object from an OutputState message. Also converts values to other types if specified.
         * @param message OutputState
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.OutputState, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this OutputState to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an OutputStateChangedEvent. */
    interface IOutputStateChangedEvent {

        /** OutputStateChangedEvent state */
        state?: (brewery.IOutputState|null);
    }

    /** Represents an OutputStateChangedEvent. */
    class OutputStateChangedEvent implements IOutputStateChangedEvent {

        /**
         * Constructs a new OutputStateChangedEvent.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.IOutputStateChangedEvent);

        /** OutputStateChangedEvent state. */
        public state?: (brewery.IOutputState|null);

        /**
         * Creates a new OutputStateChangedEvent instance using the specified properties.
         * @param [properties] Properties to set
         * @returns OutputStateChangedEvent instance
         */
        public static create(properties?: brewery.IOutputStateChangedEvent): brewery.OutputStateChangedEvent;

        /**
         * Encodes the specified OutputStateChangedEvent message. Does not implicitly {@link brewery.OutputStateChangedEvent.verify|verify} messages.
         * @param message OutputStateChangedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.IOutputStateChangedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified OutputStateChangedEvent message, length delimited. Does not implicitly {@link brewery.OutputStateChangedEvent.verify|verify} messages.
         * @param message OutputStateChangedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.IOutputStateChangedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an OutputStateChangedEvent message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns OutputStateChangedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.OutputStateChangedEvent;

        /**
         * Decodes an OutputStateChangedEvent message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns OutputStateChangedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.OutputStateChangedEvent;

        /**
         * Verifies an OutputStateChangedEvent message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an OutputStateChangedEvent message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns OutputStateChangedEvent
         */
        public static fromObject(object: { [k: string]: any }): brewery.OutputStateChangedEvent;

        /**
         * Creates a plain object from an OutputStateChangedEvent message. Also converts values to other types if specified.
         * @param message OutputStateChangedEvent
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.OutputStateChangedEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this OutputStateChangedEvent to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SetOutputStateCommand. */
    interface ISetOutputStateCommand {

        /** SetOutputStateCommand state */
        state?: (brewery.IOutputState|null);
    }

    /** Represents a SetOutputStateCommand. */
    class SetOutputStateCommand implements ISetOutputStateCommand {

        /**
         * Constructs a new SetOutputStateCommand.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.ISetOutputStateCommand);

        /** SetOutputStateCommand state. */
        public state?: (brewery.IOutputState|null);

        /**
         * Creates a new SetOutputStateCommand instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SetOutputStateCommand instance
         */
        public static create(properties?: brewery.ISetOutputStateCommand): brewery.SetOutputStateCommand;

        /**
         * Encodes the specified SetOutputStateCommand message. Does not implicitly {@link brewery.SetOutputStateCommand.verify|verify} messages.
         * @param message SetOutputStateCommand message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.ISetOutputStateCommand, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SetOutputStateCommand message, length delimited. Does not implicitly {@link brewery.SetOutputStateCommand.verify|verify} messages.
         * @param message SetOutputStateCommand message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.ISetOutputStateCommand, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SetOutputStateCommand message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SetOutputStateCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.SetOutputStateCommand;

        /**
         * Decodes a SetOutputStateCommand message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SetOutputStateCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.SetOutputStateCommand;

        /**
         * Verifies a SetOutputStateCommand message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SetOutputStateCommand message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SetOutputStateCommand
         */
        public static fromObject(object: { [k: string]: any }): brewery.SetOutputStateCommand;

        /**
         * Creates a plain object from a SetOutputStateCommand message. Also converts values to other types if specified.
         * @param message SetOutputStateCommand
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.SetOutputStateCommand, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SetOutputStateCommand to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an EnableOutputForTimeCommand. */
    interface IEnableOutputForTimeCommand {

        /** EnableOutputForTimeCommand output */
        output?: (brewery.Output|null);

        /** EnableOutputForTimeCommand milliseconds */
        milliseconds?: (number|null);

        /** EnableOutputForTimeCommand responseTopic */
        responseTopic?: (string|null);

        /** EnableOutputForTimeCommand requestId */
        requestId?: (number|null);
    }

    /** Represents an EnableOutputForTimeCommand. */
    class EnableOutputForTimeCommand implements IEnableOutputForTimeCommand {

        /**
         * Constructs a new EnableOutputForTimeCommand.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.IEnableOutputForTimeCommand);

        /** EnableOutputForTimeCommand output. */
        public output: brewery.Output;

        /** EnableOutputForTimeCommand milliseconds. */
        public milliseconds: number;

        /** EnableOutputForTimeCommand responseTopic. */
        public responseTopic: string;

        /** EnableOutputForTimeCommand requestId. */
        public requestId: number;

        /**
         * Creates a new EnableOutputForTimeCommand instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EnableOutputForTimeCommand instance
         */
        public static create(properties?: brewery.IEnableOutputForTimeCommand): brewery.EnableOutputForTimeCommand;

        /**
         * Encodes the specified EnableOutputForTimeCommand message. Does not implicitly {@link brewery.EnableOutputForTimeCommand.verify|verify} messages.
         * @param message EnableOutputForTimeCommand message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.IEnableOutputForTimeCommand, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified EnableOutputForTimeCommand message, length delimited. Does not implicitly {@link brewery.EnableOutputForTimeCommand.verify|verify} messages.
         * @param message EnableOutputForTimeCommand message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.IEnableOutputForTimeCommand, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an EnableOutputForTimeCommand message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EnableOutputForTimeCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.EnableOutputForTimeCommand;

        /**
         * Decodes an EnableOutputForTimeCommand message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EnableOutputForTimeCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.EnableOutputForTimeCommand;

        /**
         * Verifies an EnableOutputForTimeCommand message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an EnableOutputForTimeCommand message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns EnableOutputForTimeCommand
         */
        public static fromObject(object: { [k: string]: any }): brewery.EnableOutputForTimeCommand;

        /**
         * Creates a plain object from an EnableOutputForTimeCommand message. Also converts values to other types if specified.
         * @param message EnableOutputForTimeCommand
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.EnableOutputForTimeCommand, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this EnableOutputForTimeCommand to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a RequestCompletedEvent. */
    interface IRequestCompletedEvent {

        /** RequestCompletedEvent requestId */
        requestId?: (number|null);
    }

    /** Represents a RequestCompletedEvent. */
    class RequestCompletedEvent implements IRequestCompletedEvent {

        /**
         * Constructs a new RequestCompletedEvent.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.IRequestCompletedEvent);

        /** RequestCompletedEvent requestId. */
        public requestId: number;

        /**
         * Creates a new RequestCompletedEvent instance using the specified properties.
         * @param [properties] Properties to set
         * @returns RequestCompletedEvent instance
         */
        public static create(properties?: brewery.IRequestCompletedEvent): brewery.RequestCompletedEvent;

        /**
         * Encodes the specified RequestCompletedEvent message. Does not implicitly {@link brewery.RequestCompletedEvent.verify|verify} messages.
         * @param message RequestCompletedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.IRequestCompletedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified RequestCompletedEvent message, length delimited. Does not implicitly {@link brewery.RequestCompletedEvent.verify|verify} messages.
         * @param message RequestCompletedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.IRequestCompletedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a RequestCompletedEvent message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns RequestCompletedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.RequestCompletedEvent;

        /**
         * Decodes a RequestCompletedEvent message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns RequestCompletedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.RequestCompletedEvent;

        /**
         * Verifies a RequestCompletedEvent message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a RequestCompletedEvent message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns RequestCompletedEvent
         */
        public static fromObject(object: { [k: string]: any }): brewery.RequestCompletedEvent;

        /**
         * Creates a plain object from a RequestCompletedEvent message. Also converts values to other types if specified.
         * @param message RequestCompletedEvent
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.RequestCompletedEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this RequestCompletedEvent to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a ControllerUpdatedEvent. */
    interface IControllerUpdatedEvent {

        /** ControllerUpdatedEvent controller */
        controller?: (brewery.Controller|null);

        /** ControllerUpdatedEvent dutyCycle */
        dutyCycle?: (number|null);

        /** ControllerUpdatedEvent strategy */
        strategy?: (string|null);

        /** ControllerUpdatedEvent values */
        values?: (google.protobuf.IStruct|null);
    }

    /** Represents a ControllerUpdatedEvent. */
    class ControllerUpdatedEvent implements IControllerUpdatedEvent {

        /**
         * Constructs a new ControllerUpdatedEvent.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.IControllerUpdatedEvent);

        /** ControllerUpdatedEvent controller. */
        public controller: brewery.Controller;

        /** ControllerUpdatedEvent dutyCycle. */
        public dutyCycle: number;

        /** ControllerUpdatedEvent strategy. */
        public strategy: string;

        /** ControllerUpdatedEvent values. */
        public values?: (google.protobuf.IStruct|null);

        /**
         * Creates a new ControllerUpdatedEvent instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ControllerUpdatedEvent instance
         */
        public static create(properties?: brewery.IControllerUpdatedEvent): brewery.ControllerUpdatedEvent;

        /**
         * Encodes the specified ControllerUpdatedEvent message. Does not implicitly {@link brewery.ControllerUpdatedEvent.verify|verify} messages.
         * @param message ControllerUpdatedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.IControllerUpdatedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ControllerUpdatedEvent message, length delimited. Does not implicitly {@link brewery.ControllerUpdatedEvent.verify|verify} messages.
         * @param message ControllerUpdatedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.IControllerUpdatedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ControllerUpdatedEvent message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ControllerUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.ControllerUpdatedEvent;

        /**
         * Decodes a ControllerUpdatedEvent message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ControllerUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.ControllerUpdatedEvent;

        /**
         * Verifies a ControllerUpdatedEvent message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a ControllerUpdatedEvent message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ControllerUpdatedEvent
         */
        public static fromObject(object: { [k: string]: any }): brewery.ControllerUpdatedEvent;

        /**
         * Creates a plain object from a ControllerUpdatedEvent message. Also converts values to other types if specified.
         * @param message ControllerUpdatedEvent
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.ControllerUpdatedEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ControllerUpdatedEvent to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of an UpdateControllerStategyCommand. */
    interface IUpdateControllerStategyCommand {

        /** UpdateControllerStategyCommand controller */
        controller?: (brewery.Controller|null);

        /** UpdateControllerStategyCommand strategy */
        strategy?: (string|null);

        /** UpdateControllerStategyCommand parameters */
        parameters?: (google.protobuf.IStruct|null);
    }

    /** Represents an UpdateControllerStategyCommand. */
    class UpdateControllerStategyCommand implements IUpdateControllerStategyCommand {

        /**
         * Constructs a new UpdateControllerStategyCommand.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.IUpdateControllerStategyCommand);

        /** UpdateControllerStategyCommand controller. */
        public controller: brewery.Controller;

        /** UpdateControllerStategyCommand strategy. */
        public strategy: string;

        /** UpdateControllerStategyCommand parameters. */
        public parameters?: (google.protobuf.IStruct|null);

        /**
         * Creates a new UpdateControllerStategyCommand instance using the specified properties.
         * @param [properties] Properties to set
         * @returns UpdateControllerStategyCommand instance
         */
        public static create(properties?: brewery.IUpdateControllerStategyCommand): brewery.UpdateControllerStategyCommand;

        /**
         * Encodes the specified UpdateControllerStategyCommand message. Does not implicitly {@link brewery.UpdateControllerStategyCommand.verify|verify} messages.
         * @param message UpdateControllerStategyCommand message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.IUpdateControllerStategyCommand, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified UpdateControllerStategyCommand message, length delimited. Does not implicitly {@link brewery.UpdateControllerStategyCommand.verify|verify} messages.
         * @param message UpdateControllerStategyCommand message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.IUpdateControllerStategyCommand, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an UpdateControllerStategyCommand message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns UpdateControllerStategyCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.UpdateControllerStategyCommand;

        /**
         * Decodes an UpdateControllerStategyCommand message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns UpdateControllerStategyCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.UpdateControllerStategyCommand;

        /**
         * Verifies an UpdateControllerStategyCommand message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an UpdateControllerStategyCommand message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns UpdateControllerStategyCommand
         */
        public static fromObject(object: { [k: string]: any }): brewery.UpdateControllerStategyCommand;

        /**
         * Creates a plain object from an UpdateControllerStategyCommand message. Also converts values to other types if specified.
         * @param message UpdateControllerStategyCommand
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.UpdateControllerStategyCommand, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this UpdateControllerStategyCommand to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a ControllerStrategyUpdatedEvent. */
    interface IControllerStrategyUpdatedEvent {

        /** ControllerStrategyUpdatedEvent controller */
        controller?: (brewery.Controller|null);

        /** ControllerStrategyUpdatedEvent strategy */
        strategy?: (string|null);

        /** ControllerStrategyUpdatedEvent parameters */
        parameters?: (google.protobuf.IStruct|null);
    }

    /** Represents a ControllerStrategyUpdatedEvent. */
    class ControllerStrategyUpdatedEvent implements IControllerStrategyUpdatedEvent {

        /**
         * Constructs a new ControllerStrategyUpdatedEvent.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.IControllerStrategyUpdatedEvent);

        /** ControllerStrategyUpdatedEvent controller. */
        public controller: brewery.Controller;

        /** ControllerStrategyUpdatedEvent strategy. */
        public strategy: string;

        /** ControllerStrategyUpdatedEvent parameters. */
        public parameters?: (google.protobuf.IStruct|null);

        /**
         * Creates a new ControllerStrategyUpdatedEvent instance using the specified properties.
         * @param [properties] Properties to set
         * @returns ControllerStrategyUpdatedEvent instance
         */
        public static create(properties?: brewery.IControllerStrategyUpdatedEvent): brewery.ControllerStrategyUpdatedEvent;

        /**
         * Encodes the specified ControllerStrategyUpdatedEvent message. Does not implicitly {@link brewery.ControllerStrategyUpdatedEvent.verify|verify} messages.
         * @param message ControllerStrategyUpdatedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.IControllerStrategyUpdatedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified ControllerStrategyUpdatedEvent message, length delimited. Does not implicitly {@link brewery.ControllerStrategyUpdatedEvent.verify|verify} messages.
         * @param message ControllerStrategyUpdatedEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.IControllerStrategyUpdatedEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a ControllerStrategyUpdatedEvent message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns ControllerStrategyUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.ControllerStrategyUpdatedEvent;

        /**
         * Decodes a ControllerStrategyUpdatedEvent message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns ControllerStrategyUpdatedEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.ControllerStrategyUpdatedEvent;

        /**
         * Verifies a ControllerStrategyUpdatedEvent message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a ControllerStrategyUpdatedEvent message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns ControllerStrategyUpdatedEvent
         */
        public static fromObject(object: { [k: string]: any }): brewery.ControllerStrategyUpdatedEvent;

        /**
         * Creates a plain object from a ControllerStrategyUpdatedEvent message. Also converts values to other types if specified.
         * @param message ControllerStrategyUpdatedEvent
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.ControllerStrategyUpdatedEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this ControllerStrategyUpdatedEvent to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SendSystemStateCommand. */
    interface ISendSystemStateCommand {

        /** SendSystemStateCommand responseTopic */
        responseTopic?: (string|null);
    }

    /** Represents a SendSystemStateCommand. */
    class SendSystemStateCommand implements ISendSystemStateCommand {

        /**
         * Constructs a new SendSystemStateCommand.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.ISendSystemStateCommand);

        /** SendSystemStateCommand responseTopic. */
        public responseTopic: string;

        /**
         * Creates a new SendSystemStateCommand instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SendSystemStateCommand instance
         */
        public static create(properties?: brewery.ISendSystemStateCommand): brewery.SendSystemStateCommand;

        /**
         * Encodes the specified SendSystemStateCommand message. Does not implicitly {@link brewery.SendSystemStateCommand.verify|verify} messages.
         * @param message SendSystemStateCommand message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.ISendSystemStateCommand, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SendSystemStateCommand message, length delimited. Does not implicitly {@link brewery.SendSystemStateCommand.verify|verify} messages.
         * @param message SendSystemStateCommand message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.ISendSystemStateCommand, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SendSystemStateCommand message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SendSystemStateCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.SendSystemStateCommand;

        /**
         * Decodes a SendSystemStateCommand message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SendSystemStateCommand
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.SendSystemStateCommand;

        /**
         * Verifies a SendSystemStateCommand message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SendSystemStateCommand message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SendSystemStateCommand
         */
        public static fromObject(object: { [k: string]: any }): brewery.SendSystemStateCommand;

        /**
         * Creates a plain object from a SendSystemStateCommand message. Also converts values to other types if specified.
         * @param message SendSystemStateCommand
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.SendSystemStateCommand, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SendSystemStateCommand to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }

    /** Properties of a SystemStateEvent. */
    interface ISystemStateEvent {

        /** SystemStateEvent outputStates */
        outputStates?: (brewery.IOutputState[]|null);
    }

    /** Represents a SystemStateEvent. */
    class SystemStateEvent implements ISystemStateEvent {

        /**
         * Constructs a new SystemStateEvent.
         * @param [properties] Properties to set
         */
        constructor(properties?: brewery.ISystemStateEvent);

        /** SystemStateEvent outputStates. */
        public outputStates: brewery.IOutputState[];

        /**
         * Creates a new SystemStateEvent instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SystemStateEvent instance
         */
        public static create(properties?: brewery.ISystemStateEvent): brewery.SystemStateEvent;

        /**
         * Encodes the specified SystemStateEvent message. Does not implicitly {@link brewery.SystemStateEvent.verify|verify} messages.
         * @param message SystemStateEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: brewery.ISystemStateEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SystemStateEvent message, length delimited. Does not implicitly {@link brewery.SystemStateEvent.verify|verify} messages.
         * @param message SystemStateEvent message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: brewery.ISystemStateEvent, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SystemStateEvent message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SystemStateEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): brewery.SystemStateEvent;

        /**
         * Decodes a SystemStateEvent message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SystemStateEvent
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): brewery.SystemStateEvent;

        /**
         * Verifies a SystemStateEvent message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SystemStateEvent message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SystemStateEvent
         */
        public static fromObject(object: { [k: string]: any }): brewery.SystemStateEvent;

        /**
         * Creates a plain object from a SystemStateEvent message. Also converts values to other types if specified.
         * @param message SystemStateEvent
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: brewery.SystemStateEvent, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SystemStateEvent to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };
    }
}

/** Namespace google. */
export namespace google {

    /** Namespace protobuf. */
    namespace protobuf {

        /** Properties of a DoubleValue. */
        interface IDoubleValue {

            /** DoubleValue value */
            value?: (number|null);
        }

        /** Represents a DoubleValue. */
        class DoubleValue implements IDoubleValue {

            /**
             * Constructs a new DoubleValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IDoubleValue);

            /** DoubleValue value. */
            public value: number;

            /**
             * Creates a new DoubleValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns DoubleValue instance
             */
            public static create(properties?: google.protobuf.IDoubleValue): google.protobuf.DoubleValue;

            /**
             * Encodes the specified DoubleValue message. Does not implicitly {@link google.protobuf.DoubleValue.verify|verify} messages.
             * @param message DoubleValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IDoubleValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified DoubleValue message, length delimited. Does not implicitly {@link google.protobuf.DoubleValue.verify|verify} messages.
             * @param message DoubleValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IDoubleValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a DoubleValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns DoubleValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.DoubleValue;

            /**
             * Decodes a DoubleValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns DoubleValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.DoubleValue;

            /**
             * Verifies a DoubleValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a DoubleValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns DoubleValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.DoubleValue;

            /**
             * Creates a plain object from a DoubleValue message. Also converts values to other types if specified.
             * @param message DoubleValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.DoubleValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this DoubleValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a FloatValue. */
        interface IFloatValue {

            /** FloatValue value */
            value?: (number|null);
        }

        /** Represents a FloatValue. */
        class FloatValue implements IFloatValue {

            /**
             * Constructs a new FloatValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IFloatValue);

            /** FloatValue value. */
            public value: number;

            /**
             * Creates a new FloatValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns FloatValue instance
             */
            public static create(properties?: google.protobuf.IFloatValue): google.protobuf.FloatValue;

            /**
             * Encodes the specified FloatValue message. Does not implicitly {@link google.protobuf.FloatValue.verify|verify} messages.
             * @param message FloatValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IFloatValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified FloatValue message, length delimited. Does not implicitly {@link google.protobuf.FloatValue.verify|verify} messages.
             * @param message FloatValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IFloatValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a FloatValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns FloatValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.FloatValue;

            /**
             * Decodes a FloatValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns FloatValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.FloatValue;

            /**
             * Verifies a FloatValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a FloatValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns FloatValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.FloatValue;

            /**
             * Creates a plain object from a FloatValue message. Also converts values to other types if specified.
             * @param message FloatValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.FloatValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this FloatValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an Int64Value. */
        interface IInt64Value {

            /** Int64Value value */
            value?: (number|Long|null);
        }

        /** Represents an Int64Value. */
        class Int64Value implements IInt64Value {

            /**
             * Constructs a new Int64Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IInt64Value);

            /** Int64Value value. */
            public value: (number|Long);

            /**
             * Creates a new Int64Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Int64Value instance
             */
            public static create(properties?: google.protobuf.IInt64Value): google.protobuf.Int64Value;

            /**
             * Encodes the specified Int64Value message. Does not implicitly {@link google.protobuf.Int64Value.verify|verify} messages.
             * @param message Int64Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IInt64Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Int64Value message, length delimited. Does not implicitly {@link google.protobuf.Int64Value.verify|verify} messages.
             * @param message Int64Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IInt64Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an Int64Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Int64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Int64Value;

            /**
             * Decodes an Int64Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Int64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Int64Value;

            /**
             * Verifies an Int64Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an Int64Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Int64Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Int64Value;

            /**
             * Creates a plain object from an Int64Value message. Also converts values to other types if specified.
             * @param message Int64Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Int64Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Int64Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a UInt64Value. */
        interface IUInt64Value {

            /** UInt64Value value */
            value?: (number|Long|null);
        }

        /** Represents a UInt64Value. */
        class UInt64Value implements IUInt64Value {

            /**
             * Constructs a new UInt64Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IUInt64Value);

            /** UInt64Value value. */
            public value: (number|Long);

            /**
             * Creates a new UInt64Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns UInt64Value instance
             */
            public static create(properties?: google.protobuf.IUInt64Value): google.protobuf.UInt64Value;

            /**
             * Encodes the specified UInt64Value message. Does not implicitly {@link google.protobuf.UInt64Value.verify|verify} messages.
             * @param message UInt64Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IUInt64Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified UInt64Value message, length delimited. Does not implicitly {@link google.protobuf.UInt64Value.verify|verify} messages.
             * @param message UInt64Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IUInt64Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a UInt64Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns UInt64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.UInt64Value;

            /**
             * Decodes a UInt64Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns UInt64Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.UInt64Value;

            /**
             * Verifies a UInt64Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a UInt64Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns UInt64Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.UInt64Value;

            /**
             * Creates a plain object from a UInt64Value message. Also converts values to other types if specified.
             * @param message UInt64Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.UInt64Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this UInt64Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of an Int32Value. */
        interface IInt32Value {

            /** Int32Value value */
            value?: (number|null);
        }

        /** Represents an Int32Value. */
        class Int32Value implements IInt32Value {

            /**
             * Constructs a new Int32Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IInt32Value);

            /** Int32Value value. */
            public value: number;

            /**
             * Creates a new Int32Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Int32Value instance
             */
            public static create(properties?: google.protobuf.IInt32Value): google.protobuf.Int32Value;

            /**
             * Encodes the specified Int32Value message. Does not implicitly {@link google.protobuf.Int32Value.verify|verify} messages.
             * @param message Int32Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IInt32Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Int32Value message, length delimited. Does not implicitly {@link google.protobuf.Int32Value.verify|verify} messages.
             * @param message Int32Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IInt32Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes an Int32Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Int32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Int32Value;

            /**
             * Decodes an Int32Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Int32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Int32Value;

            /**
             * Verifies an Int32Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates an Int32Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Int32Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Int32Value;

            /**
             * Creates a plain object from an Int32Value message. Also converts values to other types if specified.
             * @param message Int32Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Int32Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Int32Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a UInt32Value. */
        interface IUInt32Value {

            /** UInt32Value value */
            value?: (number|null);
        }

        /** Represents a UInt32Value. */
        class UInt32Value implements IUInt32Value {

            /**
             * Constructs a new UInt32Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IUInt32Value);

            /** UInt32Value value. */
            public value: number;

            /**
             * Creates a new UInt32Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns UInt32Value instance
             */
            public static create(properties?: google.protobuf.IUInt32Value): google.protobuf.UInt32Value;

            /**
             * Encodes the specified UInt32Value message. Does not implicitly {@link google.protobuf.UInt32Value.verify|verify} messages.
             * @param message UInt32Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IUInt32Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified UInt32Value message, length delimited. Does not implicitly {@link google.protobuf.UInt32Value.verify|verify} messages.
             * @param message UInt32Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IUInt32Value, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a UInt32Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns UInt32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.UInt32Value;

            /**
             * Decodes a UInt32Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns UInt32Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.UInt32Value;

            /**
             * Verifies a UInt32Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a UInt32Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns UInt32Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.UInt32Value;

            /**
             * Creates a plain object from a UInt32Value message. Also converts values to other types if specified.
             * @param message UInt32Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.UInt32Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this UInt32Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a BoolValue. */
        interface IBoolValue {

            /** BoolValue value */
            value?: (boolean|null);
        }

        /** Represents a BoolValue. */
        class BoolValue implements IBoolValue {

            /**
             * Constructs a new BoolValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IBoolValue);

            /** BoolValue value. */
            public value: boolean;

            /**
             * Creates a new BoolValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns BoolValue instance
             */
            public static create(properties?: google.protobuf.IBoolValue): google.protobuf.BoolValue;

            /**
             * Encodes the specified BoolValue message. Does not implicitly {@link google.protobuf.BoolValue.verify|verify} messages.
             * @param message BoolValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IBoolValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified BoolValue message, length delimited. Does not implicitly {@link google.protobuf.BoolValue.verify|verify} messages.
             * @param message BoolValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IBoolValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a BoolValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns BoolValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.BoolValue;

            /**
             * Decodes a BoolValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns BoolValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.BoolValue;

            /**
             * Verifies a BoolValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a BoolValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns BoolValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.BoolValue;

            /**
             * Creates a plain object from a BoolValue message. Also converts values to other types if specified.
             * @param message BoolValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.BoolValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this BoolValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a StringValue. */
        interface IStringValue {

            /** StringValue value */
            value?: (string|null);
        }

        /** Represents a StringValue. */
        class StringValue implements IStringValue {

            /**
             * Constructs a new StringValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IStringValue);

            /** StringValue value. */
            public value: string;

            /**
             * Creates a new StringValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns StringValue instance
             */
            public static create(properties?: google.protobuf.IStringValue): google.protobuf.StringValue;

            /**
             * Encodes the specified StringValue message. Does not implicitly {@link google.protobuf.StringValue.verify|verify} messages.
             * @param message StringValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IStringValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified StringValue message, length delimited. Does not implicitly {@link google.protobuf.StringValue.verify|verify} messages.
             * @param message StringValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IStringValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a StringValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns StringValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.StringValue;

            /**
             * Decodes a StringValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns StringValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.StringValue;

            /**
             * Verifies a StringValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a StringValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns StringValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.StringValue;

            /**
             * Creates a plain object from a StringValue message. Also converts values to other types if specified.
             * @param message StringValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.StringValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this StringValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a BytesValue. */
        interface IBytesValue {

            /** BytesValue value */
            value?: (Uint8Array|null);
        }

        /** Represents a BytesValue. */
        class BytesValue implements IBytesValue {

            /**
             * Constructs a new BytesValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IBytesValue);

            /** BytesValue value. */
            public value: Uint8Array;

            /**
             * Creates a new BytesValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns BytesValue instance
             */
            public static create(properties?: google.protobuf.IBytesValue): google.protobuf.BytesValue;

            /**
             * Encodes the specified BytesValue message. Does not implicitly {@link google.protobuf.BytesValue.verify|verify} messages.
             * @param message BytesValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IBytesValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified BytesValue message, length delimited. Does not implicitly {@link google.protobuf.BytesValue.verify|verify} messages.
             * @param message BytesValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IBytesValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a BytesValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns BytesValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.BytesValue;

            /**
             * Decodes a BytesValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns BytesValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.BytesValue;

            /**
             * Verifies a BytesValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a BytesValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns BytesValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.BytesValue;

            /**
             * Creates a plain object from a BytesValue message. Also converts values to other types if specified.
             * @param message BytesValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.BytesValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this BytesValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a Struct. */
        interface IStruct {

            /** Struct fields */
            fields?: ({ [k: string]: google.protobuf.IValue }|null);
        }

        /** Represents a Struct. */
        class Struct implements IStruct {

            /**
             * Constructs a new Struct.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IStruct);

            /** Struct fields. */
            public fields: { [k: string]: google.protobuf.IValue };

            /**
             * Creates a new Struct instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Struct instance
             */
            public static create(properties?: google.protobuf.IStruct): google.protobuf.Struct;

            /**
             * Encodes the specified Struct message. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
             * @param message Struct message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IStruct, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Struct message, length delimited. Does not implicitly {@link google.protobuf.Struct.verify|verify} messages.
             * @param message Struct message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IStruct, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Struct message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Struct
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Struct;

            /**
             * Decodes a Struct message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Struct
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Struct;

            /**
             * Verifies a Struct message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Struct message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Struct
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Struct;

            /**
             * Creates a plain object from a Struct message. Also converts values to other types if specified.
             * @param message Struct
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Struct, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Struct to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** Properties of a Value. */
        interface IValue {

            /** Value nullValue */
            nullValue?: (google.protobuf.NullValue|null);

            /** Value numberValue */
            numberValue?: (number|null);

            /** Value stringValue */
            stringValue?: (string|null);

            /** Value boolValue */
            boolValue?: (boolean|null);

            /** Value structValue */
            structValue?: (google.protobuf.IStruct|null);

            /** Value listValue */
            listValue?: (google.protobuf.IListValue|null);
        }

        /** Represents a Value. */
        class Value implements IValue {

            /**
             * Constructs a new Value.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IValue);

            /** Value nullValue. */
            public nullValue: google.protobuf.NullValue;

            /** Value numberValue. */
            public numberValue: number;

            /** Value stringValue. */
            public stringValue: string;

            /** Value boolValue. */
            public boolValue: boolean;

            /** Value structValue. */
            public structValue?: (google.protobuf.IStruct|null);

            /** Value listValue. */
            public listValue?: (google.protobuf.IListValue|null);

            /** Value kind. */
            public kind?: ("nullValue"|"numberValue"|"stringValue"|"boolValue"|"structValue"|"listValue");

            /**
             * Creates a new Value instance using the specified properties.
             * @param [properties] Properties to set
             * @returns Value instance
             */
            public static create(properties?: google.protobuf.IValue): google.protobuf.Value;

            /**
             * Encodes the specified Value message. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
             * @param message Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified Value message, length delimited. Does not implicitly {@link google.protobuf.Value.verify|verify} messages.
             * @param message Value message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a Value message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.Value;

            /**
             * Decodes a Value message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns Value
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.Value;

            /**
             * Verifies a Value message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a Value message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns Value
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.Value;

            /**
             * Creates a plain object from a Value message. Also converts values to other types if specified.
             * @param message Value
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.Value, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this Value to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }

        /** NullValue enum. */
        enum NullValue {
            NULL_VALUE = 0
        }

        /** Properties of a ListValue. */
        interface IListValue {

            /** ListValue values */
            values?: (google.protobuf.IValue[]|null);
        }

        /** Represents a ListValue. */
        class ListValue implements IListValue {

            /**
             * Constructs a new ListValue.
             * @param [properties] Properties to set
             */
            constructor(properties?: google.protobuf.IListValue);

            /** ListValue values. */
            public values: google.protobuf.IValue[];

            /**
             * Creates a new ListValue instance using the specified properties.
             * @param [properties] Properties to set
             * @returns ListValue instance
             */
            public static create(properties?: google.protobuf.IListValue): google.protobuf.ListValue;

            /**
             * Encodes the specified ListValue message. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
             * @param message ListValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encode(message: google.protobuf.IListValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Encodes the specified ListValue message, length delimited. Does not implicitly {@link google.protobuf.ListValue.verify|verify} messages.
             * @param message ListValue message or plain object to encode
             * @param [writer] Writer to encode to
             * @returns Writer
             */
            public static encodeDelimited(message: google.protobuf.IListValue, writer?: $protobuf.Writer): $protobuf.Writer;

            /**
             * Decodes a ListValue message from the specified reader or buffer.
             * @param reader Reader or buffer to decode from
             * @param [length] Message length if known beforehand
             * @returns ListValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): google.protobuf.ListValue;

            /**
             * Decodes a ListValue message from the specified reader or buffer, length delimited.
             * @param reader Reader or buffer to decode from
             * @returns ListValue
             * @throws {Error} If the payload is not a reader or valid buffer
             * @throws {$protobuf.util.ProtocolError} If required fields are missing
             */
            public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): google.protobuf.ListValue;

            /**
             * Verifies a ListValue message.
             * @param message Plain object to verify
             * @returns `null` if valid, otherwise the reason why it is not
             */
            public static verify(message: { [k: string]: any }): (string|null);

            /**
             * Creates a ListValue message from a plain object. Also converts values to their respective internal types.
             * @param object Plain object
             * @returns ListValue
             */
            public static fromObject(object: { [k: string]: any }): google.protobuf.ListValue;

            /**
             * Creates a plain object from a ListValue message. Also converts values to other types if specified.
             * @param message ListValue
             * @param [options] Conversion options
             * @returns Plain object
             */
            public static toObject(message: google.protobuf.ListValue, options?: $protobuf.IConversionOptions): { [k: string]: any };

            /**
             * Converts this ListValue to JSON.
             * @returns JSON object
             */
            public toJSON(): { [k: string]: any };
        }
    }
}
