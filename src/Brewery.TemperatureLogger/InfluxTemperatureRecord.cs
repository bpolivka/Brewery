﻿using System;
using Vibrant.InfluxDB.Client;

namespace Brewery.TemperatureLogger
{
    class InfluxTemperatureRecord
    {
        [InfluxTimestamp]
        public DateTime Time { get; set; }

        [InfluxTag("probe")]
        public string Probe { get; set; }

        [InfluxField("temp")]
        public double Temperature { get; set; }

        [InfluxField("rawtemp")]
        public double UnadjustedTemperature { get; set; }
    }
}
