﻿using System;

namespace Brewery.TemperatureLogger
{
    class InfluxSettings
    {
        public Uri Endpoint { get; set; }
        public string Database { get; set; }
    }
}
