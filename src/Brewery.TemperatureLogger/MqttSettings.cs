﻿namespace Brewery.TemperatureLogger
{
    class MqttSettings
    {
        public string ClientId { get; set; }
        public string Server { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
