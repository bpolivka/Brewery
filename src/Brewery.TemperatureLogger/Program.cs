﻿using System;
using System.IO;
using System.Threading.Tasks;
using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Vibrant.InfluxDB.Client;

namespace Brewery.TemperatureLogger
{
    class Program
    {
        static int Main(string[] args)
        {
            return CommandLineApplication.Execute<Program>(args);
        }

        [Option]
        public string[] ConfigFiles { get; set; }

        private void OnExecute()
        {
            var host = new HostBuilder()
                .ConfigureHostConfiguration(OnConfigureHostConfiguration)
                .ConfigureAppConfiguration(OnConfigureAppConfiguration)
                .ConfigureServices(OnConfigureServices)
                .ConfigureLogging(OnConfigureLogging)
                .Build();

            host.Run();
        }

        private void OnConfigureLogging(HostBuilderContext ctx, ILoggingBuilder builder)
        {
            builder.AddConfiguration(ctx.Configuration.GetSection("Logging"));
            builder.AddConsole();
        }

        private void OnConfigureServices(HostBuilderContext ctx, IServiceCollection services)
        {
            services.Configure<InfluxSettings>(ctx.Configuration.GetSection("Influx"));
            services.Configure<MqttSettings>(ctx.Configuration.GetSection("Mqtt"));
            services.AddHostedService<TemperatureLoggerService>();
        }

        private void OnConfigureHostConfiguration(IConfigurationBuilder builder)
        {
            builder.AddEnvironmentVariables();
        }

        private void OnConfigureAppConfiguration(HostBuilderContext ctx, IConfigurationBuilder builder)
        {
            builder.AddEnvironmentVariables("Brewery");

            foreach (var file in ConfigFiles)
            {
                builder.AddJsonFile(Path.GetFullPath(file));
            }
        }
    }
}
