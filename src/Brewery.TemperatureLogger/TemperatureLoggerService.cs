﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using MQTTnet;
using MQTTnet.Client;
using MQTTnet.Extensions.ManagedClient;
using Vibrant.InfluxDB.Client;
using Brewery.Protobuf;
using Microsoft.Extensions.Logging;

namespace Brewery.TemperatureLogger
{
    class TemperatureLoggerService : IHostedService
    {
        readonly InfluxSettings _influxSettings;
        readonly MqttSettings _mqttSettings;
        readonly IInfluxClient _influxClient;
        readonly IManagedMqttClient _mqttClient;
        readonly ILogger _logger;

        public TemperatureLoggerService(
            IOptionsSnapshot<InfluxSettings> influxOptions,
            IOptionsSnapshot<MqttSettings> mqttOptions,
            ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<TemperatureLoggerService>();

            _influxSettings = influxOptions.Value;
            _mqttSettings = mqttOptions.Value;

            _influxClient = new InfluxClient(_influxSettings.Endpoint);
            _mqttClient = new MqttFactory().CreateManagedMqttClient();
            _mqttClient.ApplicationMessageReceived += OnApplicationMessageReceived;
        }

        private async void OnApplicationMessageReceived(object sender, MqttApplicationMessageReceivedEventArgs e)
        {
            var update = TemperatureUpdate.Parser.ParseFrom(e.ApplicationMessage.Payload);

            var influxRecord = new InfluxTemperatureRecord
            {
                Time = DateTime.UtcNow,
                Probe = GetProbeName(update.Probe),
                Temperature = update.Temperature,
                UnadjustedTemperature = update.UnadjustedTemperature
            };

            _logger.LogDebug(
                "Recieved update: probe: {Probe}, temp: {Temperature}, temp (unadj): {UnadjustedTemperature}",
                influxRecord.Probe, influxRecord.Temperature, influxRecord.UnadjustedTemperature);

            try
            {
                await _influxClient.WriteAsync(_influxSettings.Database, "temperatures", new[] { influxRecord });
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error writing to database: {ErrorMessage}", ex.Message);
            }
        }

        private static string GetProbeName(TemperatureProbe probe)
        {
            switch (probe)
            {
                case TemperatureProbe.Hlt: return "hlt";
                case TemperatureProbe.Boil: return "boil";
                case TemperatureProbe.Mltin: return "mlt-in";
                case TemperatureProbe.Mltout: return "mlt-out";
                default: return "unknown";
            }
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var options = new ManagedMqttClientOptionsBuilder()
                .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
                .WithClientOptions(new MqttClientOptionsBuilder()
                    .WithClientId(_mqttSettings.ClientId)
                    .WithTcpServer(_mqttSettings.Server)
                    .WithCredentials(_mqttSettings.Username, _mqttSettings.Password)
                    .Build())
                .Build();

            await _mqttClient.SubscribeAsync("brewery/temp-update");
            await _mqttClient.StartAsync(options);
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _mqttClient.StopAsync();
            _mqttClient.Dispose();
        }
    }
}
